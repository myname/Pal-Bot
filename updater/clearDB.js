/**
 * 
 */


module.exports = async function(item=false, db) {
    try {
        if (item) {
            await db.clearItem()

            // получаем данные от хайрез и записываем в БД
            db.items = {}
            db.items.ru = await db.api.getitems(11)
            db.items.en = await db.api.getitems(1)
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        return {
            status: false,
            err_msg: {
                ru: '',
                en: ''
            },
            log_msg: ''
        }
    }
}