/**
 * 
 */


module.exports = async function(db) {
    try {
        // удаляем данные с БД
        await db.clearChampions()

        // получаем данные от хайрез и записываем в БД
        db.champions = {}
        db.champions.ru = await db.api.getchampions(11)
        db.champions.en = await db.api.getchampions(1)
    } catch(error) {
        if ('err_msg' in error) throw error
        return {
            status: false,
            err_msg: {
                ru: '',
                en: ''
            },
            log_msg: ''
        }
    }
}