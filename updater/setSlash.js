/**
 * 
 */
const path = require('path')
const CommandsManager = require(path.join(require.main.path, 'classes', 'CommandsManager.js'))


module.exports = async function(client, db) {
	try {
        const commandsManager = new CommandsManager()
		const commandsList = []

		const champions = db.champions

		if (!champions) {
			console.log('Чемпионы не найдены.')
			return false
		}

		commandsManager.each(com => {
			if (!com.getSlash) return;
			const params = com.getSlash(champions.en)
			commandsList.push(params)
		})

		await client.application?.commands.set(commandsList)
		console.log(`\n\tВСЕ команды установленны!`)
        return true
	} catch(err) {
		console.log(err)
        return false
	}
}