/**
 * классы
 */
const path = require('path')
const Command = require(path.join(require.main.path, 'classes', 'Command.js')) // `${require.main.path}/classes/Command.js`
const commands = require(path.join(require.main.path, 'commands', 'index.js'))


module.exports = class CommandsManager {
    #commands = [] // пока не уверен нужно ли показывать это все сразу или нет

    constructor() {
        let count = 0
        for (const commandName in commands) {
            count++
            this.add(new Command(commands[ commandName ]))
        }

        this.size = count
        // this.list = this.#commands // для теста
    }

    // получает команду если она есть (вместо проверки можно использовать)
    get(text) {
        return this.#commands.find(com => com.check(text))
    }

    getByName(commandName) {
        commandName = commandName.toLowerCase()
        return this.#commands.find(com => com.name == commandName)
    }

    getBySlashName(commandName) { // group, subcommand
        return this.#commands.find(com => com.slashName == commandName)

        // return this.#commands.find(com => {
        //     const slashSub = com.slashSub || new Set()
        //     const slashGroup = com.slashGroup || new Set()
        //     const checkGroup = group ? slashGroup.has(group) : true
        //     const checkSub = subcommand ? slashGroup.has(subcommand) : true
        //     return com.slashName == commandName && checkGroup && checkSub
        // })
    }

    add(command) {
        this.#commands.push(command)
        return this
    }

    // перебирает все команды
    each(callback) {
        this.#commands.forEach(callback)
        return this
    }

    // get list() {
    //     return this.#commands
    // }
}