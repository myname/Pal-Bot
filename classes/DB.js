const fs = require('fs')


module.exports = class DB {
    constructor() {
        this.dbName = 'db.json'
        this.statsName = 'commandStats.json'
        this.data = null
    }

    get pathToMain() {
        return './db/'
    }

    get dbPath() {
        return this.pathToMain + this.dbName
    }

    get statsPath() {
        return this.pathToMain + this.statsName 
    }

    async checkHasMainDir() {
        try {
            return !!await fs.statSync( this.pathToMain )
        } catch(e) {
            return false
        }
    }

    async testMainDir() {
        const check = await this.checkHasMainDir()
        if ( check ) return;
        await fs.mkdirSync( this.pathToMain )
    }

    async save() {
        try {
            const data = this.data
            if ( !data ) return true
            const json = JSON.stringify( data )
            await fs.writeFileSync( this.dbPath, json, { encoding: 'utf-8' })
            return true
        } catch( error ) {
            console.error( error )
            return false
        }
    }

    async start() {
        try {
            this.testMainDir()

            const load = await this.load()
            if ( load ) return true

            console.log('Создаем БД')
            const create = await this.createDb()
            if ( !create ) return false
            console.log('Создание БД завершено успешно')
            return await this.load()
        } catch( error ) {
            console.error( error )
            return false
        }
    }

    async load() {
        try {
            const data = await fs.readFileSync( this.dbPath, { encoding: 'utf-8' } )
            this.data = JSON.parse( data )
            return true
        } catch( error ) {
            // console.error( error )
            return false
        }
    }

    async createDb() {
        try {
            await fs.writeFileSync( this.dbPath, '{}', { encoding: 'utf-8' })
            return true
        } catch( error ) {
            console.error( error )
            return false
        }
    }




    get session() {
        return this.data.session
    }

    setSettion( session ) {
        this.data.session = session
    }

    get users() {
        if ( !this.data.users ) this.data.users = {}
        return this.data.users
    }

    get guilds() {
        if ( !this.data.guilds ) this.data.guilds = {}
        return this.data.guilds
    }

    // get stats() {
    //     if ( !this.stats ) this.stats = []
    //     return this.stats
    // }

    async loadStats() {
        try {
            const sttsJSON = await fs.readFileSync( this.statsPath, { encoding: 'utf-8' } )
            this.stats = JSON.parse( sttsJSON )
        } catch(e) {
            console.log(`Ошибка при загрузке статистики команд/серверов`)
        }
    }

    async saveStats() {
        try {
            const json = JSON.stringify( this.stats )
            await fs.writeFileSync( this.statsPath, json, { encoding: 'utf-8' })
        } catch(e) {
            console.log(`Ошибка при сохранении статистики команд/серверов`)
        }
    }

    async clearItem() {}

    async clearChampions() {}
}

