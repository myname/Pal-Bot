/**
 * классы
 */


module.exports = class Card {
    constructor(card) {
        this.id = card.ItemId
        this.type = this.getType(card.item_type)
        this.championId = card.champion_id
        this.name = card.DeviceName
        this.description = card.Description
        this.shortDesc = card.ShortDesc
        this.recharge = card.recharge_seconds
    }

    getType(item_type) {
        if (!item_type) return null
        return item_type.match(/talents/i) ? 'legendary' : 'common'
    }

    // получает описание карты на указанном языке и убирая 'scale' подстявляя нужно значение исходя из уровня карты
    getDescriptionLvl(lvl) {
        if (!this.description) return ''
        const desc = this.description.replace(/^\[[а-яa-z `'_-]+\] /i, '') // убираем принадлежность (то что в [...])
        const matchArr = desc.match(/\{scale ?= ?([0-9\.]+)\|(-?[0-9\.]+)s?\}/i)
        if (!matchArr) return desc

        let scaleText = +matchArr[1]
        for (let i = 1; i < lvl; i++) {
            scaleText += +matchArr[2]
        }

        // фиксим до 2 точек и если 2 нуля в конце то убираем их
        scaleText = scaleText.toFixed(2)
        if (scaleText.slice(-2) == "00") scaleText = scaleText.slice(0, -3)

        const retDesc = desc.replace(/\{scale ?= ?([0-9\.]+)\|(-?[0-9\.]+)s?\}/i, scaleText)
        if ( retDesc.match(/\{scale ?= ?([0-9\.]+)\|(-?[0-9\.]+)s?\}/i) ) {
            const matchArr = retDesc.match(/\{scale ?= ?([0-9\.]+)\|(-?[0-9\.]+)s?\}/i)
            if (!matchArr) return false // поидее такого быть не должно

            let scaleText = +matchArr[1]
            for (let i = 1; i < lvl; i++) {
                scaleText += +matchArr[2]
            }

            // фиксим до 2 точек и если 2 нуля в конце то убираем их
            scaleText = scaleText.toFixed(2)
            return retDesc.replace(/\{scale ?= ?([0-9\.]+)\|(-?[0-9\.]+)s?\}/i, scaleText)
        }
        return retDesc
    }
}