/**
 * классы DELETE
 */
const path = require('path')
// const Setting = require(path.join(require.main.path, 'classes', 'Setting.js'))
const { settingsUserParams, settingsGuildParams, defaultSettings, langToNum } = require(path.join(require.main.path, 'config', 'index.js'))


module.exports = class SettingsManager {
	constructor({ db, userId, guildId, preferredLocale }) {
		this.db = db

		// фиксим en-US
		if ( preferredLocale && preferredLocale.indexOf('en') === 0 ) preferredLocale = 'en'

		this.userId = userId
		this.guildId = guildId
		this.lang = preferredLocale
	}

	get user() {
		// сначала дефолтные данные, потом данные сервера, потом данные пользователя
		const id = this.userId
		const guildId = this.guildId
		const userData = this.db?.users?.[ id ]
		const guildData = this.db?.guilds?.[ guildId ]

		const lang = userData?.lang || guildData?.lang || this.lang || defaultSettings.lang
		const timezone = userData?.timezone || guildData?.timezone || defaultSettings.timezone
		const filters = userData?.filters || {}

		return { id, lang, timezone, filters }
	}

	get guild() {
		const id = this.guildId
		const guildData = this.db?.guilds?.[ id ]

		const lang = guildData?.lang || this.lang || defaultSettings.lang
		const timezone = guildData?.timezone || defaultSettings.timezone

		return { id, lang, timezone }
	}

	async saveUser() {
		this.db.users[ this.userId ] = this.user
		this.db.save()
	}
}
