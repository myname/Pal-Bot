/**
 * https://docs.google.com/document/d/1OFS-3ocSx-1Rvg4afAnEHlT3917MAK_6eJTR6rzr-BM/edit#
 */
const moment = require("moment")
const md5 = require("md5")
const randomUseragent = require('random-useragent')
const axios = require('axios')


module.exports = class Hirez {
	#id; #key;
	constructor({ DEVID, AUTHKEY, db }) {
        // const path = require('path')
        // const { session: sessionModel  } = require(path.join(require.main.path, 'mongo', 'schemes.js'))
        // const { DEVID, AUTHKEY } = process.env

        // this.model = sessionModel
		this.#id = DEVID
		this.#key = AUTHKEY
        this.db = db
        this.session = db.session
        this.url = 'https://api.paladins.com/paladinsapi.svc/'
	}

    // првоеряет сессию по времени
	async testSession() { // проверяет валидность сессии, если не валидна то создает новую
        if ( !this.session ) return false

        const newdate = +(moment().utc())
        // const checkTime = newdate - 840000 < this.timeStartSession // 900000
        const checkTime = newdate - 840000 < this.session.timestamp // 900000

        console.log(`Минут с последнего использования сессии: ${(newdate - this.session.timestamp) / 60000}`)

        return checkTime
	}

    // запуск работы
    async start() {
        const check = await this.testSession()
        if ( check ) return;

        await this.createSession()
    }

    // создает новую сессию
	async createSession() {
        try {
            console.log('\n !!! создаем сессию\n')
            // const { sendSite } = require(path.join(require.main.path, 'utils', 'requestFunctions.js'))

            const timestamp = moment().utc().format("YYYYMMDDHHmmss")
            const signature = md5(this.#id + "createsession" + this.#key + timestamp)
            const urlCreateSession = `${this.url}createsessionJson/${this.#id}/${signature}/${timestamp}`

            const response = await axios.get(urlCreateSession, {
                headers: {
                    'User-Agent': randomUseragent.getRandom(),
                    'Content-Type': 'application/json',
                    'X-Powered-By': 'bacon',
                    'Connection': 'keep-alive',
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/jpg,*/*;q=0.8'
                }
            })

            const data = response.data
            // console.log( `RES:`, data )

            if ( data?.ret_msg !== 'Approved' ) throw { data: response.data, error: 'Ошибка создания сессии' }

            // записываем изменения сессии
            this.session = {
                id: data.session_id,
                timestamp: +(moment().utc())
            }

            await this.saveSessionOnDb()
        } catch(error) {
            console.log( error )
            if (error?.err_msg) throw error
            return {
                error,
                err_msg: {
                    ru: 'Ошибка создания сессии.',
                    en: 'Session creation error.'
                },
                log_msg: 'hi-rez api.createSession',
                status: false
            }
        }
	}

    async saveSessionOnDb() {
        console.log(`\nзаписываем сессию в БД`)
        this.db.setSettion( this.session )
        await this.db.save()
    }

    // выполняет запрос статистики к хайрезам
	async ex(format, ...params) {
        try {
            console.log(`ex: ${format}`, params)
			if (!format) return { status: false, error: 'Не указан формат' }

            // сделать проверку параметров всех! (не должны содержать запрещенные символы)

            // типо проверки сессии всегда
            await this.start()

            const timestamp = moment().utc().format("YYYYMMDDHHmmss")
            const signature = md5(this.#id + format + this.#key + timestamp)
            const strParams = params.length > 0 ? `/${params.join("/")}` : ''
            const url = `${this.url}${format}Json/${this.#id}/${signature}/${this.session.id}/${timestamp}${strParams}`

            let response
            try {
                response = await axios.get( url )
            } catch( e ) {
                let resolver = null
                let rejected = null
                const promise = new Promise((resolve, reject) => {resolver = resolve; rejected = reject})
                setTimeout(async () => {
                    try {
                        response = await axios.get( url )
                        resolver()
                    } catch(e) {
                        rejected()
                    }
                }, 777)
                await promise
            }
            // const response = await sendSite({ url, json:true })
            const { data } = response
            // console.log( `DATA:::`, data )

            if (data.constructor === String) throw {
                status: false,
                error: data,
                err_msg: {
                    ru: 'Ошибка API запроса. Возможно API временно недоступно. Хайрезы наверняка уже трудятся чтобы решить эту проблему.',
                    en: 'Request API error. The API maybe temporarily unavailable. Hi-Rez are already working on a solution to this problem.'
                },
                log_msg: `hi-rez api.ex: ${data}`
            }

            // если пустые данные
            if (data.constructor === Array && !data.length) {
                // сохраняем сессию (изменяем)
                await this.saveSessionOnDb()

                return {
                    status: true,
                    data: data
                }
            }

            // если сессия устарела то получаем новую
            if (data[0]?.ret_msg == 'Invalid session id.') {
                await this.createSession()
                return await this.ex(format, ...params)
            }

            // обновляем время сессии
            this.session.timestamp = +(moment().utc())

            await this.saveSessionOnDb()

            return {
                data: data,
                // lastUpdate: +timestamp.updateToDate(),
                status: true
            }
        } catch(error) {
            console.log( error )
            if (error?.err_msg) throw error
            return {
                error,
                err_msg: {
                    ru: 'Ошибка получения данных.',
                    en: 'Error receiving data.'
                },
                log_msg: 'hi-rez api.ex',
                status: false
            }
        }
	}
}