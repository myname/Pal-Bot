/**
 * https://docs.google.com/document/d/1OFS-3ocSx-1Rvg4afAnEHlT3917MAK_6eJTR6rzr-BM/edit#
 */
const path = require('path')
const fs = require('fs')
const Hirez = require(path.join(require.main.path, 'classes', 'Hirez.js'))


// основные функции для запросы статы у хайрезов (главные, движ)
module.exports = class API {
    #api;
    constructor({ DEVID, AUTHKEY, db }) {
        this.#api = new Hirez({ DEVID, AUTHKEY, db })
        this.db = db
    }

    async start() {
        await this.#api.start()
    }

    async getSessionOfDB() {
        // await this.#api.getSessionOfDB()
        throw 'SMOTRI!!!'
    }

    async checkHasFile( filepath ) {
        try {
            return await fs.statSync( filepath )
        } catch( error ) {
            return false
        }
    }

    async getchampions(lang=1, update=false) {
        const langStr = lang === 1 ? 'EN' : 'RU'
        const pathToFile = `./db/champions${ langStr }.json`

        const checkFile = await this.checkHasFile( pathToFile )
        if ( !checkFile ) {
            await fs.writeFileSync( pathToFile, '[]', { encoding: 'utf-8' })
        }

        const dataJSON = await fs.readFileSync( pathToFile, { encoding: 'utf-8' } )
        const getchampions = JSON.parse( dataJSON )

        // вернем даные если они есть
        if ( getchampions.length && !update ) return getchampions

        // если данных нет то запросим у хайрезов
        const newData = await this.#api.ex('getchampions', lang)
        if (!newData.status) throw newData

        // фильтруем ненужные данные от хайрезов (экономим память)
        const newDataChampions = newData.data.map(champ => {
            const { Health, Lore, Name, Name_English, Roles, Speed, Title, id } = champ
            return {
                health: Health,
                lore: Lore,
                name: Name,
                nameEn: Name_English,
                role: Roles.replace(/Paladins /i, '').replace(/[ ]/ig, '').trim().toLowerCase(),
                speed: Speed,
                title: Title,
                id: id
            }
        })

        // сохраним в файл новые данные
        await fs.writeFileSync( pathToFile, JSON.stringify( newDataChampions ), { encoding: 'utf-8' })

        return newDataChampions
    }

    async getitems(lang=1, update=false) {
        const langStr = lang === 1 ? 'EN' : 'RU'
        const pathToFile = `./db/items${ langStr }.json`

        const checkFile = await this.checkHasFile( pathToFile )
        if ( !checkFile ) {
            await fs.writeFileSync( pathToFile, '[]', { encoding: 'utf-8' })
        }

        const dataJSON = await fs.readFileSync( pathToFile, { encoding: 'utf-8' } )
        const getitems = JSON.parse( dataJSON )

        // вернем даные если они есть
        if ( getitems.length && !update ) return getitems

        // если данных нет то запросим у хайрезов
        const newData = await this.#api.ex('getitems', lang)
        if (!newData.status) throw newData

        // фильтруем ненужные данные от хайрезов (экономим память)
        const newDataChampions = newData.data.map(champ => {
            delete champ.itemIcon_URL
            delete champ.ret_msg
            return champ
        })

        // сохраним в файл новые данные
        await fs.writeFileSync( pathToFile, JSON.stringify( newDataChampions ), { encoding: 'utf-8' })

        return newDataChampions
    }

    async searchplayers(name) {
        const newData = await this.#api.ex('searchplayers', name)
        if (!newData.status) throw newData

        return newData
    }
    
    async getplayer(nameOrId) {
        const newData = await this.#api.ex('getplayer', nameOrId)
        if (!newData.status) throw newData

        return newData
    }

    // getplayeridbyportaluserid
    async getplayeridbyportaluserid(id, portalId) {
        const newData = await this.#api.ex('getplayeridbyportaluserid', portalId, id)
        if (!newData.status) throw newData

        return newData
    }

    // getplayeridinfoforxboxandswitch
    
    async getchampionranks(id) {
        const newData = await this.#api.ex('getchampionranks', id)
        if (!newData.status) throw newData

        return newData
    }

    async getplayerloadouts(id, langSearch=1) {
        const newData = await this.#api.ex('getplayerloadouts', id, langSearch)
        if (!newData.status) throw newData

        return newData
    }

    async getplayerstatus(playerId) {
        const newData = await this.#api.ex('getplayerstatus', playerId)
        if (!newData.status) throw newData

        return newData
    }

    async getmatchplayerdetails(id) {
        const newData = await this.#api.ex('getmatchplayerdetails', id)
        if (!newData.status) throw newData

        return newData
    }

    async getmatchhistory(id) {
        const newData = await this.#api.ex('getmatchhistory', id)
        if (!newData.status) throw newData

        return newData
    }

    async getmatchdetails(id) {
        const newData = await this.#api.ex('getmatchdetails', id)
        if (!newData.status) throw newData

        return newData
    }

    async getfriends(id) {
        const newData = await this.#api.ex('getfriends', id)
        if (!newData.status) throw newData

        return newData
    }

    // async getdataused() {}
}