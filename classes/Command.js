/**
 * классы
 */
const path = require('path')


module.exports = class Command {
    constructor(params) {
        for (let parName in params) {
            this[parName] = params[parName]
        }

        params.files.forEach(filename => {
            this[filename] = require(path.join(require.main.path, 'commands', this.name, `${filename}.js`))
        })
    }

    // проверяет есть ли текущая команда в строке (без префикса) и возвращает ее имя либо false
    check(text) {
        text = text.toLowerCase()
        const commandName = (text.match(/^[a-zа-я]+/i) || [])[0]
        if (!commandName) return false
        return this.possibly.find(name => name == commandName) || false
    }

    // добавляет использование команды в статистику
    async used({ client, db }) {
        const stats = db.stats
        const current = stats.length - 1
        const lastDay = stats[current < 0 ? 0 : current]
        const DATE = new Date()
        const newDayNow = !lastDay ? true : new Date(lastDay.date).getUTCDate() != DATE.getUTCDate()

        let servers = 0
        let users = 0
        client.guilds.cache.forEach(guild => {
            servers++
            users += guild.memberCount
        })

        const usedCommands = lastDay && !newDayNow ? +lastDay.used + 1 : 1
        // const timeWork = DATE - process.env.timeStart
        const commandsStats = (lastDay && !newDayNow) ? lastDay.com : {}
        const statsName = this.statsName || this.name
        if (!commandsStats[ statsName ]) commandsStats[ statsName ] = 0
        commandsStats[ statsName ]++

        if (!lastDay || newDayNow) {
            // если данных нет то сейвим (или новый день)
            stats.push({
                servers,
                users,
                used: usedCommands,
                com: commandsStats,
                date: DATE
            })

            await db.saveStats() // более оптимизированней будет если запись будет по таймеру в main.js
        } else {
            // если есть данные то изменяем их и сохраняем (старый день)
            lastDay.servers = servers
            lastDay.users = users
            lastDay.used = usedCommands
            lastDay.com = commandsStats

            await db.saveStats() // более оптимизированней будет если запись будет по таймеру в main.js
        }
    }
}