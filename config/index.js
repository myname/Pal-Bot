/**
 * основной файл-скрипт для загрузки всех файлов в этой папке (переделать под ЭТО)
 * файл-конфиг
 */


module.exports = {
	youtube: {
		playlist: 'https://www.youtube.com/playlist?list=PL6cmDtlldgjjvucCEYaSJwCvxxqMi1YCR',
		search: 'https://www.youtube.com/watch?v=x-ue1m1xgXg&list=PL6cmDtlldgjjvucCEYaSJwCvxxqMi1YCR',
		me: 'https://www.youtube.com/watch?v=S8wEKL-iEwA&list=PL6cmDtlldgjjvucCEYaSJwCvxxqMi1YCR',
		// help: '',
		setting: 'https://www.youtube.com/watch?v=Iqi-egskbiw&list=PL6cmDtlldgjjvucCEYaSJwCvxxqMi1YCR',
		stats: 'https://www.youtube.com/watch?v=O5ran7ta6qE&list=PL6cmDtlldgjjvucCEYaSJwCvxxqMi1YCR',
		menu: 'https://www.youtube.com/watch?v=SGKkqffTWws&list=PL6cmDtlldgjjvucCEYaSJwCvxxqMi1YCR',
		last: 'https://www.youtube.com/watch?v=OdeTRk4yINk&list=PL6cmDtlldgjjvucCEYaSJwCvxxqMi1YCR',
		history: 'https://www.youtube.com/watch?v=cyWmHALlsa0&list=PL6cmDtlldgjjvucCEYaSJwCvxxqMi1YCR',
		friends: 'https://www.youtube.com/watch?v=Nk90eb9PQXY&list=PL6cmDtlldgjjvucCEYaSJwCvxxqMi1YCR',
		deck: 'https://www.youtube.com/watch?v=DcNH4-V2r38&list=PL6cmDtlldgjjvucCEYaSJwCvxxqMi1YCR',
		current: 'https://www.youtube.com/watch?v=v__QkUe9nvY&list=PL6cmDtlldgjjvucCEYaSJwCvxxqMi1YCR',
		champions: 'https://www.youtube.com/watch?v=8t06-iC3CUs&list=PL6cmDtlldgjjvucCEYaSJwCvxxqMi1YCR',
		champion: 'https://www.youtube.com/watch?v=KQJ-v0UAHnQ&list=PL6cmDtlldgjjvucCEYaSJwCvxxqMi1YCR',
		an: 'https://www.youtube.com/watch?v=FhRb4AfeAXw&list=PL6cmDtlldgjjvucCEYaSJwCvxxqMi1YCR'
	},
	gif: {
		search: 'https://media.discordapp.net/attachments/786140964360159283/1009752691058167828/search.gif',
		me: 'https://media.discordapp.net/attachments/786140964360159283/1010512821647331418/me.gif',
		help: 'https://media.discordapp.net/attachments/786140964360159283/1010564646933115040/help.gif',
		setting: 'https://media.discordapp.net/attachments/786140964360159283/1011178458325467227/setting.gif',
		stats: 'https://media.discordapp.net/attachments/786140964360159283/1011277888026316821/stats.gif',
		menu: 'https://media.discordapp.net/attachments/786140964360159283/1011278720142684160/menu.gif',
		last: 'https://media.discordapp.net/attachments/786140964360159283/1011279405785550878/last.gif',
		history: 'https://media.discordapp.net/attachments/786140964360159283/1011279905851457566/history.gif',
		friends: 'https://media.discordapp.net/attachments/786140964360159283/1011280485697196082/friends.gif',
		deck: 'https://media.discordapp.net/attachments/786140964360159283/1011281352261370017/lodouts.gif',
		current: 'https://media.discordapp.net/attachments/786140964360159283/1011282418721894450/lodouts.gif',
		champions: 'https://media.discordapp.net/attachments/786140964360159283/1011283345419812925/champions.gif',
		champion: 'https://media.discordapp.net/attachments/786140964360159283/1011283749956227212/champion.gif'
	},
	defaultSettings: {
		lang: 'en',
		timezone: 0
	},
	slogan: {
		ru: 'Palb-Bot - Лучшая статистика в наилучшем виде',
		en: 'Palb-Bot - The best statistics in the best possible way'
	},
	settingsUserParams: [ 'id', 'lang', 'timezone', 'params' ],
	settingsGuildParams: [ 'id', 'lang', 'timezone', 'searchservers', 'voicemembersize', 'membercount', 'icon', 'name', 'locale', 'invite' ],
	news: { // то что будет писатсья вместе с каждым сообщением бота (в его начале)
		ru: '```yaml\nНе стесняйтесь жать кнопки. Если что непонятно, то воспользуйтесь командой /help или же спросите об этом на сервере поддержки бота.```',
		en: '```yaml\nFeel free to press the buttons. If something is unclear, then use the /help command or ask about it on the bot support server.```'
	},
	backgrounds: [],
	colors: { // цвета используемые в canvas
		red: '#CC0000',
		white: '#FFFFFF',
		blue: '#2e6dc8', // 0088bb
		darkBlue: '#0a2948',
		// lightBlue: '#3399CC',
		black: '#000000',
		purple: '#9966FF',
		orange: '#ca541b', // EE5500
		lightGreen : '#2da851',
		green: '#3ba934', // 36BF09
		yellow: '#F2EE13',
		greyYellow: '#cece00',
		grey: '#36393f',
		lightGrey: '#676d78',
		transparent: '#00000090'
	},
	platforms: {
		1: 'Hi-Rez',
		5: 'Steam',
		9: 'PS4',
		10: 'Xbox',
		13: 'Google',
		22: 'Switch',
		25: 'Discord',
		28: 'Epic Games'
	},
	ranks: {
		ru: [
			'Калибровка', 'Бронза 5', 'Бронза 4', 'Бронза 3', 'Бронза 2', 'Бронза 1',
			'Сильвер 5', 'Сильвер 4', 'Сильвер 3', 'Сильвер 2', 'Сильвер 1',
			'Золото 5', 'Золото 4', 'Золото 3', 'Золото 2', 'Золото 1',
			'Платина 5', 'Платина 4', 'Платина 3', 'Платина 2', 'Платина 1',
			'Алмаз 5', 'Алмаз 4', 'Алмаз 3', 'Алмаз 2', 'Алмаз 1', 'Мастер', 'ГМ'
		],
		en: [
			'Unranked', 'Bronze 5', 'Bronze 4', 'Bronze 3', 'Bronze 2', 'Bronze 1',
			'Silver 5', 'Silver 4', 'Silver 3', 'Silver 2', 'Silver 1',
			'Gold 5', 'Gold 4', 'Gold 3', 'Gold 2', 'Gold 1',
			'Platinum 5', 'Platinum 4', 'Platinum 3', 'Platinum 2', 'Platinum 1',
			'Diamond 5', 'Diamond 4', 'Diamond 3', 'Diamond 2', 'Diamond 1', 'Master', 'GM'
		]
	},
	championsByRoles: { // заменить бы это (удалить)
		androxus: 'flanker',
		ash: 'frontline',
		atlas: 'frontline',
		azaan: 'frontline',
		barik: 'frontline',
		bettylabomba: 'damage',
		bombking: 'damage',
		buck: 'flanker',
		cassie: 'damage',
		corvus: 'support',
		dredge: 'damage',
		drogoz: 'damage',
		evie: 'flanker',
		fernando: 'frontline',
		furia: 'support',
		grohk: 'support',
		grover: 'support',
		imani: 'damage',
		inara: 'frontline',
		io: 'support',
		jenos: 'support',
		khan: 'frontline',
		kinessa: 'damage',
		koga: 'flanker',
		lex: 'flanker',
		lian: 'damage',
		lillith: 'support',
		maeve: 'flanker',
		makoa: 'frontline',
		maldamba: 'support',
		moji: 'flanker',
		octavia: 'damage',
		pip: 'support',
		raum: 'frontline',
		rei: 'support',
		ruckus: 'frontline',
		saati: 'damage',
		seris: 'support',
		shalin: 'damage',
		skye: 'flanker',
		strix: 'damage',
		talus: 'flanker',
		terminus: 'damage',
		tiberius: 'damage',
		torvald: 'frontline',
		tyra: 'damage',
		vatu: 'flanker',
		vii: 'flanker',
		viktor: 'damage',
		vivian: 'damage',
		vora: 'flanker',
		willo: 'damage',
		yagorath: 'frontline',
		ying: 'support',
		zhin: 'flanker',
		caspian: 'flanker',
		kasumi: 'damage',
		nyx: 'frontline',
		omen: 'frontline',
		horse: 'frontline'
	},
	maps: [
		'abyss', 'abyss spire', 'ascension peak', 'bazaar', 'brightmarsh', 'dragon arena', 'fish market', 'foremans rise', 'frog isle', 
		'frozen guard', 'ice mines', 'jaguar falls', 'magistrates archives', 'marauders port', 'primal court', 'serpent beach', 
		'shattered desert', 'shooting range', 'snowfall junction', 'splitstone quarry', 'stone keep day', 'stone keep night', 
		'timber mill', 'trade district', 'tutorial', 'warders gate'
	],
	langToNum: { // для перевода языка в числовой вид
		ru: 11,
		en: 1
	},
	numToLang: { // для перевода языка в буквенный вид
		1: 'en',
		11: 'ru'
	},
	langFlag: {
		ru: '🇷🇺',
		en: '🇺🇸'
	},
	fullnameLang: {
		ru: 'Русский',
		en: 'English'
	},
	example: { // id и ник который будет использован в примерах
		name: 'YourNickname',
		id: 3368378
	}
}