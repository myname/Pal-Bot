/**
 * функция которая выполняет комнаду и отправляет результат пользователю (кнопки)
 */


module.exports = async ({ settings, command, statsFor, branches, selectMenuValues=[], db }) => {
    try {
        const typeValue = new Set(branches)
        const champOpt = typeValue.has('champion')

        const champion = selectMenuValues[0] || (champOpt ? branches[2] : '')
        const messageData = await command.execute({ settings, command, statsFor, champion , db})

        return {
            status: 1,
            messageData
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'sc.button'
        }
    }
}