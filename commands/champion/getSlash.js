/**
 * функция возвращающая обьект слеш-команды
 */


module.exports = (champions) => {
    const supList = champions.filter(ch => ch.role == 'support').map(ch => ({ name: ch.nameEn, value: ch.nameEn }))
    const frontList = champions.filter(ch => ch.role == 'frontline').map(ch => ({ name: ch.nameEn, value: ch.nameEn }))
    const dmgList = champions.filter(ch => ch.role == 'damage').map(ch => ({ name: ch.nameEn, value: ch.nameEn }))
    const flankList = champions.filter(ch => ch.role == 'flanker').map(ch => ({ name: ch.nameEn, value: ch.nameEn }))

    return {
        name: 'champion', // st
        description: 'Displays detailed statistics for the specified champion',
        type: 1,
        options: [
            {
                name: 'nickname',
                description: 'Nickname or id of the player whose stats you want to see',
                type: 3
            },
            {
                name: 'support',
                description: 'Choose a champion from one of the categories (Support)',
                type: 3,
                choices: supList
            },
            {
                name: 'frontline',
                description: 'Choose a champion from one of the categories (Frontline)',
                type: 3,
                choices: frontList
            },
            {
                name: 'flank',
                description: 'Choose a champion from one of the categories (Flank)',
                type: 3,
                choices: flankList
            },
            {
                name: 'damage',
                description: 'Choose a champion from one of the categories (Damage)',
                type: 3,
                choices: dmgList
            }
        ]
    }
}