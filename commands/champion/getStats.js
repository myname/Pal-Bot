/**
 * функция которая получает данные статистик и обрабатывает возможные ошибки связанные с этим
 */
const path = require('path')
const { getUserSavedData } = require(path.join(require.main.path, 'utils', 'index.js'))


module.exports = async (discordUserId, nameOrId, db) => {
    try {
        const { api: hirez } = db
        
        let { playerId, playerName } = await getUserSavedData(nameOrId, discordUserId, true)

        if (!playerId) { // если нет id то делаем запрос для получения id
            const fetchGetplayer = await hirez.getplayer(playerName)
            if (!fetchGetplayer.status) throw fetchGetplayer

            const getplayer = fetchGetplayer.data[0]
            playerId = getplayer?.Id
            if (!getplayer || !playerId) throw {
                status: false,
                err_msg: {
                    ru: 'Аккаунт скрыт.',
                    en: 'The account is hidden.'
                }
            }
        }

        const fetchGetchampionranks = await hirez.getchampionranks(playerId)
        if (!fetchGetchampionranks.status) throw fetchGetchampionranks
        const getchampionranks = fetchGetchampionranks.data // если данных нет то ничего страшного

        return {
            getchampionranks: {
                lastUpdate: fetchGetchampionranks.lastUpdate,
                data: getchampionranks,
                old: fetchGetchampionranks.old
            },
            playerId,
            playerName,
            status: true
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'sc.getStats'
        }
    }
}