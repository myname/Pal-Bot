/**
 * функция обрабатывающая слеш-команду
 */


module.exports = async ({ settings, command, options, db }) => {
    try {
        const statsFor = options.getString('nickname')
        const supportOpt = options.getString('support')
        const frontlineOpt = options.getString('frontline')
        const flankOpt = options.getString('flank')
        const damageOpt = options.getString('damage')
        
        const countParams = [ supportOpt, frontlineOpt, flankOpt, damageOpt ].filter(el => !!el)
        if (countParams.length > 1) throw {
            err_msg: {
                ru: 'Вы должны выбрать только одного чемпиона.',
                en: 'You have to choose only one champion.'
            }
        }

        const champion = supportOpt || frontlineOpt || flankOpt || damageOpt
        const messageData = await command.execute({ settings, command, statsFor, champion, db })

        return {
            status: 1,
            messageData
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'sh.slash'
        }
    }
}