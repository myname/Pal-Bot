/**
 * Скрипт который только рисует указанную инфу в canvas и возвращает ее или ошибку
 */
const path = require('path')
const { createCanvas } = require('canvas')
const { imageNormalize, drawRoundedImage } = require(path.join(require.main.path, 'utils', 'canvas.js'))
const { getChampionsWallpapers, getRandomBackground, getIconBot, formProposals, 
    getChampionIcon } = require(path.join(require.main.path, 'utils', 'index.js'))
const { colors } = require(path.join(require.main.path, 'config', 'index.js'))


/**
 * 
 * @param {*} body - 
 * @param {Object} prop - 
 */
module.exports = async ({ champion, championInfo, lang }) => {
    try {
        const { red, white, black, transparent, green, orange, blue, darkBlue, lightGreen, greyYellow } = colors

        const width = 800
        const height = 450
        const canvas = createCanvas(width, height)
        const ctx = canvas.getContext('2d')
        ctx.font = 'bold 16px Ledger'

        const background = await getChampionsWallpapers(champion.name)
        if (background) {
            const newCanvas = imageNormalize({ img: background, width, height })
            if (newCanvas) ctx.drawImage(newCanvas, 0, 0, width, height) // рисуем
        } else {
            const background = await getRandomBackground()
            const newCanvas = imageNormalize({ img: background, width, height })
            if (newCanvas) ctx.drawImage(newCanvas, 0, 0, width, height) // рисуем
        }

        // фон для круговой статы ака (лвл и опыт)
        ctx.fillStyle = transparent
        ctx.beginPath()
        ctx.rect(36, 105, 71, 48)
        ctx.fill()

        // круг опыта
        ctx.fillStyle = darkBlue
        ctx.beginPath()
        ctx.moveTo(71, 70)
        ctx.arc(71, 70, 60, getRadians(126), 7.208209810736581)
        ctx.closePath()
        ctx.fill()

        // const championExp = champion.parseExp(player.Total_XP)
        const radiusExp = 126 + 287 * champion.exp.progress / 100 // отсчет от 126 идет
        // console.log(`${champion.exp.progress}%`, radiusExp)
        // круг опыта - заполнение
        ctx.fillStyle = lightGreen
        ctx.beginPath()
        ctx.moveTo(71, 70)
        ctx.arc(71, 70, 60, getRadians(126), getRadians(radiusExp))
        ctx.closePath()
        ctx.fill()

        // аватарка
        const avatar = await getChampionIcon(champion.name)
        drawRoundedImage.bind(ctx)(avatar, 50, 72-50, 71-50, 96, 96)

        // процент опыта
        ctx.fillStyle = white
        ctx.font = 'bold 12px Ledger'
        ctx.fillText(`${(+champion.exp.progress).toFixed(0)}%`, 77, 128)

        // лвл ака
        ctx.textAlign = 'center'
        ctx.font = 'bold 20px Ledger'
        ctx.fillText(champion.exp.lvl, 71, 148)

        // фон для базовых данных чемпиона
        ctx.fillStyle = transparent
        ctx.beginPath()
        ctx.rect(150, 10, 640, 55)
        ctx.fill()

        // текст - базовая инфа чемпиона
        ctx.fillStyle = red
        ctx.textAlign = 'start'
        ctx.font = 'bold 18px Ledger'
        ctx.fillText(championInfo?.name, 157, 30)

        ctx.fillStyle = orange
        ctx.fillText(championInfo?.role, 357, 30)

        ctx.fillStyle = white
        ctx.fillText(championInfo?.title, 557, 30)

        ctx.fillStyle = green
        ctx.fillText(`${{ru: 'Жизни', en: 'Health'}[lang]}: ${championInfo?.health}`, 157, 55)

        ctx.fillStyle = greyYellow
        ctx.fillText(`${{ru: 'Скорость', en: 'Speed'}[lang]}: ${championInfo?.speed}`, 357, 55)

        // фон для статы чемпиона
        ctx.fillStyle = transparent
        ctx.beginPath()
        ctx.rect(150, 80, 640, 110)
        ctx.fill()

        ctx.font = 'bold 20px Ledger'
        ctx.fillStyle = white
        // ctx.fillStyle = green
        ctx.fillText(`${{ru: 'Убийств', en: 'Kills'}[lang]}: ${champion?.Kills.goDot()}`, 157, 105)
        // ctx.fillStyle = red
        ctx.fillText(`${{ru: 'Смертей', en: 'Deaths'}[lang]}: ${champion?.Deaths.goDot()}`, 157, 130)
        // ctx.fillStyle = greyYellow
        ctx.fillText(`${{ru: 'Помощи', en: 'Assists'}[lang]}: ${champion?.Assists.goDot()}`, 157, 155)
        // ctx.fillStyle = orange
        ctx.fillText(`K/D/A: ${champion?.kda}`, 157, 180)
        // ctx.fillStyle = green
        ctx.fillText(`${{ru: 'Побед', en: 'Wins'}[lang]}: ${champion?.Wins.goDot()}`, 490, 105)
        // ctx.fillStyle = red
        ctx.fillText(`${{ru: 'Поражений', en: 'Losses'}[lang]}: ${champion?.Losses.goDot()}`, 490, 130)
        // ctx.fillStyle = greyYellow
        ctx.fillText(`${{ru: 'Винрейт', en: 'Winrate'}[lang]}: ${champion?.winrate}%`, 490, 155)

        const textArr = formProposals(championInfo?.lore, lang == 'ru' ? 87 : 94) // 15 - 83
        if ( textArr && textArr?.length ) {
            // фон для статы чемпиона
            ctx.fillStyle = transparent
            ctx.beginPath()
            ctx.rect(10, 200, 780, 16 * textArr.length + 20)
            ctx.fill()
    
            ctx.fillStyle = white
            ctx.font = 'bold 16px Ledger'
            for (let i = 0; i < textArr.length; i++) {
                const y = 220 + 16 * i
                const spacePoop = String.fromCharCode(160)
                const text = textArr[i].replaceAll(new RegExp(`${spacePoop}`, 'g'), ' ')
                ctx.fillText(text, 18, y)
            }
        }

        // рисуем иконку бота
        const icon = await getIconBot()
        if (icon) ctx.drawImage(icon, width - 62, height - 65, 60, 60)

        // фон для текста снизу (бота)
        ctx.fillStyle = transparent
        ctx.beginPath()
        ctx.rect(75, height - 34, width - 75 - 75, 34)
        ctx.fill()

        // текст снизу (бота)
        ctx.fillStyle = white
        ctx.font = 'bold 18px Ledger'
        ctx.textAlign = 'end'
        ctx.fillText({
            ru: 'Pal-Bot - Лучшая статистика по Paladins в наилучшем виде',
            en: 'Pal-Bot - The best statistics on Paladins in the best possible way'
        }[lang], width - 75 - 12, height - 34 + 25)

        return {
            status: true,
            canvas
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            status: false,
            error,
            err_msg: {
                ru: 'Ошибка при обработке данных. Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Error in data processing. Try again or report this error to the bot creator.'
            },
            log_msg: 'Ошибка функции "sc.draw"'
        }
    }
}


function getRadians(degrees) {return (Math.PI / 180) * degrees}