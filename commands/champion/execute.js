/**
 * функция которая выполняет комнаду и отправляет результат пользователю
 */
const path = require('path')
const { ActionRowBuilder, ButtonBuilder, StringSelectMenuBuilder } = require('discord.js')

const ChampionsManager = require(path.join(require.main.path, 'classes', 'ChampionsManager.js'))
const ChampionsStats = require(path.join(require.main.path, 'classes', 'ChampionsStats.js'))
const { news, langToNum, championsByRoles } = require(path.join(require.main.path, 'config', 'index.js'))
const { resolveName } = require(path.join(require.main.path, 'utils', 'discord.js'))


module.exports = async ({ settings, command, statsFor, champion, db }) => {
    try {
        const { id: discordUserId, lang } = settings.user
        const nameOrId = resolveName(statsFor || discordUserId)

        const body = await command.getStats(discordUserId, nameOrId, db)
        const { getchampionranks } = body

        const championsStats = new ChampionsStats(getchampionranks.data)
        if (championsStats.error) throw {
            body,
            err_msg: {
                ru: 'Чемпионы не найдены.',
                en: 'No champions found.'
            }
        }

        const champions = new ChampionsManager( db, lang )

        const buttonsLine_1 = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`menu_${discordUserId}_${nameOrId}`)
            .setLabel({en: 'Menu', ru: 'Меню'}[lang])
            .setStyle('Danger')
            .setEmoji('<:menu:943824092635758632>')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`champion_${discordUserId}_${nameOrId}${champion ? `_champion__${champion}` : ''}`)
            .setLabel({en: 'Refresh', ru: 'Обновить'}[lang])
            .setStyle('Success')
            .setEmoji('<:refresh_mix:943814451226873886>')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`champions_${discordUserId}_${nameOrId}`)
            .setLabel({en: 'Champions', ru: 'Чемпионы'}[lang])
            .setStyle('Secondary')
            .setEmoji('<:champions:943447650647310356>')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`deck_${discordUserId}_${nameOrId}${champion ? `_card__${champion}` : ''}`)
            .setLabel({en: 'Decks', ru: 'Колоды'}[lang])
            .setStyle('Secondary')
            .setEmoji('<:cards:943453491907661845>')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`builddeck_${discordUserId}_${nameOrId}`)
            .setLabel({en: 'Build a deck', ru: 'Создать колоду'}[lang])
            .setStyle('Secondary')
            .setEmoji('<:cards:943453491907661845>')
            .setDisabled(true)
        )

        const roleIcons = {
            flanker: '<:flank:943440471823360010>',
            damage: '<:damage:943440471554924554>',
            support: '<:support:943440471924023328>',
            frontline: '<:frontline:943440471743672320>'
        }
        const championsFilterOpts = []
        champions.each((champion, i) => {
            const count = Math.floor(i / 25) // массив по счету
            if (!(i % 25)) championsFilterOpts.push([]) // новый массив

            const champStats = championsStats.getByName(champion.nameEn)
            if (!champStats) return; // если нет статы то зачем показывать тут

            const description = {
                en: `Lvl: ${champStats.exp.lvl} | KDA: ${champStats.kda} | Winrate: ${Math.floor(champStats.winrate)}%`, 
                ru: `Уровень: ${champStats.exp.lvl} | KDA: ${champStats.kda} | Винрейт: ${Math.floor(champStats.winrate)}%`
            }

            championsFilterOpts[count].push({
                label: { en: champion.nameEn, ru: champion.name }[lang],
                description: description[lang],
                value: champion.nameEn,
                emoji: roleIcons[championsByRoles[ChampionsStats.nameNormalize(champion.nameEn)]]
            })
        })
        const championsOpt = []
        championsFilterOpts.forEach((optList, i) => {
            championsOpt.push(
                new ActionRowBuilder()
                .addComponents(
                    new StringSelectMenuBuilder()
                        .setCustomId(`champion_${discordUserId}_${nameOrId}_champion_${i}`)
                        .setPlaceholder({en: 'Show champion statistics', ru: 'Показать статистику чемпиона'}[lang])
                        .addOptions(optList)
                )
            )
        })

        if (!champion) {
            return {
                content: `${news[lang]}${{
                    ru: `Выберите чемпиона:`,
                    en: `Choose a champion:`
                }[lang]}`,
                components: [ buttonsLine_1, ...championsOpt ]
            }
        }

        const drawChampion = championsStats.getByName(champion)
        if (!drawChampion) return {
            content: `${news[lang]}${{
                ru: `У вас нет статистики ${champion}.`,
                en: `You have no statistics ${champion}.`
            }[lang]}`,
            components: [ buttonsLine_1, ...championsOpt ]
        }

        const championInfo = champions.getByName(champion)
        const draw = await command.draw({ champion: drawChampion, championInfo, lang })
        if (!draw.status) throw draw

        const canvas = draw.canvas
        const buffer = canvas.toBuffer('image/png') // buffer image

        const showOldStatsText = {
            ru: 'Аккаунт скрыт или API временно не работает.\n__**Вам будут показаны данные последнего удачного запроса.**__',
            en: 'The account is hidden or the API is temporarily not working.\n__**You will be shown the details of the last Successful request.**__'
        }

        const replayOldText = body.getchampionranks.old ?
                `${showOldStatsText[lang]}\n` : ''

        const matchesInfo = `\`\`\`\n[${body.playerName}](${body.playerId})<${champion?.name?.en||''}>\`\`\``

        return {
            content: `${news[lang]}${replayOldText}${matchesInfo}`,
            components: [ buttonsLine_1, ...championsOpt ],
            files: [{
                attachment: buffer,
                name: `${nameOrId}.png`
            }]
        }
    } catch (error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Ошибка при формировании ответа. Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Error when forming the response. Try again or report this error to the bot creator.'
            },
            log_msg: 'Какая-то непредвиденная ошибка поидее "sc.execute"'
        }
    }
}