/**
 * функция которая выполняет комнаду и отправляет результат пользователю
 */
const path = require('path')
const championsData = require(path.join(require.main.path, 'updater', 'championsData.js'))
const clearDB = require(path.join(require.main.path, 'updater', 'clearDB.js'))
const setSlash = require(path.join(require.main.path, 'updater', 'setSlash.js'))


module.exports = async ({ contentParams, client, db }) => {
    // команда (слеш) должна будет ставиться только на сервере бота или другом сервере предназначенном для этого
    try {
        if (contentParams?.startsWith('champions')) { // обновление чемпионов
            await championsData(db)
            return { content: 'champions OK' }
        }

        if (contentParams?.startsWith('cleardb')) { // очистка БД
            const removeItems = contentParams?.startsWith('cleardbItem')
            await clearDB(removeItems, db)
            return { content: 'clearDB OK' }
        }

        if (contentParams?.startsWith('slash')) { // обновление слеш-команд
            const res = await setSlash(client, db)
            if (res) return { content: 'slash OK' }
            return { content: 'slash ERR' }
        }

        const data = await new Promise(resolve => {
            eval(`const result = (async function() {${contentParams}})();resolve(result)`)
        })

        return data || { content : 'ok' }
    } catch(error) {
        console.log(error)
        return { content : 'error' }
    }
}