/**
 * функция обрабатывающая команду (устаревшие)
 */


module.exports = async ({ settings, command, contentParams, db }) => {
    try {
        const [ statsFor, typeSort ] = contentParams.split(' ')
        const typeSortList = { // виды сортировок
            lvl: ['lvl', 'level', 'лвл', 'уровень'],
            winrate: ['winrate', 'винрейт'],
            time: ['time', 'время'],
            kda: ['kda', 'кда'],
            // match: ['match', 'matches', 'game', 'games', 'матч', 'матчи', 'игра', 'игры']
        }

        const getTypeSort = typeSort === undefined || typeSort === '' ? 'lvl' :
            typeSortList.find((typeName, typeList) => {
            return typeList.find(type => type === typeSort) ? typeName : false
        })

        if (!getTypeSort) throw {
            err_msg: {
                ru: 'Указан неверный тип сортировки.',
                en: 'Invalid sort type specified.'
            }
        }

        return await command.execute({ settings, command, statsFor, typeSort: getTypeSort, db })
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'st.command'
        }
    }
}