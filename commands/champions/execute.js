/**
 * функция которая выполняет комнаду и отправляет результат пользователю
 */
const path = require('path')
const { ActionRowBuilder, ButtonBuilder, StringSelectMenuBuilder } = require('discord.js')

const ChampionsStats = require(path.join(require.main.path, 'classes', 'ChampionsStats.js'))
const { news } = require(path.join(require.main.path, 'config', 'index.js'))
const { resolveName } = require(path.join(require.main.path, 'utils', 'discord.js'))


module.exports = async ({ settings, command, statsFor, typeSort, db }) => {
    try {
        const { id: discordUserId, lang } = settings.user
        const nameOrId = resolveName(statsFor || discordUserId)

        const body = await command.getStats(discordUserId, nameOrId, db)
        const { getchampionranks, playerId, playerName } = body

        const champions = new ChampionsStats(getchampionranks.data)
        if (champions.error) throw {
            body,
            err_msg: {
                ru: 'Чемпионы не найдены.',
                en: 'No champions found.'
            }
        }

        // сортируем
        champions.sortType(typeSort)

        // рисуем
        const draw = await command.draw({ champions, lang })
        if (!draw.status) throw draw

        const canvas = draw.canvas
        const buffer = canvas.toBuffer('image/png') // buffer image

        const showOldStatsText = {
            ru: 'Аккаунт скрыт или API временно не работает.\n__**Вам будут показаны данные последнего удачного запроса.**__',
            en: 'The account is hidden or the API is temporarily not working.\n__**You will be shown the details of the last successful request.**__'
        }

        const replayOldText = getchampionranks.old ?
                `${showOldStatsText[lang]}\n` : ''

        const matchesInfo = `\`\`\`\n[${playerName}](${playerId})<${typeSort||''}>\`\`\``

        const buttonsLine_1 = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`menu_${discordUserId}_${nameOrId}`)
            .setLabel({en: 'Menu', ru: 'Меню'}[lang])
            .setStyle('Danger')
            .setEmoji('<:menu:943824092635758632>')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`champions_${discordUserId}_${nameOrId}`)
            .setLabel({en: 'Refresh', ru: 'Обновить'}[lang])
            .setStyle('Success')
            .setEmoji('<:refresh_mix:943814451226873886>')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`champion_${discordUserId}_${nameOrId}`)
            .setLabel({en: 'Choose a champion', ru: 'Выбрать чемпиона'}[lang])
            .setStyle('Secondary')
            .setEmoji('<:champion:943440471601061888>')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`deck_${discordUserId}_${nameOrId}`)
            .setLabel({en: 'Decks', ru: 'Колоды'}[lang])
            .setStyle('Secondary')
            .setEmoji('<:cards:943453491907661845>')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`random_${discordUserId}_${nameOrId}`)
            .setLabel({ en: 'Random champion', ru: 'Случайный чемпион' }[lang])
            .setStyle('Secondary')
            .setDisabled(true)
        )

        const buttonsLine_2 = new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
                .setCustomId(`champions_${discordUserId}_${nameOrId}_sort`)
                .setPlaceholder({en: 'Sorted by...', ru: 'Сортировать по...'}[lang])
                .addOptions([
                    {
                        label: {en: 'Level', ru: 'Уровню'}[lang],
                        description: {en: 'Sort by level', ru: 'Сортировать по уровню'}[lang],
                        value: 'lvl',
                        emoji: '<:lvl:943865190452178984>'
                    },
                    {
                        label: {en: 'Winrate', ru: 'Винрейт'}[lang],
                        description: {en: 'Sort by winrate', ru: 'Сортировать по винрейту'}[lang],
                        value: 'winrate',
                        emoji: '<:stats:943819417131839501>'
                    },
                    {
                        label: {en: 'Time', ru: 'Времени'}[lang],
                        description: {en: 'Sort by the time of the game on the champion', ru: 'Сортировать по времени игры на чемпионе'}[lang],
                        value: 'time',
                        emoji: '<:time:943836999771623475>'
                    },
                    {
                        label: {en: 'K/D/A', ru: 'K/D/A'}[lang],
                        description: {en: 'Sort by K/D/A', ru: 'Сортировать по K/D/A'}[lang],
                        value: 'kda',
                        emoji: '<:rip:943864073248993290>'
                    }
                ])
        )

        return {
            content: `${news[lang]}${replayOldText}${matchesInfo}`,
            components: [ buttonsLine_1, buttonsLine_2],
            files: [{
                attachment: buffer,
                name: `${nameOrId}.png`
            }]
        }
    } catch (error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'Какая-то непредвиденная ошибка поидее'
        }
    }
}