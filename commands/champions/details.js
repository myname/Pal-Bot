/**
 * функция возвращающая текст полного описания команды с примерами их работы
 */


module.exports = function(command, lang) {
    const path = require('path')
    const { slogan, youtube: { champions: videoURL }, gif: { champions: gif } } = require(path.join(require.main.path, 'config', 'index.js'))
    const { EMPTY_ICON, BOT_ICON } = process.env

    const embed = {
        title: {
            ru: '`Смотреть пример на ютубе`',
            en: '`Watch an example on YouTube`'
        }[lang],
        url: videoURL,
        image: {
            url: gif
        },
        description: {
            ru: `**${command.info[lang]}.**\n</champions:884702180207124499> \`nickname\` \`platform\` \`\`\`md\n> Параметры:\`\`\``,
            en: `**${command.info[lang]}.**\n</champions:884702180207124499> \`nickname\` \`platform\` \`\`\`md\n> Params:\`\`\``
        }[lang],
        color: '3092790',
        footer: {
            icon_url: BOT_ICON,
            text: slogan[lang]
        },
        author: {
            name: `/${command.name}`,
            icon_url: EMPTY_ICON
        },
        fields: [
            {
                name: 'nickname:',
                value: {
                    ru: `\`\`\`css\n[Ник или id игрока, статистику которого вы хотите увидеть].\nВведите сюда никнейм ` + 
                        `игрока, чью статистику хотите посмотреть.\`\`\``,
                    en: `\`\`\`css\n[Nickname or id of the player whose stats you want to see].\nEnter here the nickname ` + 
                        `of the player whose statistics you want to see.\`\`\``
                }[lang]
            },
            {
                name: 'sorted:',
                value: {
                    ru: `\`\`\`css\n[Сортирует чемпионов указанным способом].\nДает возможность отсортировать чемпионов по ` + 
                    `указанному критерию.\`\`\``,
                    en: `\`\`\`css\n[Sorts the champions in the specified way].\nMakes it possible to sort champions by ` + 
                    `the specified criterion.\`\`\``
                }[lang]
            }
        ]
    }

    return {
        embeds: [ embed ],
        ephemeral: true
    }
}