/**
 * функция которая выполняет комнаду и отправляет результат пользователю (кнопки)
 */


module.exports = async ({ command, selectMenuValues=[], statsFor, branches, settings, db }) => {
    try {
        const typeValue = new Set(branches)
        const typeSort = typeValue.has('sort') ? selectMenuValues[0] : 'lvl'
        const messageData =  await command.execute({ settings, command, statsFor, typeSort, db })

        return {
            status: 1,
            messageData
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'st.button'
        }
    }
}