/**
 * функция возвращающая обьект слеш-команды
 */


module.exports = () => {
    return {
        name: 'champions', // st
        description: 'Displays top champions with sorting options',
        type: 1,
        options: [
            {
                name: 'nickname',
                description: 'Nickname or id of the player whose stats you want to see',
                type: 3
            },
            {
                name: 'sorted',
                description: 'Sorts the champions in the specified way',
                type: 3,
                choices: [
                    {
                        name: 'Lvl',
                        value: 'lvl'
                    },
                    {
                        name: 'Winrate',
                        value: 'winrate'
                    },
                    {
                        name: 'Playing time',
                        value: 'time'
                    },
                    {
                        name: 'KDA',
                        value: 'kda'
                    }
                ]
            }
        ]
    }
}