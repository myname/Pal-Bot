/**
 * Скрипт который только рисует указанную инфу в canvas и возвращает ее или ошибку
 */


/**
 * 
 * @param {*} body - 
 * @param {Object} prop - 
 */
module.exports = async ({ champions, lang }) => {
    try {
        const path = require('path')
        const { createCanvas } = require('canvas')
        const { getIconBot, getRandomBackground } = require(path.join(require.main.path, 'utils', 'index.js'))
        const { imageNormalize } = require(path.join(require.main.path, 'utils', 'canvas.js'))
        const { colors } = require(path.join(require.main.path, 'config', 'index.js'))
        const { red, white, blue, black, orange, green, yellow, transparent } = colors

        const len = champions.size
        const width = 1060
        const height = 22 * len / 2 + 100
        const canvas = createCanvas(width, height)
        const ctx = canvas.getContext('2d')
        const paddingLeft = width / 2 // отступ до второй колонки
        ctx.font = 'bold 16px Ledger'

        const background = await getRandomBackground()
        if (background) {
            const newCanvas = imageNormalize({ img: background, width, height: height - 105 })
            if (newCanvas) ctx.drawImage(newCanvas, 0, 30, width, height - 75) // рисуем
        }

        // прозрачный фон
        ctx.fillStyle = transparent
        ctx.fillRect(0, 0, width, height)

        // черные полосы сверху и снизу
        ctx.fillStyle = black
        ctx.fillRect(0, 0, width, 30)
        ctx.fillRect(0, height - 45, width, 45)

        ctx.fillStyle = blue
        ctx.fillRect(paddingLeft - 2, 30, 2, height - 75)
        // const lastUpdate = last_update.updateToDate(timezone).toText()
        // const nextUpdate = last_update.getNextUpdate('getchampionranks', timezone)
        // const champText = `${translate.Champions[lang]}: ${lastUpdate} | ${translate.Update[lang]}: ${nextUpdate}`
        // ctx.fillStyle = red
        // ctx.fillText(champText, 20, height - 10)
        const text = {
            ru: 'Pal-Bot - Лучшая статистика по паладинс в наилучшем виде',
            en: 'Pal-Bot - The best statistics on paladins in the best possible way'
        }
        ctx.font = 'bold 24px Ledger'
        ctx.fillText(text[lang], 53, height - 15)
        ctx.font = 'bold 16px Ledger'

        const icon = await getIconBot()
        if (icon) ctx.drawImage(icon, 10, height - 40, 35, 35) // рисуем

        // рисуем заголовки таблицы
        ctx.fillStyle = blue
        for (let i = 0; i < 2; i++) {
            ctx.fillText(`№`, 10 + paddingLeft * i, 20)
            ctx.fillText({ ru: 'Чемпион', en: 'Champion' }[lang], 45 + paddingLeft * i, 20)
            ctx.fillText(`Lvl`, 180 + paddingLeft * i, 20)
            ctx.fillText({ ru: 'Минуты', en: 'Minutes' }[lang], 230 + paddingLeft * i, 20)
            ctx.fillText({ ru: 'Винрейт', en: 'Winrate' }[lang], 320 + paddingLeft * i, 20) // дать больше места ..!!!!!!
            ctx.fillText(`KDA`, 480 + paddingLeft * i, 20)
        }

        // рисуем стату персонажей
        champions.each((champion, i) => {
            const j =  Math.round(len / 2) // половина len в большую сторону
            const jj = Math.floor(len / 2) // половина len в меньшую сторону
            const padding = jj < i ? paddingLeft : 0 // распределение лево-право
            const k = jj < i ? i - j : i
            const paddingTop = 22 * k + 50

            if (!champion || !champion.name) return;

            ctx.fillStyle = white
            ctx.fillText(champion.Name, 45 + padding, paddingTop)
            ctx.fillStyle = green
            ctx.fillText(champion.exp.lvl, 180 + padding, paddingTop)
            ctx.fillStyle = yellow
            ctx.fillText(champion.Minutes.goDot(), 230 + padding, paddingTop)
            ctx.fillStyle = blue
            ctx.fillText(`${champion.winrate}%`, 320 + padding, paddingTop)
            ctx.fillStyle = red
            ctx.fillText(`${champion.Wins}/${champion.Losses}`, 390 + padding, paddingTop)
            ctx.fillStyle = orange
            ctx.fillText(champion.kda, 480 + padding, paddingTop)
            ctx.fillText(`${i + 1}.`, 10 + padding, paddingTop)
        })

        return {
            status: true,
            canvas
        }
    } catch(error) {
        if ('err_msg' in error) throw error // проброс ошибки если есть описание
        throw {
            status: false,
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'Ошибка функции "st.draw"'
        }
    }
}