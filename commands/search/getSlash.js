/**
 * функция возвращающая обьект слеш-команды
 */


module.exports = () => {
    return {
        name: 'search',
        description: 'Search for accounts of consoles and other platforms',
        options: [
            {
                name: 'nickname',
                description: 'Nickname or part of the nickname of the player you want to find',
                type: 3,
                required: true
            }
        ]
    }
}