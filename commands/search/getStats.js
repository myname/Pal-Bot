/**
 * функция которая получает данные статистик и обрабатывает возможные ошибки связанные с этим
 */


module.exports = async (nameOrId, db) => {
    try {
        const { api: hirez } = db

        const fetchSearchplayers = await hirez.searchplayers(nameOrId)
        if (!fetchSearchplayers.status) throw fetchSearchplayers
        const { data: searchplayers } = fetchSearchplayers // если данных нет то ничего страшного

        return {
            searchplayers: {
                lastUpdate: fetchSearchplayers.lastUpdate,
                data: searchplayers,
                old: fetchSearchplayers.old
            },
            status: true
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'se.getStats'
        }
    }
}