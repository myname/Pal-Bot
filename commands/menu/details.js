/**
 * функция возвращающая текст полного описания команды с примерами их работы
 */


module.exports = function(command, lang) {
    const path = require('path')
    const { slogan, youtube: { menu: videoURL }, gif: { menu: gif } } = require(path.join(require.main.path, 'config', 'index.js'))
    const { EMPTY_ICON, BOT_ICON } = process.env

    const embed = {
        title: {
            ru: '`Смотреть пример на ютубе`',
            en: '`Watch an example on YouTube`'
        }[lang],
        url: videoURL,
        image: {
            url: gif
        },
        description: {
            ru: `**${command.info[lang]}.**\`\`\`\n[/menu 'nickname']\`\`\` \`\`\`\n-Параметры:\`\`\``,
            en: `**${command.info[lang]}.**\`\`\`\n[/menu 'nickname']\`\`\` \`\`\`\n-Params:\`\`\``
        }[lang],
        color: '3092790',
        footer: {
            icon_url: BOT_ICON,
            text: slogan[lang]
        },
        author: {
            name: `/${command.name}`,
            icon_url: EMPTY_ICON
        },
        fields: [
            {
                name: 'nickname:',
                value: {
                    ru: `\`\`\`\n[Ник или id игрока, меню для которого вы хотите открыть].\nВведите сюда никнейм ` + 
                        `игрока, чью статистику хотите смотреть.\`\`\``,
                    en: `\`\`\`\n[Nickname or id of the player whose menu you want to open].\nEnter here the nickname ` + 
                        `of the player whose statistics do you want to see.\`\`\``
                }[lang]
            }
        ]
    }

    return {
        embeds: [ embed ],
        ephemeral: true
    }
}