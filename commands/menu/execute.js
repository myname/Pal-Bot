/**
 * функция которая выполняет комнаду принимая данные
 */
const path = require('path')
const { ActionRowBuilder, ButtonBuilder } = require('discord.js')

const { news } = require(path.join(require.main.path, 'config', 'index.js'))
const { DISCORD_INVITE } = process.env

const { getSteamData } = require(path.join(require.main.path, 'utils', 'requestFunctions.js'))
const { resolveName } = require(path.join(require.main.path, 'utils', 'discord.js'))


module.exports = async ({ settings, contentParams, db }) => {
    try {
        const { id: discordUserId, lang, params={} } = settings.user
        const nameOrId = resolveName(contentParams || discordUserId)

        const steamCountPlayers = await getSteamData()
        const steamInfo = {
            ru: `**Онлайн Steam игроков: ${steamCountPlayers}.**`,
            en: `**Online Steam Players: ${steamCountPlayers}.**`
        }

        const buttonsLine_1 = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`setting_${discordUserId}_${nameOrId}`)
            .setLabel({ en: 'Settings', ru: 'Настройки' }[lang])
            .setStyle('Secondary')
            .setEmoji('🛠️')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`menu_${discordUserId}_${nameOrId}`)
            .setLabel({ en: 'Refresh', ru: 'Обновить' }[lang])
            .setStyle('Success')
            .setEmoji('<:refresh_mix:943814451226873886>')
        )
        // .addComponents(
        //     new ButtonBuilder()
        //     .setURL(config.siteUrl)
        //     .setLabel({ en: 'Site Stats', ru: 'Сайт статистики' }[lang])
        //     .setStyle('Link')
        //     .setDisabled(true)
        // )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`help_${discordUserId}_${nameOrId}`)
            .setLabel({ en: 'Help', ru: 'Помощь' }[lang])
            .setStyle('Danger')
            .setEmoji('❔')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`bot_${discordUserId}_${nameOrId}`)
            .setLabel({ en: 'Bot statistics', ru: 'Статистика бота' }[lang])
            .setStyle('Secondary')
            .setEmoji('🤖')
        )
        .addComponents(
            new ButtonBuilder()
            .setURL(DISCORD_INVITE)
            .setLabel({ en: 'Bot server', ru: 'Сервер бота' }[lang])
            .setStyle('Link')
        )

        const consoleStats = params?.ss?.console || false
        const buttonsLine_2 = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`stats_${discordUserId}_${nameOrId}` + (consoleStats ? '_console' : ''))
            .setLabel({ en: 'Account stats', ru: 'Статистика аккаунта' }[lang])
            .setStyle('Secondary')
            .setEmoji('<:stats:943819417131839501>')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`current_${discordUserId}_${nameOrId}`)
            .setLabel({ en: 'Current', ru: 'Статус в игре' }[lang])
            .setStyle('Secondary')
            .setEmoji('<:current:943440471680753694>')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`history_${discordUserId}_${nameOrId}`)
            .setLabel({ en: 'History', ru: 'История' }[lang])
            .setStyle('Secondary')
            .setEmoji('<:history:943818397009985597>')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`last_${discordUserId}_${nameOrId}`)
            .setLabel({ en: 'Last match', ru: 'Последний матч' }[lang])
            .setStyle('Secondary')
            .setEmoji('<:match:943925118286069781>')
        )

        const buttonsLine_3 = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`champions_${discordUserId}_${nameOrId}`)
            .setLabel({ en: 'Champions', ru: 'Чемпионы' }[lang])
            .setStyle('Secondary')
            .setEmoji('<:champions:943447650647310356>')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`champion_${discordUserId}_${nameOrId}`)
            .setLabel({ en: 'Select champion', ru: 'Выбрать чемпиона' }[lang])
            .setStyle('Secondary')
            .setEmoji('<:champion:943440471601061888>')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`random_${discordUserId}_${nameOrId}`)
            .setLabel({ en: 'Random champion', ru: 'Случайный чемпион' }[lang])
            .setStyle('Secondary')
            .setDisabled(true)
        )

        const buttonsLine_4 = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`friends_${discordUserId}_${nameOrId}`)
            .setLabel({ en: 'Friends', ru: 'Друзья' }[lang])
            .setStyle('Secondary')
            .setEmoji('<:friends:943449946428960798>')
        )
        // .addComponents(
        //     new ButtonBuilder()
        //     .setCustomId(`servers_${discordUserId}_${nameOrId}`)
        //     .setLabel({ en: 'Find someone to play with', ru: 'Найти с кем играть' }[lang])
        //     .setStyle('Success')
        //     .setEmoji('👥')
        //     .setDisabled(true)
        // )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`deck_${discordUserId}_${nameOrId}`)
            .setLabel({ en: 'Decks', ru: 'Колоды' }[lang])
            .setStyle('Secondary')
            .setEmoji('<:cards:943453491907661845>')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`builddeck_${discordUserId}_${nameOrId}`)
            .setLabel({en: 'Build a deck', ru: 'Создать колоду'}[lang])
            .setStyle('Secondary')
            .setEmoji('<:cards:943453491907661845>')
            .setDisabled(true)
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`quests_${discordUserId}_${nameOrId}`)
            .setLabel({en: 'Quests (give away)', ru: 'Квесты (раздача)'}[lang])
            .setStyle('Success')
            // .setEmoji('')
            .setDisabled(true)
        )

        return {
            content: `${news[lang]}\n${steamInfo[lang]}`,
            components: [buttonsLine_1, buttonsLine_2, buttonsLine_3, buttonsLine_4]
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'pal.execute'
        }
    }
}