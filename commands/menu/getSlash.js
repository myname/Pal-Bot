/**
 * функция возвращающая обьект слеш-команды
 */


module.exports = () => {
    return {
        name: 'menu',
        description: 'Command menu',
        options: [{
            name: 'nickname',
            description: 'Nickname or id of the player whose menu you want to open',
            type: 3
        }]
    }
}