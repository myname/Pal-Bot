/**
 * функция возвращающая текст полного описания команды с примерами их работы
 */
const path = require('path')
const { slogan, youtube: { search: searchVideo, menu: menuVideo, setting: settingVideo, an: anVideo } } = require(path.join(require.main.path, 'config', 'index.js'))
const { EMPTY_ICON, BOT_ICON, DISCORD_INVITE } = process.env


module.exports = function(command, lang) {
    const embed = {
        title: 'Bot prefix: `!`\n' + {
            ru: 'Вы также можете использовать текстовые команды, их названия аналогичны слеш-командам.\nНапример: `!stats mutu`',
            en: 'You can also use text commands, their names are similar to slash commands.\nFor example: `!stats mutu`'
        }[lang],
        // url: 'https://codeberg.org/myname/Pal-Bot',
        description: {
            ru: `\`\`\`\n-Проявите чуточку терпения. Когда знаешь как это работает, то все кажется простым и удобным.\n-Не стесняйтесь задавать вопросы создателю бота.\`\`\`` + 
                `**[Политика приватности - смотреть пример на ютубе](${settingVideo})**\`\`\`\n['Политика приватности']\nПри использовании команды [/settings] и [/me] в базу данных бота будут записаны соответствующие настройки, а также ваш ID дискорда. Вы можете в любой момент самостоятельно удалить все данные о себе из баз данных бота. Для этого используйте команду [/settings 'For me'] и нажмите кнопку ['Удалить настройки'] - для удаления всех настроек, либо если вы хотите удалить свои сохраненные никнеймы то нажмите кнопку ['Сохраненные никнеймы'], после чего там появится кнопка для удаления ваших никнеймов - ['Удалить данные'].\`\`\`` + 
                `**[Консольные игроки - смотреть пример на ютубе](${searchVideo})**\`\`\`\n['Консольные игроки']\nДля просмотра статистики консольных аккаунтов используйте команду [/stats 'platform':'Console']\nЕсли же вы хотите сохранить консольный аккаунт с помощью команды [/me], то вам сначала нужно найти ID этого аккаунта с помощью команды [/search].\`\`\`` + 
                `**[Логика работы бота - смотреть пример на ютубе](${menuVideo})**\`\`\`\n['Логика работы бота']\n1. Отвечая на команды бот запоминает того, кому он ответил в каждом сообщении и в дальнейшем будет реагировать только на клики от вызвавшего это сообщение человека.\n2. Если кнопку бота использует человек, не являющийся владельцем того сообщения, то бот ответит ему создав новое сообщение и подставив туда все те данные которые были в использованной кнопке (это новое сообщение будет "принадлежать" тому кто кликнул).\n3. Вместо ['me'] вы можете подставить данные другого человека, если он их сохранил, просто упомянув его. Например [/stats 'nickname':'@my name'].\`\`\`` + 
                `**[Контент сообщений - смотреть пример на ютубе](${anVideo})**\`\`\`\n['Контент сообщений']\n1. Контент сообщений требуется для улиты анализа слов.\n2. Слова будут обрабатываться и анализироваться только если вы сами нажмете на кнопку в настройках, тем самым давая согласие на анализ контента сообщений.\n3. Все данные можно удалить самостоятельно, нажав кнопку для удаления данных (смотрите видео).\n4. Вы также можете получить помощь вступив на сервер, не стоитисняйтесь задавать вопросы в ЛС разработчику бота!\`\`\`` + 
                `**[ПРИСОЕДИНИТЬСЯ К СЕРВЕРУ](${DISCORD_INVITE})**` + 
                `\`\`\`\n-Нажмите на выпадающий список, расположенный чуть ниже, для просмотра команд и помощи по ним:\`\`\``,
            en: `\`\`\`\n-Show a little patience. When you know how it works, everything seems simple and convenient.\n-Feel free to ask questions to the creator of the bot.\`\`\`` +  
                `**[Privacy policy - watch an example on YouTube](${settingVideo})**\`\`\`\n['Privacy policy']\nWhen using the command [/settings] and [/me] the corresponding settings will be recorded in the bot database, as well as your discord ID. You can delete all data about yourself from the bot's databases at any time. To do this, use the command [/settings 'For me'] and press the button ['Delete settings'] - to delete all settings, or if you want to delete your saved nicknames then click ['Saved nicknames'], after that, there will be a button to delete your nicknames - ['Delete data'].\`\`\`` + 
                `**[Console players - watch an example on YouTube](${searchVideo})**\`\`\`\n['Console players']\nTo view the statistics of console accounts, use the command [/stats 'platform':'Console']\nIf you want to save a console account using the command [/me], then you first need to find the ID of this account using the command [/search].\`\`\`` + 
                `**[The logic of the bot - watch an example on YouTube](${menuVideo})**\`\`\`\n['The logic of the bot']\n1. Responding to commands, the bot remembers who it answered in each message and in the future will respond only to clicks from the person who caused this message.\n2. If the bot button is used by a person who is not the owner of that message, the bot will respond to him by creating a new message and substituting there all the data that was in the used button (this new message will "belong" to the one who clicked)\n3. Instead of ['me'], you can substitute the data of another person, if he saved them, by simply mentioning him. For example [/stats 'nickname':'@my name'].\`\`\`` + 
                `**[Message content - watch an example on YouTube](${anVideo})**\`\`\`\n['Message content']\n1. The content of the messages is required for word analysis.\n2. The words will be processed and analyzed only if you click on the button in the settings yourself, thereby agreeing to the analysis of the content of the messages.\n3. You can delete all the data yourself by clicking the delete data button (watch the video).\n4 You can also get help by joining the server, do not hesitate to ask questions in the DM to the bot developer!\`\`\`` + 
                `**[JOIN THE SERVER](${DISCORD_INVITE})**` + 
                `\`\`\`\n-Click on the drop-down list located just below to view the commands and help on them:\`\`\``
        }[lang],
        color: '3092790', // #2F3136
        footer: {
            icon_url: BOT_ICON,
            text: slogan[lang]
        },
        author: {
            name: {
                ru: 'Код бота полностью открыт',
                en: 'An opensource bot'
            }[lang],
            icon_url: EMPTY_ICON, // тут лучше иконку как в боте эмоджи у команды этой
            url: `https://codeberg.org/myname/Pal-Bot`
        }
    }

    return {
        embeds: [ embed ],
        ephemeral: true
    }
}