/**
 * функция которая выполняет комнаду и отправляет результат пользователю (кнопки)
 */


module.exports = async ({ settings, command, selectMenuValues=[], branches=[], statsFor, db }) => {
    try {
        const commandNameForHelp = selectMenuValues[0]
        const newReply = branches[0] == 'newtab'
        const messageData = await command.execute({ settings, command, statsFor, commandNameForHelp, db })

        if (newReply) messageData.ephemeral = true

        return {
            status: 1,
            messageData,
            needEmbed: true,
            newReply
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'help.button'
        }
    }
}