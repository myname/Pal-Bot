/**
 * функция которая выполняет комнаду принимая данные
 */
const path = require('path')
const { DISCORD_INVITE } = process.env
const CommandsManager = require(path.join(require.main.path, 'classes', 'CommandsManager.js'))
// const { news } = require(path.join(require.main.path, 'config', 'index.js'))
const { ActionRowBuilder, ButtonBuilder, StringSelectMenuBuilder } = require('discord.js')


module.exports = async ({ settings, command, statsFor, commandNameForHelp, db }) => {
    try {
        const { id: discordUserId, lang } = settings.user
        const commandsManager = new CommandsManager()

        if (!command) command = commandsManager.get('help')

        if (commandNameForHelp) {
            // если выбрана команда для помощи, то возвращаем новый ответ
            const com = commandsManager.get(commandNameForHelp)
            return com.details(com, lang)
        }

        // const { resolveName } = require(path.join(require.main.path, 'utils', 'discord.js'))
        // const nameOrId = resolveName(statsFor || discordUserId)

        const buttonsLine_1 = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`menu_${discordUserId}_${statsFor}`)
            .setLabel({en: 'Menu', ru: 'Меню'}[lang])
            .setStyle('Danger')
            .setEmoji('<:menu:943824092635758632>')
        )
        .addComponents(
            new ButtonBuilder()
            .setURL(DISCORD_INVITE)
            .setLabel({en: 'Join bot server', ru: 'Посетить сервер бота'}[lang])
            .setStyle('Link')
        )

        const pageListOpt = []
        commandsManager.each((com, i) => {
            if (com.owner || com.hiddenInHelp) return;

            const cObj = {
                label: `/${com.slashName || com.name}`,
                description: com.info[lang],
                value: com.name
            }

            if (com.emoji) cObj.emoji = com.emoji
            pageListOpt.push(cObj)
        })
        const buttonsLine_2 = new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
                .setCustomId(`help_${discordUserId}_${statsFor}`)
                .setPlaceholder({en: 'Select a command for help', ru: 'Выберите команду для справки'}[lang])
                .addOptions(pageListOpt)
        )

        // console.log( command.details(command, lang) )

        return {
            ...command.details(command, lang),
            components: [ buttonsLine_1, buttonsLine_2 ]
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'help.execute'
        }
    }
}