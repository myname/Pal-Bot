/**
 * функция обрабатывающая слеш-команду
 */


module.exports = async ({ settings, command, db }) => {
    try {
        const messageData = await command.execute({ settings, command, statsFor: 'me', db })

        return {
            status: 1,
            messageData
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'help.slash'
        }
    }
}