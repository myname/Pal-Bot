/**
 * функция обрабатывающая команду (устаревшие)
 */


module.exports = async ({ settings, command, db }) => {
    try {
        return await command.execute({ settings, statsFor: 'me', db })
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'hh.command'
        }
    }
}