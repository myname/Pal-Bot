/**
 * функция возвращающая обьект слеш-команды
 */


module.exports = () => {
    return {
        name: 'help',
        description: 'Provides text and video descriptions of commands',
        options: null
    }
}