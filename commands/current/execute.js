/**
 * функция которая выполняет комнаду и отправляет результат пользователю
 */
const path = require('path')
const { news, platforms } = require(path.join(require.main.path, 'config', 'index.js'))
const { ActionRowBuilder, ButtonBuilder, StringSelectMenuBuilder } = require('discord.js')
const { resolveName } = require(path.join(require.main.path, 'utils', 'discord.js'))


module.exports = async ({ settings, command, statsFor, db }) => {
    try {
        const { id: discordUserId, lang, timezone } = settings.user
        const nameOrId = resolveName(statsFor || discordUserId)

        const stats = await command.getStats(discordUserId, nameOrId, db)
        const { getplayerstatus } = stats
        const data = getplayerstatus.json[0]
        const { lastUpdate } = getplayerstatus

        const buttonsLine_1 = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`menu_${discordUserId}_${nameOrId}`)
            .setLabel({en: 'Menu', ru: 'Меню'}[lang])
            .setStyle('Danger')
            .setEmoji('<:menu:943824092635758632>')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`current_${discordUserId}_${nameOrId}`)
            .setLabel({en: 'Update', ru: 'Обновить'}[lang])
            .setStyle('Success')
            .setEmoji('<:refresh_mix:943814451226873886>')
        )

        const matchId = data.Match

        let replyText = ''
        if (data.status === 0) {
            replyText = {
                ru: 'Игрок Offline.',
                en: 'Player Offline.'
            }
        } else if (data.status === 1) {
            replyText = {
                ru: 'Игрок находится в меню игры.',
                en: 'The player is in the game menu.'
            }
        } else if (data.status === 2) {
            replyText = {
                ru: 'Игрок выбирает чемпиона или загружается в матч.',
                en: 'The player selects a champion or loads into the match.'
            }
        } else if (data.status === 3) {
            // ниче не делаем
        } else if (data.status === 4) {
            replyText = {
                ru: 'Игрок Online, но блокирует трансляцию состояния игрока.',
                en: `The player is Online, but blocks the broadcast of the player's state.`
            }
        } else if (data.status === 5) {
            replyText = {
                ru: 'Игрок не найден.',
                en: 'Player not found.'
            }
        } else {
            // неизвестная ошибка - status должен быть !
            throw {
                err_msg: {
                    ru: 'Неизвестная ошибка в данных Hi-Rez. Сообщите создателю бота.',
                    en: 'Unknown error in Hi-Rez data. Let the bot creator know.'
                }
            }
        }

        if (replyText) {
            // отправляем сообщение и завершаем скрипт
            // тут еще отправляем новости
            return {
                content: `${news[lang]}\`\`\`\n${replyText[lang]}\`\`\``,
                components: [ buttonsLine_1 ]
            }
        } else if (matchId) {
            // если игрок в матче getmatchplayerdetails (data.match_queue_id)
            const match = stats.getmatchplayerdetails.json

            if (!match[0].Queue && match[0].ret_msg && match.length == 1) {
                // текущая очередь не поддерживается
                return {
                    content: {
                        en: 'A player in the match lobby. The current mode is not supported.', 
                        ru: 'Игрок в лобби матча. Текущий режим не поддерживается.'
                    }[lang],
                    components: [ buttonsLine_1 ]
                }
            }

            const draw = await command.draw({ match, lang, timezone, lastUpdate })
            if (!draw.status) throw draw

            const canvas = draw.canvas
            const buffer = canvas.toBuffer('image/png') // buffer image

            const emojiNumbers = [
                '<:1_:943870598541615114>', '<:2_:943870598789079060>', '<:3_:943870598545801218>',
                '<:4_:943870598667436083>', '<:5_:943870598625497089>', '<:6_:943870598550007829>',
                '<:7_:943870598935883776>', '<:8_:943870598470332487>', '<:9_:943870599023960154>',
                '<:10:943870599040729098>'
            ]

            const optionsForStats = []
            match.forEach((player, i) => {
                const id = +player.playerId
                if (!id) return

                optionsForStats.push({
                    label: `${player.playerName} (${player.Account_Level} lvl)`,
                    description: `${player.ChampionName} (${player.ChampionLevel} lvl)`,
                    value: `_${id}`,
                    emoji: emojiNumbers[i]
                })
            })

            const buttonsLine_2 = new ActionRowBuilder()
            .addComponents(
                new StringSelectMenuBuilder()
                .setCustomId(`stats_${discordUserId}_${nameOrId}`)
                .setPlaceholder({en: 'Show player statistics', ru: 'Показать статистику игрока'}[lang])
                .addOptions(optionsForStats)
            )

            const showOldStatsText = {
                ru: '__**Вам будут показаны данные последнего удачного запроса.**__',
                en: '__**You will be shown the details of the last successful request.**__'
            }

            const replayOldText = stats.getmatchplayerdetails.old ?
                `${stats.getmatchplayerdetails.new.err_msg[lang]}\n${showOldStatsText[lang]}\n` : ''

            const matchesInfoText = match.map((player, i) => {
                    return `${i+1}. [${player.ChampionName}](${player.playerName})<id ${player.playerId}>` +
                        `[${platforms[player.playerPortalId]}](${player.playerPortalUserId})`
                }).join('\n')


            const matchesInfo = `\`\`\`\n[Match id](${draw.matchId})\n\n${matchesInfoText}\`\`\``

            return {
                content: `${news[lang]}${replayOldText}${matchesInfo}`,
                components: [ buttonsLine_1, buttonsLine_2 ],
                files: [{
                    attachment: buffer,
                    name: `${nameOrId}.png`
                }]
            }
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'sp.execute'
        }
    }
}