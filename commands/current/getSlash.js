/**
 * функция возвращающая обьект слеш-команды
 */


module.exports = () => {
    return {
        name: 'current',
        description: 'Returns the player status in real time',
        options: [
            {
                name: 'nickname',
                description: 'Nickname or id of the player whose stats you want to see',
                type: 3
            }
        ]
    }
}