/**
 * Скрипт который только рисует указанную инфу в canvas и возвращает ее или ошибку
 */


/**
 * 
 * @param {*} body - 
 * @param {Object} prop - 
 */
module.exports = async ({ match, lang, timezone, lastUpdate }) => {
    try {
        const { createCanvas } = require('canvas')
        const width = 952
        const height = 535
        const canvas = createCanvas(width, height)
        const ctx = canvas.getContext('2d')

        const resDefault = await drawDefault({ ctx, match, lang, width, height, timezone, lastUpdate })
        if (!resDefault.status) throw resDefault

        return {
            status: true,
            canvas,
            matchId: match[0].Match
        }
    } catch(error) {
        if ('err_msg' in error) throw error // проброс ошибки если есть описание
        throw {
            status: false,
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'Ошибка функции "sp.draw"'
        }
    }
}


async function drawDefault({ ctx, match, lang, timezone, width, height, lastUpdate }) { // возможно ее лучше вынести в отдельный файл (рядом)
    try {
        const path = require('path')
        const { imageNormalize } = require(path.join(require.main.path, 'utils', 'canvas.js'))
        const AbstractChampion = require(path.join(require.main.path, 'classes', 'AbstractChampion.js'))
        const { colors } = require(path.join(require.main.path, 'config', 'index.js'))
        const { getMap } = require(path.join(require.main.path, 'utils', 'index.js'))
        const { white, blue, black, orange, transparent } = colors
        const { loadImage } = require('canvas')
        const matchOne = match[0]
        // const maps = config.img.maps

        // let mapImg = null // узнаем карту, получаем ее картинку
        const getMapMatch = await getMap(matchOne.mapGame)
        const mapImg = getMapMatch.img
        const mapName = mapImg ? getMapMatch.name : '-'

        if (mapImg) {
            const newCanvas = imageNormalize({ img: mapImg, width, height })
            if (newCanvas) ctx.drawImage(newCanvas, 0, 0, width, height)
        }

        // затемняющий прозрачный фон
        ctx.fillStyle = transparent
        ctx.fillRect(0, 0, width, height)

        ctx.fillStyle = black
        ctx.fillRect(0, 0, width, 40)
        ctx.fillRect(0, height - 40, width, height)

        ctx.font = 'bold 16px Ledger'
        ctx.fillStyle = blue
        ctx.fillText(`${{ ru: 'Команда', en: 'Team' }[lang]} 1`, 35, 25)
        ctx.textAlign = 'end'
        ctx.fillText(`${{ ru: 'Команда', en: 'Team' }[lang]} 2`, width - 35, 25)
        ctx.fillText(`${{ ru: 'Карта', en: 'Map' }[lang]}: `, width / 4, height - 15)
        ctx.fillText(`${{ ru: 'Регион', en: 'Region' }[lang]}: `, width / 4 + width / 2, height - 15)
        ctx.fillText(`${{ ru: 'Id матча', en: 'Match id' }[lang]}: `, width / 2, 25)
        ctx.textAlign = 'start'
        ctx.fillStyle = orange
        ctx.fillText(` ${mapName}`, width / 4, height - 15)
        ctx.fillText(` ${matchOne.playerRegion}`, width / 4 + width / 2, height - 15)
        ctx.fillText(` ${matchOne.Match}`, width / 2, 25)
        ctx.fillStyle = white

        let countTeam1 = 0
        match.sort((p1, p2) => p1.taskForce - p2.taskForce) // сортируем по возрастанию
        // перебираем игроков матча и рисуем их инфу
        for (let i = 0; i < match.length; i++) {
            const player = match[i]
            // const champName = player.ChampionName
            // if (!champName) {
                // console.log(player, matchOne.Match) // показываем игрока и id матча
                // сообщить на канале об ошибке
                // _local.utils.sendToChannel(config.chLog, 'Не найдено имя чемпиона у команды SP.').catch(console.log)
            // }
            const champName = AbstractChampion.nameNormalize(player.ChampionName)
            let champImgSrc = null
            let champImg = null
            if (champName) {
                champImgSrc = path.join(require.main.path, 'champions', champName, 'icon.jpg')
                champImg = await loadImage(champImgSrc).catch(console.log)
            }
            // const champImgSrc = champion ? champion.icon : undefined
            // const champImg = await loadImage(champImgSrc).catch(console.log)
            const tier = player.Tier
            const winrate = AbstractChampion.getWinrate(player.tierWins, player.tierLosses)
            // const winrate = (player.tierWins / (player.tierWins + player.tierLosses) * 100).toFixed(2) || '-'

            // просто рисуем команду 1, а когда понимаем что началась команда 2 то делаем принудительно
            // i=5 и какие там другие еще переменные

            if (player.taskForce == 1) {
                countTeam1++
                if (champImg) ctx.drawImage(champImg, 70, 90 * i + 50, 50, 50)
                ctx.fillText(`${{ ru: 'Ник', en: 'Nickname' }[lang]}: ${player.playerName}`, 130, 90 * i + 65)
                if (player.Queue == 486) ctx.fillText(`${{ ru: 'Винрейт', en: 'Winrate' }[lang]}: ${winrate}%`, 130, 90 * i + 88)
                ctx.fillText(`Lvl: ${player.Account_Level}`, 130, 90 * i + 110)
                ctx.textAlign = 'center'
                ctx.fillText(player.ChampionLevel, 95, 90 * i + 120)
                ctx.textAlign = 'start'

                if (player.Queue != 486) continue
                const rankImgWidth = tier == 26 ? 264 : tier == 27 ? 264 : 256
                const rankImgHeight = tier == 26 ? 304 : tier == 27 ? 332 : 256
                // const imgPaddingY = tier == 26 ? -15 : tier == 27 ? -20 : 0
                const imgPaddingY = 0
                const divisionImgSrc = path.join(require.main.path, 'img', 'divisions', `${tier}.png`)
                const divisionImg = await loadImage(divisionImgSrc).catch(console.log)
                ctx.drawImage(divisionImg, 0, 90 * i + 45 + imgPaddingY, rankImgWidth / 3.7, rankImgHeight / 3.7)
            } else if (player.taskForce == 2) {
                if (champImg) ctx.drawImage(champImg, width - 120, 90 * (i - countTeam1) + 50, 50, 50)
                ctx.textAlign = 'end'
                ctx.fillText(`${{ ru: 'Ник', en: 'Nickname' }[lang]}: ${player.playerName}`, width - 130, 90 * (i - countTeam1) + 65)
                // рисуем винрейт только в рейте, так как он доступен только там
                if (player.Queue == 486) ctx.fillText(`${{ ru: 'Винрейт', en: 'Winrate' }[lang]}: ${winrate}%`, width - 130, 90 * (i - countTeam1) + 88)
                ctx.fillText(`Lvl: ${player.Account_Level}`, width - 130, 90 * (i - countTeam1) + 110)
                ctx.textAlign = 'center'
                ctx.fillText(player.ChampionLevel, width - 95, 90 * (i - countTeam1) + 120)

                if (player.Queue != 486) continue
                const rankImgWidth = tier == 26 ? 264 : tier == 27 ? 264 : 256
                const rankImgHeight = tier == 26 ? 304 : tier == 27 ? 332 : 256
                // const imgPaddingY = tier == 26 ? -15 : tier == 27 ? -20 : 0
                const imgPaddingY = 0
                const divisionImgSrc = path.join(require.main.path, 'img', 'divisions', `${tier}.png`)
                const divisionImg = await loadImage(divisionImgSrc).catch(console.log)
                ctx.drawImage(divisionImg, width - 70, 90 * (i - countTeam1) + 45 + imgPaddingY, rankImgWidth / 3.7, rankImgHeight / 3.7)
            }
        }

        return { status: true }
    } catch(error) {
        if ('err_msg' in error) throw error // проброс ошибки если есть описание
        throw {
            status: false,
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'Ошибка функции "sp.drawDefault"'
        }
    }
}