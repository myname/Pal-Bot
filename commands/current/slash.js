/**
 * функция обрабатывающая слеш-команду
 */


module.exports = async ({ command, settings, options, db }) => {
    try {
        const nickname = options.getString('nickname')
        const messageData = await command.execute({ settings, command, statsFor: nickname, db })

        return {
            status: 1,
            messageData
        }
    } catch(err) {
        console.log(JSON.stringify(err))
        if (err && err.err_msg !== undefined) throw err
        throw {
            err,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'sp.slash'
        }
    }
}