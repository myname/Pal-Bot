/**
 * функция которая получает данные статистик и обрабатывает возможные ошибки связанные с этим
 */
const path = require('path')
const { getUserSavedData } = require(path.join(require.main.path, 'utils', 'index.js'))


module.exports = async (userId, nameOrId, db) => {
    try {
        const { api: hirez } = db

        const playerIdOrName = await getUserSavedData(nameOrId, userId, false, db)
        let playerId = !isNaN(parseInt(+playerIdOrName)) ? +playerIdOrName : null
        let playerName = null
        // console.log(`playerIdOrName: ${playerIdOrName} | playerId: ${playerId} | playerName: ${playerName}`)

        if (!playerId) { // если нет id то делаем запрос для получения id
            const fetchGetplayer = await hirez.getplayer(playerIdOrName)
            if (!fetchGetplayer.status) throw fetchGetplayer

            const getplayer = fetchGetplayer.data[0]
            playerName = getplayer?.hz_player_name || getplayer?.hz_gamer_tag
            playerId = getplayer?.Id
            if (!getplayer || !playerId) throw {
                status: false,
                err_msg: {
                    ru: 'Аккаунт скрыт.',
                    en: 'The account is hidden.'
                }
            }
        }

        // console.log(`playerIdOrName: ${playerIdOrName} | playerId: ${playerId} | playerName: ${playerName}`)
        const fetchGetplayerstatus = await hirez.getplayerstatus(playerId)
        if (!fetchGetplayerstatus.status) throw fetchGetplayerstatus

        const getplayerstatus = fetchGetplayerstatus.data
        const matchId = getplayerstatus[0].Match
        if (matchId) {
            const fetchGetmatchplayerdetails = await hirez.getmatchplayerdetails(matchId)
            if (!fetchGetmatchplayerdetails.status) throw fetchGetmatchplayerdetails
            const getmatchplayerdetails = fetchGetmatchplayerdetails.data

            return {
                getplayerstatus: {
                    lastUpdate: getplayerstatus.lastUpdate,
                    json: getplayerstatus
                },
                getmatchplayerdetails: {
                    lastUpdate: getplayerstatus.lastUpdate,
                    json: getmatchplayerdetails
                },
                playerId,
                playerName,
                status: true
            }
        }

        return {
            getplayerstatus: {
                lastUpdate: getplayerstatus.lastUpdate,
                json: getplayerstatus
            },
            playerId,
            playerName,
            status: true
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'sp.getStats'
        }
    }
}