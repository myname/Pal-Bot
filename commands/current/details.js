/**
 * функция возвращающая текст полного описания команды с примерами их работы
 */


module.exports = function(command, lang) {
    const path = require('path')
    const { slogan, youtube: { current: videoURL }, gif: { current: gif } } = require(path.join(require.main.path, 'config', 'index.js'))
    const { EMPTY_ICON, BOT_ICON } = process.env

    const embed = {
        title: {
            ru: '`Смотреть пример на ютубе`',
            en: '`Watch an example on YouTube`'
        }[lang],
        url: videoURL,
        image: {
            url: gif
        },
        description: {
            ru: `**${command.info[lang]}.**\n</current:884702180207124498> \`nickname\` \`\`\`md\n> Параметры:\`\`\``,
            en: `**${command.info[lang]}.**\n</current:884702180207124498> \`nickname\` \`\`\`md\n> Params:\`\`\``
        }[lang],
        color: '3092790',
        footer: {
            icon_url: BOT_ICON,
            text: slogan[lang]
        },
        author: {
            name: `/${command.name}`,
            icon_url: EMPTY_ICON
        },
        fields: [
            {
                name: 'nickname:',
                value: {
                    ru: `\`\`\`\n[Ник или id игрока, статистику которого вы хотите увидеть].\nВведите сюда никнейм ` + 
                        `игрока, чью статистику хотите посмотреть.\`\`\``,
                    en: `\`\`\`\n[Nickname or id of the player whose stats you want to see].\nEnter here the nickname ` + 
                        `of the player whose statistics you want to see.\`\`\``
                }[lang]
            }
        ]
    }

    return {
        embeds: [ embed ],
        ephemeral: true
    }
}