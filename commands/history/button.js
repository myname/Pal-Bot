/**
 * функция которая выполняет комнаду и отправляет результат пользователю (кнопки)
 */
const path = require('path')
const AbstractChampion = require(path.join(require.main.path, 'classes', 'AbstractChampion.js'))
const { championsByRoles } = require(path.join(require.main.path, 'config', 'index.js'))
const { ActionRowBuilder, ButtonBuilder, StringSelectMenuBuilder } = require('discord.js')


module.exports = async ({ command, selectMenuValues=[], statsFor, branches, settings, db }) => {
    try {
        const { id: discordUserId, lang, timezone, filters } = settings.user
        const typeValue = new Set(branches)

        const prefFull = filters?.shFull !== false
        const nowFull = typeValue.has('full')
        if (nowFull) { // если есть изменения
            settings.user.filters.shFull = !prefFull
            await settings.saveUser()
        }

        if (typeValue.has('bot-show')) {
            filters.shBot = true
            await settings.saveUser()
        }

        if (typeValue.has('bot-hide')) {
            filters.shBot = false
            await settings.saveUser()
        }

        const prefChampion = filters?.shChampion || null // чемпион или роль
        const prefChampType = prefChampion ? championsByRoles[AbstractChampion.nameNormalize(prefChampion)] ? 'champion' : 'role' : null
        const prefMode = filters?.shMode || null
        // console.log(`prefChampion: ${prefChampion}; prefChampType: ${prefChampType}`)

        // const pageShow = typeValue.has('page') ? selectMenuValues[0] : 1
        const pageShow = typeValue.has('page') ? selectMenuValues[0] : typeValue.has('pageShow') ? branches[branches.length-1] : 1
        // console.log('\n\n', branches, '\n\n')
        // const modifier = typeValue.has('full') ? '-f' : null
        const championRole = typeValue.has('role') ? selectMenuValues[0] : (prefChampType == 'role' ? prefChampion : null)
        const modeType = typeValue.has('queue') ? selectMenuValues[0] : (prefMode || null)
        const championType = typeValue.has('champion') ? selectMenuValues[0] : (prefChampType == 'champion' ? prefChampion : null)

        // console.log(`championRole: ${championRole}; modeType: ${modeType}; championType: ${championType}`)
        if (championType && prefChampion != championType) {
            filters.shChampion = championType
            await settings.saveUser()
        }

        if (championRole && prefChampion != championRole) {
            filters.shChampion = championRole
            await settings.saveUser()
        }

        if (modeType && prefMode != modeType) {
            filters.shMode = modeType
            await settings.saveUser()
        }

        if (typeValue.has('filters')) {
            // нужно открыть меню с фильтрами
            const buttonsLine_1 = new ActionRowBuilder()
            .addComponents(
                new ButtonBuilder()
                .setCustomId(`pal_${discordUserId}_${statsFor}`)
                .setLabel({en: 'Menu', ru: 'Меню'}[lang])
                .setStyle('Danger')
                .setEmoji('<:menu:943824092635758632>')
            )
            .addComponents(
                new ButtonBuilder()
                .setCustomId(`sh_${discordUserId}_${statsFor}`)
                .setLabel({en: 'Back to matches', ru: 'Назад к матчам'}[lang])
                .setStyle('Secondary')
                .setEmoji('<:history:943818397009985597>')
            )
            .addComponents(
                new ButtonBuilder()
                .setCustomId(`sh_${discordUserId}_${statsFor}_filterchampions`)
                .setLabel({en: 'Filter by champion', ru: 'Фильтр по чемпиону'}[lang])
                .setStyle('Secondary')
                .setEmoji('<:filter:943854779648581652>')
            )
            if (filters.shBot ?? true) {
                buttonsLine_1
                .addComponents(
                    new ButtonBuilder()
                    .setCustomId(`sh_${discordUserId}_${statsFor}_bot-hide`)
                    .setLabel({en: 'Hide game of bots', ru: 'Скрыть игры с ботами'}[lang])
                    .setStyle('Secondary')
                    .setEmoji('🤖')
                )
            } else {
                buttonsLine_1
                .addComponents(
                    new ButtonBuilder()
                    .setCustomId(`sh_${discordUserId}_${statsFor}_bot-show`)
                    .setLabel({en: 'Show game of bots', ru: 'Показать игры с ботами'}[lang])
                    .setStyle('Secondary')
                    .setEmoji('🤖')
                )
            }

            const buttonsLine_2 = new ActionRowBuilder()
            .addComponents(
                new StringSelectMenuBuilder()
                .setCustomId(`sh_${discordUserId}_${statsFor}_role`)
                .setPlaceholder({en: 'Filter by roles', ru: 'Фильтр по ролям'}[lang])
                .addOptions([
                    {
                        label: {en: 'All', ru: 'Все'}[lang],
                        // description: {en: 'Show games only in the specified queue', ru: 'Показать игры только указанного режима'}[lang],
                        value: 'all',
                        emoji: '<:champions:943447650647310356>'
                    },
                    {
                        label: {en: 'Support', ru: 'Поддержка'}[lang],
                        // description: {en: 'Show games only for the specified role', ru: 'Показать игры только указанной роли'}[lang],
                        value: 'support',
                        emoji: '<:support:943440471924023328>'
                    },
                    {
                        label: {en: 'Frontline', ru: 'Танк'}[lang],
                        // description: {en: 'Show games only for the specified role', ru: 'Показать игры только указанной роли'}[lang],
                        value: 'frontline',
                        emoji: '<:frontline:943440471743672320>'
                    },
                    {
                        label: {en: 'Flanker', ru: 'Фланг'}[lang],
                        // description: {en: 'Show games only for the specified role', ru: 'Показать игры только указанной роли'}[lang],
                        value: 'flanker',
                        emoji: '<:flank:943440471823360010>'
                    },
                    {
                        label: {en: 'Damage', ru: 'Урон'}[lang],
                        // description: {en: 'Show games only for the specified role', ru: 'Показать игры только указанной роли'}[lang],
                        value: 'damage',
                        emoji: '<:damage:943440471554924554>'
                    }
                ])
            )

            const buttonsLine_3 = new ActionRowBuilder()
            .addComponents(
                new StringSelectMenuBuilder()
                .setCustomId(`sh_${discordUserId}_${statsFor}_queue`)
                .setPlaceholder({en: 'Filter by queue', ru: 'Фильтр по режиму'}[lang])
                .addOptions([
                    {
                        label: {en: 'All', ru: 'Все'}[lang],
                        // description: {en: 'Show games only in the specified queue', ru: 'Показать игры только указанного режима'}[lang],
                        value: 'all'
                    },
                    {
                        label: {en: 'Ranked', ru: 'Ранкед'}[lang],
                        // description: {en: 'Show games only in the specified queue', ru: 'Показать игры только указанного режима'}[lang],
                        value: 'ranked'
                    },
                    {
                        label: {en: 'Siege', ru: 'Осада'}[lang],
                        // description: {en: 'Show games only in the specified queue', ru: 'Показать игры только указанного режима'}[lang],
                        value: 'siege'
                    },
                    {
                        label: {en: 'Deathmatch', ru: 'Бой насмерть'}[lang],
                        // description: {en: 'Show games only in the specified queue', ru: 'Показать игры только указанного режима'}[lang],
                        value: 'deathmatch'
                    },
                    {
                        label: {en: 'Onslaught', ru: 'Натиск'}[lang],
                        // description: {en: 'Show games only in the specified queue', ru: 'Показать игры только указанного режима'}[lang],
                        value: 'onslaught'
                    }
                ])
            )

            return {
                status: 2,
                messageData: {
                    content: {en: 'Filters', ru: 'Фильтры'}[lang],
                    components: [ buttonsLine_1, buttonsLine_2, buttonsLine_3 ]
                }
            }
        } else if (typeValue.has('filterchampions')) {
            // если фильтр по чемпионам
            const buttonsLine_1 = new ActionRowBuilder()
            .addComponents(
                new ButtonBuilder()
                .setCustomId(`pal_${discordUserId}_${statsFor}`)
                .setLabel({en: 'Menu', ru: 'Меню'}[lang])
                .setStyle('Danger')
                .setEmoji('<:menu:943824092635758632>')
            )
            .addComponents(
                new ButtonBuilder()
                .setCustomId(`sh_${discordUserId}_${statsFor}_filters`)
                .setLabel({en: 'Back to filters', ru: 'Назад к фильтрам'}[lang])
                .setStyle('Secondary')
                .setEmoji('<:filter:943854779648581652>')
            )
            .addComponents(
                new ButtonBuilder()
                .setCustomId(`sh_${discordUserId}_${statsFor}`)
                .setLabel({en: 'Back to matches', ru: 'Назад к матчам'}[lang])
                .setStyle('Secondary')
                .setEmoji('<:history:943818397009985597>')
            )

            const roleIcons = {
                flanker: '<:flank:943440471823360010>',
                damage: '<:damage:943440471554924554>',
                support: '<:support:943440471924023328>',
                frontline: '<:frontline:943440471743672320>'
            }
            const championsFilterOpts = []
            let counter = 0
            for (let champion in championsByRoles) {
                const role = championsByRoles[champion]
                const count = Math.floor(counter / 25) // массив по счету

                if (!(counter % 25)) championsFilterOpts.push([]) // новый массив

                championsFilterOpts[count].push({
                    label: champion.slice(0, 1).toUpperCase() + champion.slice(1), // с большой буквы
                    // description: {en: `Filter by the selected champion`, ru: `Фильтр по выбранному чемпиону`}[lang],
                    value: champion,
                    emoji: roleIcons[role]
                })
                counter++
            }

            const championsOpt = []
            championsFilterOpts.forEach((optList, i) => {
                championsOpt.push(
                    new ActionRowBuilder()
                    .addComponents(
                        new StringSelectMenuBuilder()
                            .setCustomId(`sh_${discordUserId}_${statsFor}_champion_${i}`)
                            .setPlaceholder({en: 'Select a champion', ru: 'Выберите чемпиона'}[lang])
                            .addOptions(optList)
                    )
                )
            })

            return {
                status: 2,
                messageData: {
                    content: {en: 'Filters', ru: 'Фильтры'}[lang],
                    components: [ buttonsLine_1, ...championsOpt ]
                }
            }
        } else if (typeValue.has('reset')) {
            // сбрасываем фильтры
            settings.user.filters = {}
            await settings.saveUser()

            console.log('настройки сбросили, запускаем execute')
            const messageData =  await command.execute({ settings, command, statsFor, db })

            return {
                status: 1,
                messageData
            }
        }

        const messageData =  await command.execute({ settings, command, statsFor, pageShow, db })

        return {
            status: 1,
            messageData
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'sh.button'
        }
    }
}