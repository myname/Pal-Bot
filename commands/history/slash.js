/**
 * функция обрабатывающая слеш-команду
 */


module.exports = async ({ settings, command, options, db }) => {
    try {
        const statsFor = options.getString('nickname')
        const pageShow = options.getString('page') || 1
        const championRole = options.getString('role')
        const modeType = options.getString('queue')
        const fullinfoOpt = options.getString('fullinfo')

        const { filters } = settings.user

        // if (!settings.user.data.params) settings.user.data.params = {}
        // if (!settings.user.data.params.sh) settings.user.data.params.sh = {}
        // if (!settings.data.params) settings.data.params = {}
        // if (!settings.data.params.sh) settings.data.params.sh = {}

        if (championRole) db.users[ settings.user.id ].shChampion = championRole
        if (modeType) db.users[ settings.user.id ].shMode = modeType
        if (fullinfoOpt) db.users[ settings.user.id ].shFull = fullinfoOpt == 'yes' ? true : false
        if (championRole || modeType || fullinfoOpt) await db.save()

        const messageData = await command.execute({ settings, command, statsFor, pageShow, db })

        return {
            status: 1,
            messageData
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'sh.slash'
        }
    }
}