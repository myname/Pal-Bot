/**
 * функция возвращающая текст полного описания команды с примерами их работы
 */


module.exports = function(command, lang) {
    const path = require('path')
    const { slogan, youtube: { history: videoURL }, gif: { history: gif } } = require(path.join(require.main.path, 'config', 'index.js'))
    const { EMPTY_ICON, BOT_ICON } = process.env

    const embed = {
        title: {
            ru: '`Смотреть пример на ютубе`',
            en: '`Watch an example on YouTube`'
        }[lang],
        url: videoURL,
        image: {
            url: gif
        },
        description: {
            ru: `**${command.info[lang]}.**\`\`\`\n[Внимание!] все фильтры работают как чекбоксы - выбрав один раз, бот запомнит их ` + 
            `и будет применять ко всем другим историям матчей.\nЕсли вы этого не хотите, то просто нажмите кнопку сброса фильтров - ` + 
            `['Сбросить фильтры'].\n\n[/history 'nickname' 'queue' 'role' 'page' 'fullinfo']\`\`\` \`\`\`\n-Параметры:\`\`\``,
            en: `**${command.info[lang]}.**\`\`\`\n[Attention!] all filters work like checkboxes - once selected, the bot will ` + 
            `remember them and will apply to all other match stories.\nIf you don't want that, then just click the filter reset button - ` + 
            `['Reset filters'].\n\n[/history 'nickname' 'queue' 'role' 'page' 'fullinfo']\`\`\`\`\`\`\n-Params:\`\`\``
        }[lang],
        color: '3092790',
        footer: {
            icon_url: BOT_ICON,
            text: slogan[lang]
        },
        author: {
            name: `/${command.name}`,
            icon_url: EMPTY_ICON
        },
        fields: [
            {
                name: 'nickname:',
                value: {
                    ru: `\`\`\`\n[Ник или id игрока, статистику которого вы хотите увидеть].\nВведите сюда никнейм ` + 
                        `игрока, чью статистику хотите посмотреть.\`\`\``,
                    en: `\`\`\`\n[Nickname or id of the player whose stats you want to see].\nEnter here the nickname ` + 
                        `of the player whose statistics you want to see.\`\`\``
                }[lang]
            },
            {
                name: 'queue:',
                value: {
                    ru: `\`\`\`\n[Покажет историю матчей только указанного режима].\nПозволяет отфильтровать историю матчей ` + 
                    `и отобразить игры лишь из указанного режима.\`\`\``,
                    en: `\`\`\`\n[Will show the history of matches only of the specified queue].\nAllows you to filter the ` + 
                    `history of matches and display games only from the specified mode.\`\`\``
                }[lang]
            },
            {
                name: 'role:',
                value: {
                    ru: `\`\`\`\n[Покажет историю матчей только для указанной роли].\nПозволяет отфильтровать историю матчей ` + 
                    `по ролям, отобразив игры лишь с указанной ролью.\`\`\``,
                    en: `\`\`\`\n[Will show the history of matches only for the specified role].\nAllows you to filter the ` + 
                    `history of matches by roles, displaying games only with the specified role.\`\`\``
                }[lang]
            },
            {
                name: 'page:',
                value: {
                    ru: `\`\`\`\n[Выбор страницы для отображения].\nДает возможность выбрать какую страницу истории матча отобразить.\n` + 
                    `На одной странице помещается 10 матчей.\`\`\``,
                    en: `\`\`\`\n[Selecting a page to display].\nAllows you to choose which page of the match history to display.\n` + 
                    `There are 10 matches on one page.\`\`\``
                }[lang]
            },
            {
                name: 'full:',
                value: {
                    ru: `\`\`\`\n[Покажет много дополнительной информации и статистику по играм].\nЭтот параметр отвечает за показ ` + 
                    `дополнительной информации.\nОн работает как чекбокс - один раз активировал и бот запомнит ваш выбор.\`\`\``,
                    en: `\`\`\`\n[It will show a lot of additional information and statistics on games].\nThis parameter is responsible for` + 
                    ` displaying additional information.\nIt works like a checkbox - activated once and the bot will remember your choice.\`\`\``
                }[lang]
            }
        ]
    }

    return {
        embeds: [ embed ],
        ephemeral: true
    }
}