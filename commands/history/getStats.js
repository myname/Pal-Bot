/**
 * функция которая получает данные статистик и обрабатывает возможные ошибки связанные с этим
 */
const path = require('path')
const { getUserSavedData } = require(path.join(require.main.path, 'utils', 'index.js'))


module.exports = async function(discordUserId, nameOrId, db) {
    try {
        const { api: hirez } = db

        const playerIdOrName = await getUserSavedData(nameOrId, discordUserId, false, db)
        let playerId = !isNaN(+playerIdOrName) ? playerIdOrName : null
        let playerName = playerId ? null : playerIdOrName

        if (!playerId) { // если нет id то делаем запрос для получения id
            playerName = playerIdOrName // навсяк случай
            const fetchGetplayer = await hirez.getplayer(playerIdOrName)
            if (!fetchGetplayer.status) throw fetchGetplayer

            const getplayer = fetchGetplayer.data[0]
            playerId = getplayer?.Id
            if (!getplayer || !playerId) throw {
                status: false,
                err_msg: {
                    ru: 'Аккаунт скрыт.',
                    en: 'The account is hidden.'
                }
            }
        }

        const fetchGetmatchhistory = await hirez.getmatchhistory(playerId)
        if (!fetchGetmatchhistory.status) throw fetchGetmatchhistory

        const { data: getmatchhistory } = fetchGetmatchhistory // если данных нет то ничего страшного

        return {
            getmatchhistory: {
                lastUpdate: fetchGetmatchhistory.lastUpdate,
                data: getmatchhistory,
                old: fetchGetmatchhistory.old
            },
            playerId,
            playerName,
            status: true
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'sh.getStats'
        }
    }
}