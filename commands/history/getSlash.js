/**
 * функция возвращающая обьект слеш-команды
 */


module.exports = () => {
    return {
        name: 'history',
        description: 'Displays the match history of the specified player',
        options: [
            {
                name: 'nickname',
                description: 'Nickname or id of the player whose stats you want to see',
                type: 3
            },
            {
                name: 'queue',
                description: 'Will show the history of matches only of the specified queue',
                type: 3,
                choices: [
                    {
                        name: 'All queue',
                        value: 'all'
                    },
                    {
                        name: 'Ranked',
                        value: 'ranked'
                    },
                    {
                        name: 'Siege',
                        value: 'siege'
                    },
                    {
                        name: 'Deathmatch',
                        value: 'deathmatch'
                    },
                    {
                        name: 'Onslaught',
                        value: 'onslaught'
                    }
                ]
            },
            {
                name: 'role',
                description: 'Will show the history of matches only for the specified role',
                type: 3,
                choices: [
                    {
                        name: 'All role',
                        value: 'all'
                    },
                    {
                        name: 'Damage',
                        value: 'damage'
                    },
                    {
                        name: 'Support',
                        value: 'support'
                    },
                    {
                        name: 'Frontline',
                        value: 'frontline'
                    },
                    {
                        name: 'Flank',
                        value: 'flanker'
                    }
                ]
            },
            {
                name: 'page',
                description: 'Selecting a page to display',
                type: 3,
                choices: [
                    {
                        name: '1',
                        value: '1'
                    },
                    {
                        name: '2',
                        value: '2'
                    },
                    {
                        name: '3',
                        value: '3'
                    },
                    {
                        name: '4',
                        value: '4'
                    },
                    {
                        name: '5',
                        value: '5'
                    }
                ]
            },
            {
                name: 'fullinfo',
                description: 'It will show a lot of additional information and statistics on games',
                type: 3,
                choices: [
                    {
                        name: 'Show additional information',
                        value: 'yes'
                    },
                    {
                        name: 'Hide additional information',
                        value: 'no'
                    }
                ]
            }
        ]
    }
}