/**
 * Скрипт который только рисует указанную инфу в canvas и возвращает ее или ошибку
 */


/**
 * 
 */
module.exports = async ({ matches, lang, timezone, lastUpdate, page }) => {
    try {
        const { createCanvas } = require('canvas')
        const width = 980
        const height = 580
        const canvas = createCanvas(width, height)
        const ctx = canvas.getContext('2d')

        // рисуем дефолтные
        const resDefault = await drawDefault({ ctx, lastUpdate, lang, timezone, width, height })
        if (!resDefault.status) throw resDefault

        const resTdable = await drawTable({ ctx, matches, lang, timezone, page })
        if (!resTdable.status) throw resTdable

        return {
            status: true,
            canvas,
            name: matches[0].playerName,
            id: matches[0].playerId
        }
    } catch(error) {
        if ('err_msg' in error) throw error // проброс ошибки если есть описание
        throw {
            status: false,
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'Ошибка функции "sh.draw"'
        }
    }
}


async function drawDefault({ ctx, lang, timezone, width, height }) {
    try {
        const path = require('path')
        const { imageNormalize } = require(path.join(require.main.path, 'utils', 'canvas.js'))
        const { colors } = require(path.join(require.main.path, 'config', 'index.js'))
        const { getIconBot, getRandomBackground } = require(path.join(require.main.path, 'utils', 'index.js'))
        const { red, white, black, blue, transparent } = colors

        const background = await getRandomBackground()
        if (background) {
            const newCanvas = imageNormalize({ img: background, width, height: height - 80 })
            if (newCanvas) ctx.drawImage(newCanvas, 0, 30, width, height - 50) // рисуем
        }

        // прозрачный фон (затемняющий)
        ctx.fillStyle = transparent
        ctx.fillRect(0, 30, width, height - 50)

        ctx.fillStyle = black
        ctx.fillRect(0, 0, width, 30) // прямоугольник сверху
        ctx.fillRect(0, height - 30, width, 30) // прямоугольник снизу

        ctx.textAlign = 'end'
        ctx.font = 'bold 16px Ledger'
        ctx.fillStyle = red

        const propsText = ctx.measureText(`${{ ru: 'Часовой пояс', en: 'Timezone' }[lang]}: ${timezone}`)
        ctx.fillText(`${{ ru: 'Часовой пояс', en: 'Timezone' }[lang]}: ${timezone}`, width - 20, height - 24)

        ctx.fillStyle = white
        ctx.fillRect(10, height - 30, width - propsText.width - 40, 1)
        ctx.fillRect(width - propsText.width - 31, height - 44, 1, 14) // полоса вверх
        ctx.fillRect(width - propsText.width - 31, height - 29, 1, 14) // полоса вниз
        ctx.fillRect(width - propsText.width - 30, height - 44, propsText.width + 20, 1) // продолжение полосы сверху
        ctx.fillRect(width - propsText.width - 30, height - 16, propsText.width + 20, 1) // продолжение полосы снизу
        ctx.textAlign = 'start'
        ctx.fillStyle = red

        const slogan = {
            ru: 'Лучшая статистика по Paladins в наилучшем виде',
            en: 'The best statistics on Paladins in the best possible way'
        }
        ctx.fillStyle = blue
        ctx.fillText(`Pal-Bot - ${slogan[lang]}`, 45,  height - 10)

        const icon = await getIconBot()
        if (icon) ctx.drawImage(icon, 20, height - 25, 20, 20)

        return { status: true }
    } catch(error) {
        if ('err_msg' in error) throw error // проброс ошибки если есть описание
        throw {
            status: false,
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'Ошибка функции "sh.drawDefault"'
        }
    }
}


async function drawTable({ ctx, matches, lang, timezone, page }) {
    try {
        const path = require('path')
        const { secToMin } = require(path.join(require.main.path, 'utils', 'time.js'))
        const { getChampionIcon, getCard, getMap } = require(path.join(require.main.path, 'utils', 'index.js'))
        const { colors, maps } = require(path.join(require.main.path, 'config', 'index.js'))
        const { red, white, blue, orange, green, yellow  } = colors
        const AbstractChampion = require(path.join(require.main.path, 'classes', 'AbstractChampion.js'))
        const { imageNormalize } = require(path.join(require.main.path, 'utils', 'canvas.js'))

        const pos = [10, 160, 345, 405, 510, 590, 680, 790]
        ctx.fillStyle = blue

        const tableNames = {
            ru: ['№', 'Дата/Статус', 'Время', 'Режим', 'К/Д/А', 'Урон', 'Карта'],
            en: ['№', 'Date/Status', 'Time', 'Queue', 'K/D/A', 'Damage', 'Map']
        }
        // рисуем таблицу для инфы
        ctx.fillText(tableNames[lang][0], pos[0], 20)
        ctx.fillText(tableNames[lang][1], pos[1], 20)
        ctx.fillText(tableNames[lang][2], pos[2], 20)
        ctx.fillText(tableNames[lang][3], pos[3], 20)
        ctx.fillText(tableNames[lang][4], pos[4], 20)
        ctx.fillText(tableNames[lang][5], pos[5], 20)
        ctx.fillText(tableNames[lang][6], pos[6], 20)

        const positionY = 30
        for (let i = 0; i < matches.length; i++) {
            const match = matches[i]

            // получаем картинки чемпионов с истории и рисуем
            const champion = AbstractChampion.nameNormalize(match.Champion)
            const img = await getChampionIcon(champion)
            const y = positionY + 52 * i
            if (img) ctx.drawImage(img, 40, y, 50, 50)

            // рисуем легендарку
            const legendary = await getCard({
                id: match.ItemId6, 
                name: match.Item_6, 
                legendary: true, 
                champion
            })
            if (legendary) ctx.drawImage(legendary, 100, y, 50, 50)

            // рисуем превью карты
            const getMapMatch = await getMap(match.Map_Game)
            const mapImg = getMapMatch.img
            if (mapImg) {
                const newCanvas = imageNormalize({ img: mapImg, width: 89, height: 50 })
                if (newCanvas) ctx.drawImage(newCanvas, 680, y, 89, 50) // рисуем карту на весь экран
            }

            ctx.fillStyle = white
            ctx.fillText(`${ i + 1 + (page-1) * 10 }.`, pos[0], 52 * i + 60)
            ctx.fillText(`${ new Date(match.Match_Time).addHours(timezone).toText() }`, pos[1], 52 * i + 48)
            ctx.fillStyle = yellow
            ctx.fillText(`${ secToMin(match.Time_In_Match_Seconds) }`, pos[2], 52 * i + 70)
            // ctx.fillStyle = white
            // ctx.fillText(`${match.Objective_Assists}`, pos[4], 52 * i + 60) // У цели
            ctx.fillStyle = orange
            ctx.fillText(`${match.Kills}/${match.Deaths}/${match.Assists}`, pos[4], 52 * i + 60)
            ctx.fillStyle = red
            ctx.fillText(`${match.Damage.goDot()}`, pos[5], 52 * i + 60)

            const mapName = maps.find(map => {
                const reg = new RegExp(`${map}`, 'i')
                return match.Map_Game.replace(/[\(\)\']/ig, '').replace(/v\w/ig, '').replace(/[ ]+/ig, ' ').match(reg)
            }) || 'Test Maps'
  
            ctx.fillStyle = yellow
            ctx.fillText(`${mapName.wordToUp()}`, pos[6] + 100, 52 * i + 60)
            // ctx.fillStyle = green
            // ctx.fillText(`${match.Healing}`, pos[8], 52 * i + 60)
            // ctx.fillStyle = white
            // ctx.fillText(`${match.Gold}`, pos[9], 52 * i + 60)

            const getStats = match.Win_Status
            const status = getStats == 'Win' ? {ru: 'Победа', en: 'Win'} : {ru: 'Поражение', en: 'Loss'}
            const statusColor = getStats == 'Win' ? green : red

            const getQueue = match.Queue
            const queue = getQueue == 'Siege' ? {ru: 'Осада', en: 'Siege'} :
                getQueue == 'Siege Training' ? {ru: '*Осада', en: '*Siege'} :
                getQueue == 'Ranked' ? {ru: 'Ранкед', en: 'Ranked'} :
                getQueue == 'Onslaught' ? {ru: 'Натиск', en: 'Onslaught'} :
                getQueue == 'Onslaught Training' ? {ru: '*Натиск', en: '*Onslaught'} :
                getQueue == 'Team Deathmatch' ? {ru: 'Насмерть', en: 'Deathmatch'} :
                getQueue == 'Team Deathmatch Training' ? {ru: '*Насмерть', en: '*Deathmatch'} :
                getQueue == 'Test Maps' ? {ru: 'Тестовые', en: 'Test Maps'} :
                getQueue.startsWith('Siege') ? {ru: 'Осада', en: 'Siege'} :
                getQueue.startsWith('Ranked') ? {ru: 'Ранкед', en: 'Ranked'} :
                {ru: getQueue, en: getQueue}

            ctx.fillStyle = statusColor
            ctx.fillText(`${status[lang]}`, pos[1], 52 * i + 72) // сатус
            ctx.fillStyle = white
            ctx.fillText(`${queue[lang]}`, pos[3], 52 * i + 60) // Режим
        }

        return { status: true }
    } catch(error) {
        if ('err_msg' in error) throw error // проброс ошибки если есть описание
        throw {
            status: false,
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'Ошибка функции "sh.drawTable"'
        }
    }
}