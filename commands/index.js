/**
 * отвечает за загрузку всех дефолтных настроек команд
 */


module.exports.console = {
    name: 'console',
    statsName: 'console',
    possibly: [ 'console', 'con' ],
    owner: true,
    files: [ 'command' ] // список файлов которые нужно будет загрузить для команды
}


module.exports.guildcreate = {
    name: 'guildcreate',
    possibly: ['guildcreate'],
    hiddenInHelp: true, // не показывает в хелпе
    // newTab: true, // отсылает новое сообщение (не будет редактировать текущее)
    guild: true, // отправляет доп параметр, получая сервер из данных кнопки
    files: [ 'button' ]
}


module.exports.help = {
    name: 'help',
    slashName: 'help',
    possibly: [ 'help', 'помощь', 'hh', 'хелп' ],
    hiddenInHelp: true, // не показывает в хелпе
    newMessInCommand: true, // сообщения внутри команды будут отсылаться отдельно не меняя основное сообщение
    info: {
        ru: 'Предоставляет текстовые и видео описания команд',
        en: 'Provides text and video descriptions of commands'
    },
    files: [ 'details', 'command', 'execute', 'button', 'getSlash', 'slash' ],
    emoji: '❔'
}


module.exports.menu = {
    name: 'menu',
    slashName: 'menu',
    possibly: [ 'menu', 'pal', 'меню', 'pal-bot', 'palbot' ],
    info: {
        ru: 'Меню команд',
        en: 'Command menu'
    },
    files: [ 'execute', 'details', 'command', 'button', 'slash', 'getSlash' ],
    emoji: '<:menu:943824092635758632>'
}


module.exports.setting = {
    name: 'setting',
    statsName: 'set',
    slashName: 'setting',
    possibly: [ 'setting', 'set', 'установить', 'настройки' ],
    admin: true, // нужно ли посылать дополнительный параметр с проверкой прав пользователя на админ права
    info: {
        ru: 'Меняет настройки языка, часового пояса и другое...',
        en: 'Changes the language settings, time zone, and more...'
    },
    files: [ 'execute', 'details', 'button', 'slash', 'getSlash', 'command' ],
    emoji: '🛠️'
}


module.exports.bot = {
    name: 'bot',
    possibly: [ 'bot' ],
    hiddenInHelp: true, // не показывает в хелпе
    info: {
        ru: 'Показывает статистику использования команд бота',
        en: 'Shows statistics on the use of bot commands'
    },
    files: [ 'execute', 'button', 'draw' ],
    emoji: '🤖'
}


module.exports.me = {
    name: 'me',
    slashName: 'me',
    possibly: [ 'me' ],
    info: {
        ru: `Сохраняет ваш никнейм для автоматической подстановки его в другие команды`,
        en: `Saves your nickname for automatic substitution in other commands`
    },
    files: [ 'details', 'getStats', 'execute', 'saveStats', 'command', 'slash', 'getSlash', 'button', 'deleteStats' ],
    emoji: '👤'
}


module.exports.search = {
    name: 'search',
    statsName: 'se',
    slashName: 'search',
    possibly: [ 'search', 'поиск', 'se' ],
    info: {
        ru: `Поиск аккаунтов консолей и других платформ`,
        en: `Search for accounts of consoles and other platforms`
    },
    files: [ 'details', 'getStats', 'execute', 'button', 'slash', 'command', 'getSlash' ],
    emoji: '<:search:943934321968951397>'
}


module.exports.stats = {
    name: 'stats',
    statsName: 'ss',
    slashName: 'stats',
    possibly: [ 'stats', 'стата', 'ss' ],
    info: {
        ru: `Выводит общую статистику аккаунта`,
        en: `Displays general account statistics`
    },
    files: [ 'details', 'draw', 'getStats', 'execute', 'button', 'slash', 'command', 'getSlash' ],
    emoji: '<:stats:943819417131839501>'
}


module.exports.history = {
    name: 'history',
    statsName: 'sh',
    slashName: 'history',
    possibly: [ 'history', 'история', 'sh' ],
    info: {
        ru: `Выводит историю матчей указанного игрока`,
        en: `Displays the match history of the specified player`
    },
    files: [ 'details', 'draw', 'getStats', 'execute', 'button', 'command', 'slash', 'getSlash' ],
    emoji: '<:history:943818397009985597>'
}


module.exports.last = {
    name: 'last',
    statsName: 'sm',
    slashName: 'last',
    possibly: ['last', 'sm', 'матч', 'match'],
    info: {
        ru: `Выводит статистику последнего (или указанного) матча или игрока`,
        en: `Displays statistics of the last (or specified) match or player`
    },
    files: [ 'details', 'draw', 'getStats', 'execute', 'button', 'command', 'slash', 'getSlash' ],
    emoji: '<:match:943925118286069781>'
}


module.exports.current = {
    name: 'current',
    statsName: 'sp',
    slashName: 'current',
    possibly: [ 'current', 'sp', 'сталкер', 'live' ],
    info: {
        ru: `Возвращает статус игрока в реальном времени (если в матче - то покажет матч)`,
        en: `Returns the player status in real time (if in a match, then it will show the match)`
    },
    files: [ 'details', 'draw', 'getStats', 'execute', 'button', 'command', 'slash', 'getSlash' ],
    emoji: '<:current:943440471680753694>'
}


module.exports.champions = {
    name: 'champions',
    statsName: 'st',
    slashName: 'champions',
    possibly: [ 'champions', 'st', 'топ', 'top' ],
    info: {
        ru: `Выводит топ чемпионов с возможностью сортировки`,
        en: `Displays top champions with sorting options`
    },
    files: [ 'details', 'draw', 'getStats', 'execute', 'command', 'slash', 'getSlash', 'button' ],
    emoji: '<:champions:943447650647310356>'
}


module.exports.champion = {
    name: 'champion',
    statsName: 'sc',
    slashName: 'champion',
    possibly: [ 'champion', 'чемпион', 'sc' ],
    info: {
        ru: `Выводит подробную статистику указанного чемпиона`,
        en: `Displays detailed statistics for the specified champion`
    },
    files: [ 'details', 'draw', 'getStats', 'execute', 'command', 'slash', 'getSlash', 'button' ],
    emoji: '<:champion:943440471601061888>'
}


module.exports.deck = {
    name: 'deck',
    statsName: 'sl',
    slashName: 'lodouts',
    possibly: [ 'deck', 'колода', 'sl', 'lodouts', 'lodout' ],
    info: {
        ru: `Выводит колоды игрока указанного чемпиона`,
        en: `Displays the player decks of the specified champion`
    },
    files: [ 'details', 'draw', 'getStats', 'execute', 'button', 'command', 'slash', 'getSlash' ],
    emoji: '<:cards:943453491907661845>'
}


module.exports.friends = {
    name: 'friends',
    statsName: 'sf',
    slashName: 'friends',
    possibly: [ 'friends', 'друзья', 'friend', 'sf' ],
    info: {
        ru: `Выводит список друзей из игры`,
        en: `Displays the list of friends from the game`
    },
    files: [ 'details', 'getStats', 'execute', 'command', 'button', 'slash', 'getSlash' ],
    emoji: '<:friends:943449946428960798>'
}