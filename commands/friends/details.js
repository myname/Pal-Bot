/**
 * функция возвращающая текст полного описания команды с примерами их работы
 */


module.exports = function(command, lang) {
    const path = require('path')
    const { slogan, youtube: { friends: videoURL }, gif: { friends: gif } } = require(path.join(require.main.path, 'config', 'index.js'))
    const { EMPTY_ICON, BOT_ICON } = process.env

    const embed = {
        title: {
            ru: '`Смотреть пример на ютубе`',
            en: '`Watch an example on YouTube`'
        }[lang],
        url: videoURL,
        image: {
            url: gif
        },
        description: {
            ru: `**${command.info[lang]}.**\`\`\`\n[/friends 'nickname' 'page' 'search']\`\`\` \`\`\`\n-Параметры:\`\`\``,
            en: `**${command.info[lang]}.**\`\`\`\n[/friends 'nickname' 'page' 'search']\`\`\` \`\`\`\n-Params:\`\`\``
        }[lang],
        color: '3092790',
        footer: {
            icon_url: BOT_ICON,
            text: slogan[lang]
        },
        author: {
            name: `/${command.name}`,
            icon_url: EMPTY_ICON
        },
        fields: [
            {
                name: 'nickname:',
                value: {
                    ru: `\`\`\`\n[Ник или id игрока, статистику которого вы хотите увидеть].\nВведите сюда никнейм ` + 
                        `игрока, чью статистику хотите посмотреть.\`\`\``,
                    en: `\`\`\`\n[Nickname or id of the player whose stats you want to see].\nEnter here the nickname ` + 
                        `of the player whose statistics you want to see.\`\`\``
                }[lang]
            },
            {
                name: 'page:',
                value: {
                    ru: `\`\`\`\n[Выбор страницы для отображения (от 1 до 50)].\`\`\``,
                    en: `\`\`\`\n[Selecting the page to display (from 1 to 50)].\`\`\``
                }[lang]
            },
            {
                name: 'search:',
                value: {
                    ru: `\`\`\`\n[Введите часть букву или часть ника для поиска его среди всех друзей (фильтр)].\n` + 
                    `Позволяет осуществлять поиск среди друзей, без учета регистра букв.\`\`\``,
                    en: `\`\`\`\n[Enter part of the letter or part of the nickname to search for it among all friends (filter)].\n` + 
                    `Allows you to search among friends, case-insensitive letters.\`\`\``
                }[lang]
            }
        ]
    }

    return {
        embeds: [ embed ],
        ephemeral: true
    }
}