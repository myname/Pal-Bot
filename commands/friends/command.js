/**
 * функция обрабатывающая команду (устаревшие)
 */


module.exports = async ({ settings, command, contentParams, db }) => {
    try {
        const [ firstParam, secondParam, thirdParam ] = contentParams.split(' ')

        /**
         * если есть thirdParam то firstParam полюбому должен указывать на пользователя с которого получить инфу нужно
         * 
         * если secondParam является числом то это указание страницы, firstParam в таком случае будет указывать на пользователя
         * 
         * если secondParam не число то page=1 и повторяем ситуацию 1
         * 
         * если secondParam не указан то получаем инфу самого пользователя и page=1
         * 
         * 
         * если 3 параметра:
         *      первый это пользователь
         *      второй или третий это search
         * 
         * если 2 параметра:
         *      первые это пользователь
         *      второй это page, если его нет то search
         * 
         * если 1 параметр:
         *      может быть page, тогда пользователь = me, в рпотивном случае пользователь = первому параметру
         */

        const page = thirdParam ? thirdParam : secondParam && isFinite(secondParam) && secondParam <= 50 ? secondParam : 
            firstParam && isFinite(firstParam) && firstParam <= 50 ? firstParam : false
        const pageShow = Math.floor(page) || 1

        const nameOrId = !secondParam && page ? 'me' : firstParam
        const search = thirdParam && page === thirdParam ? secondParam :
            thirdParam && page === secondParam ? thirdParam :
            secondParam && !page ? secondParam : false

        return await command.execute({ settings, command, nameOrId, pageShow, search, db })
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'sf.command'
        }
    }
}