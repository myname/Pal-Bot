/**
 * функция возвращающая обьект слеш-команды
 */


module.exports = () => {
    return {
        name: 'friends',
        description: 'Displays the list of friends from the game',
        options: [
            {
                name: 'nickname',
                description: 'Nickname or id of the player whose stats you want to see',
                type: 3
            },
            {
                name: 'page',
                description: 'Selecting the page to display (from 1 to 50)',
                type: 3
            },
            {
                name: 'search',
                description: 'Enter part of the letter or part of the nickname to search for it among all friends (filter)',
                type: 3
            }
        ]
    }
}