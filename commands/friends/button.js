/**
 * функция которая выполняет комнаду и отправляет результат пользователю (кнопки)
 */


module.exports = async ({ command, selectMenuValues, statsFor, branches, settings, db }) => {
    try {
        const { filters } = settings.user
        const typeValue = new Set(branches)
        const pageShow = typeValue.has('page') ? selectMenuValues[0] : 1
        const isBlockUsers = filters?.sfBlock
        const search = typeValue.has('page') && typeValue.has('search') ? branches[3] : typeValue.has('search') ? branches[1] : ''

        if (typeValue.has('block')) {
            filters.sfBlock = !isBlockUsers
            await settings.saveUser()
        }

        const messageData = await command.execute({ settings, command, statsFor, pageShow, search, db })

        return {
            status: 1,
            messageData
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'sf.button'
        }
    }
}