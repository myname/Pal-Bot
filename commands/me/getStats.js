/**
 * функция которая получает данные статистик и обрабатывает возможные ошибки связанные с этим
 * DELETE
 */


module.exports = async function(userId) {
    try {
        const path = require('path')
        const { usersNicknames } = require(path.join(require.main.path, 'mongo', 'schemes.js'))
        return await usersNicknames.find({ id: userId })
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'me.getStats'
        }
    }
}