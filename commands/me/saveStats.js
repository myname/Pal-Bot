/**
 * функция которая сохраняет данные статистик и обрабатывает возможные ошибки связанные с этим
 */


module.exports = async function(discordUserId, nameOrId, slot, db) {
    try {
        const { api: hirez } = db

        const responseGetplayer = await hirez.getplayer(nameOrId)
        if (!responseGetplayer.status) throw responseGetplayer

        const getplayer = responseGetplayer.data[0]
        if (!getplayer) throw {
            status: false,
            err_msg: {
                ru: 'Аккаунт не найден.',
                en: 'The account was not found.'
            }
        }
        const playerId = getplayer.Id
        const name = getplayer.hz_player_name

        if (!playerId) throw {
            err_msg: {
                ru: 'Аккаунт скрыт.',
                en: 'The account is hidden.'
            }
        }

        let userData = db.users[ discordUserId ]
        if ( !userData ) userData = db.users[ discordUserId ] = { id: discordUserId }
        if ( !userData?.slot ) userData.slot = {}

        // если указан слот
        if ( slot ) {
            userData.slot[ slot ] = {
                id: playerId,
                name: name || nameOrId
            }
        } else {
            userData.saveId = playerId
            userData.saveName = name || nameOrId
        }

        await db.save()

        return db.users[ discordUserId ]
    } catch(eror) {
        if ('err_msg' in eror) throw eror
        throw {
            eror,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'me.saveStats'
        }
    }
}