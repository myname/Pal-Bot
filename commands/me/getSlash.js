/**
 * функция возвращающая обьект слеш-команды
 */


module.exports = () => {
    return {
        name: 'me',
        description: 'Saves your nickname for automatic substitution in other command',
        options: [
            {
                name: 'nickname',
                description: 'The nickname or id of the account you want to link',
                type: 3
            },
            {
                name: 'slot',
                description: 'Choose a place to save (multiaccounts)',
                type: 3,
                choices: [
                    {
                        name: 'me1',
                        value: 'me1'
                    },
                    {
                        name: 'me2',
                        value: 'me2'
                    },
                    {
                        name: 'me3',
                        value: 'me3'
                    },
                    {
                        name: 'me4',
                        value: 'me4'
                    },
                    {
                        name: 'me5',
                        value: 'me5'
                    },
                    {
                        name: 'me6',
                        value: 'me6'
                    },
                    {
                        name: 'me7',
                        value: 'me7'
                    },
                    {
                        name: 'me8',
                        value: 'me8'
                    },
                    {
                        name: 'me9',
                        value: 'me9'
                    }
                ]
            },
            {
                name: 'verification',
                description: 'Go through verification to prove that this is your account',
                type: 3,
                choices: [
                    {
                        name: 'Yes',
                        value: 'yes'
                    },
                    {
                        name: 'No',
                        value: 'no'
                    }
                ]
            }
        ]
    }
}