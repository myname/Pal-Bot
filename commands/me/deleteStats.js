/**
 * функция которая сохраняет данные статистик и обрабатывает возможные ошибки связанные с этим
 * DELETE
 */


module.exports = async function(discordUserId) {
    try {
        const path = require('path')
        const { usersNicknames } = require(path.join(require.main.path, 'mongo', 'schemes.js'))
        await usersNicknames.deleteOne({ id: discordUserId })

        return true
    } catch(eror) {
        if ('err_msg' in eror) throw eror
        throw {
            eror,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'me.deleteStats'
        }
    }
}