/**
 * функция обрабатывающая слеш-команду
 */


module.exports = async ({ settings, command, options, db }) => {
    try {
        const contentParams = options.getString('nickname')
        const slot = options.getString('slot')
        const verification = options.getString('verification')

        const messageData = await command.execute({ settings, command, contentParams, slot, verification, db })

        return {
            status: 1,
            messageData
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'pal.slash'
        }
    }
}