/**
 * функция возвращающая текст полного описания команды с примерами их работы
 */


module.exports = function(command, lang) {
    const path = require('path')
    const { slogan, youtube: { me: videoURL }, gif: { me: gif } } = require(path.join(require.main.path, 'config', 'index.js'))
    const { EMPTY_ICON, BOT_ICON } = process.env

    const embed = {
        title: {
            ru: '`Смотреть пример на ютубе`',
            en: '`Watch an example on YouTube`'
        }[lang],
        url: videoURL,
        image: {
            url: gif
        },
        description: {
            ru: `**${command.info[lang]}.**\n` + 
            `\`\`\`\nВы сможете подставлять сохраненные аккаунты, например\n` + 
            `[/stats 'nickname':'me3'] или [/stats 'nickmane':'me8'].\n` + 
            `Вместо ['me3'] и ['me8'] будут подставлены данные которые вы сохранили.\n` + 
            `['Вы можете в любой момент удалить эти данные самостоятельно из базы данных бота'].\n` + 
            `Чтобы удалить сохраненные никнеймы, перейдите в ['Настройки'], затем в ['Сохраненные никнеймы'] ` + 
            `и нажмите кнопку ['Удалить данные'].\n` + 
            `\n[/me 'nickname' 'slot' 'verification']\`\`\` \`\`\`\n-Параметры:\`\`\``,
            en: `**${command.info[lang]}.**\n` + 
            `\`\`\`\nYou will be able to substitute saved accounts, for example\n` + 
            `[/stats 'nickname':'me3'] or [/stats 'nickmane':'me8'].\n` + 
            `Instead of ['me3'] and ['me8'], the data that you saved will be substituted.\n` + 
            `['You can delete this data yourself from the bot database at any time'].\n` + 
            `To delete saved nicknames, go to ['Settings'], then to ['Saved Nicknames'] and click ['Delete data'].\n` + 
            `\n[/me 'nickname' 'slot' 'verification']\`\`\` \`\`\`\n-Params:\`\`\``,
        }[lang],
        color: '3092790',
        footer: {
            icon_url: BOT_ICON,
            text: slogan[lang]
        },
        author: {
            name: `/${command.name}`,
            icon_url: EMPTY_ICON
        },
        fields: [
            {
                name: 'nickname:',
                value: {
                    ru: `\`\`\`\n[Ник или id аккаунта, который вы хотите привязать].\nВведите сюда никнейм ` + 
                        `или id игрока, аккаунт которого вы хотите привязать.\nДля консольных аккаунтов требуется ввести ID игрового аккаунта.\n` + 
                        `Узнать ID можно с помощью команды [/search].\`\`\``,
                    en: `\`\`\`\n[The nickname or ID of the account you want to link].\nEnter here the nickname ` + 
                        `or id of the player whose account you want to link.\nFor console accounts, you need to enter the game account ID.\n` + 
                        `You can find out the ID using the command [/search].\`\`\``
                }[lang]
            },
            {
                name: 'slot:',
                value: {
                    ru: `\`\`\`\n[Выберите место для сохранения (несколько учетных записей)].\nВы сможете иметь быстрый ` + 
                    `доступ к сохраненным аккаунтам, подставляя их данные для получения статистики.\nНапример [/stats 'nickname':'me3'] ` + 
                    `или [/history 'nickname':'me1'] - подставит вместо ['me3'] и ['me1'] данные которые вы сохранили.\`\`\``,
                    en: `\`\`\`\n[Choose a place to save (multiaccounts)].\nYou will be able to have quick access to ` + 
                    `saved accounts by substituting their data for statistics.\nFor example [/stats 'nickname':'me3'] ` + 
                    `or [/history 'nickname':'me1'] - it will substitute the data that you saved instead of ['me3'] and ['me1'].\`\`\``
                }[lang]
            },
            {
                name: 'verification:',
                value: {
                    ru: `\`\`\`\n-В разработке.\`\`\``,
                    en: `\`\`\`\n-In development.\`\`\``
                }[lang]
            }
        ]
    }

    return {
        embeds: [ embed ],
        ephemeral: true
    }
}