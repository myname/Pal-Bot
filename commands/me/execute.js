/**
 * функция которая выполняет комнаду и отправляет результат пользователю
 */
const path = require('path')
const { ActionRowBuilder, ButtonBuilder } = require('discord.js')
const { news, youtube: { me: videoURL }, gif: { me: gif } } = require(path.join(require.main.path, 'config', 'index.js'))
const { resolveName } = require(path.join(require.main.path, 'utils', 'discord.js'))


module.exports = async ({ settings, command, contentParams='', statsFor, slot, verification, deleteData, db }) => {
    try {
        const { id: discordUserId, lang, timezone, saveId, saveName } = settings.user

        resolveName(contentParams || statsFor || discordUserId) // чисто проверим что бы не было консольного ака
        const nameOrId = contentParams

        const buttonsLine_1 = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`menu_${discordUserId}_${nameOrId}`)
            .setLabel({en: 'Menu', ru: 'Меню'}[lang])
            .setStyle('Danger')
            .setEmoji('<:menu:943824092635758632>')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`setting_${discordUserId}_${nameOrId}`)
            .setLabel({ en: 'Settings', ru: 'Настройки' }[lang])
            .setStyle('Secondary')
            .setEmoji('🛠️')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`me_${discordUserId}_${nameOrId}_delete`)
            .setLabel({en: 'Delete data', ru: 'Удалить данные'}[lang])
            .setStyle('Danger')
            .setEmoji('<:reset:943850941940125696>')
            .setDisabled(!!deleteData)
        )

        if (deleteData) {
            // удаляем сохраненные данные
            // await command.deleteStats(discordUserId)
            const userData = db.users[ discordUserId ]
            if ( userData ) {
                delete userData.saveId
                delete userData.saveName
                delete userData.slot
                await db.save()
            }

            return {
                content: news[lang] + `:white_check_mark::white_check_mark::white_check_mark: \`\`\`\n${{
                    ru: 'Ваши данные успешно удалены!',
                    en: 'Your data has been successfully deleted!'
                }[lang]}\`\`\``,
                components: [ buttonsLine_1 ]
            }
        }

        if (!contentParams) {
            const { saveId, saveName, slot } = (db.users[ discordUserId ] || {})
            if (!saveId && !saveName) throw { // если данных нет
                err_msg: {
                    ru: 'У вас еще нет сохраненного ID.\n=\n' +
                        `Для сохранения ID воспользуйтесь командой /me\nДля поиска ID воспользуйтесь командой /search`,
                    en: `You don't have a saved ID yet.\n=\n` +
                        `To save the ID, use the command /me\nTo search for an ID, use the command /search`
                },
                embed: {
                    description: `[YOUTUBE VIDEO](${videoURL})`,
                    color: '3092790',
                    image: {
                        url: gif
                    }
                }
            }

            // если данные есть
            const replyText = {ru: `#Ваши сохраненные данные:\n`, en: `#Your saved data:\n`}
            if (saveName) (replyText.ru += `[Никнейм](${saveName}); `) && (replyText.en += `[Nicname](${saveName}); `)
            if (saveId) (replyText.ru += `[playerId](${saveId});`) && (replyText.en += `[playerId](${saveId});`)

            if (slot) {
                for (let me in slot) {
                    const dataUser = slot[me]
                    replyText.ru += `\n${me.replace('me', '')}. ` + (dataUser.name ? `[Никнейм](${dataUser.name}); ` : '') + 
                        (dataUser.id ? `[playerId](${dataUser.id}); ` : '')

                    replyText.en += `\n${me.replace('me', '')}. ` + (dataUser.name ? `[Nicname](${dataUser.name}); ` : '') + 
                        (dataUser.id ? `[playerId](${dataUser.id}); ` : '')
                }
            }

            return {
                content: news[lang] + `\`\`\`\n${replyText[lang]}\`\`\``,
                components: [ buttonsLine_1 ]
            }
        }

        // делаем запрос статы проверяя есть ли такие данные и записываем если есть
        const saveData = await command.saveStats(discordUserId, nameOrId, slot, db)

        let replySavedData = ''
        if (saveData) {
            // если данные есть
            const replyText = {ru: `#Ваши сохраненные данные:\n`, en: `#Your saved data:\n`}
            if (saveData.saveName) (replyText.ru += `[Никнейм](${saveData.saveName}); `) && (replyText.en += `[Nicname](${saveData.saveName}); `)
            if (saveData.saveId) (replyText.ru += `[playerId](${saveData.saveId});`) && (replyText.en += `[playerId](${saveData.saveId});`)

            if (saveData.slot) {
                for (let me in saveData.slot) {
                    const dataUser = saveData.slot[me]
                    replyText.ru += `\n${me.replace('me', '')}. ` + (dataUser.name ? `[Никнейм](${dataUser.name}); ` : '') + 
                        (dataUser.id ? `[playerId](${dataUser.id}); ` : '')

                    replyText.en += `\n${me.replace('me', '')}. ` + (dataUser.name ? `[Nicname](${dataUser.name}); ` : '') + 
                        (dataUser.id ? `[playerId](${dataUser.id}); ` : '')
                }
            }
            replySavedData = `\`\`\`\n${replyText[lang]}\`\`\``
        }

        return {
            content: news[lang] + `:white_check_mark::white_check_mark::white_check_mark: \`\`\`\n${{
                ru: 'Ваш никнейм успешно записан!',
                en: 'Your nickname has been successfully registered!'
            }[lang]}\`\`\`` + replySavedData,
            components: [ buttonsLine_1 ]
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'me.execute'
        }
    }
}