/**
 * функция которая выполняет комнаду и отправляет результат пользователю (кнопки)
 */
const path = require('path')
const { ActionRowBuilder, ButtonBuilder, StringSelectMenuBuilder } = require('discord.js')
const { DISCORD_INVITE, BOT_ICON, EMPTY_ICON } = process.env
const { langFlag, fullnameLang, slogan, youtube: { playlist } } = require(path.join(require.main.path, 'config', 'index.js'))


module.exports = async ({ settings, command, selectMenuValues=[], statsFor, guild }) => {
    try {
        // const Setting = require(path.join(require.main.path, 'classes', 'Setting'))
        // const guildSettings = new Setting('guild', statsFor)
        const setLang = selectMenuValues[0]

        // await guildSettings.fetch() // получаем настройки сервера
        // guildSettings.data.lang = setLang
        // await guildSettings.set()

        const fieldData = setLang == 'ru' ? {
            name: 'Спасибо что используете бота ❤️',
            value: `Есть вопрос?, предложение? или хочешь быть всегда в курсе событий? - заходи на [сервер бота](${DISCORD_INVITE}).\n` +
                `Для получения помощи введите команду **\`/help\`**.\nМы подготовили видео примеры работы команд для вас [СМОТРЕТЬ НА ЮТУБЕ](${playlist}).\n` +
                '**Изменить язык для сервера** можно нажав на одну из кнопок ниже или воспользовавшись командой **`/settings`**.\n' +
                '```yaml\nПриятного пользования!```'
        } : {
            name: 'Thanks for using the bot ❤️',
            value: `Have a question ?, a suggestion? or do you want to be always up to date? - go to the [bot server](${DISCORD_INVITE}).\n` +
                `To get help, enter the command **\`/help\`**.\nWe have prepared video examples of how teams work for you [WATCH VIDEOS ON YOUTUBE](${playlist}).\n` +
                '**You can change the language for the server** by clicking on one of the buttons below or using the **`/settings`** command.\n' +
                'Enjoy your use.'
        }

        const options = []
        for (let langName in langFlag) {
            options.push({
                label: `${fullnameLang[langName]} [${langName.toUpperCase()}]`,
                value: langName,
                emoji: langFlag[langName]
            })
        }

        const buttonsLine_1 = new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
                .setCustomId(`guildcreate_${guild.ownerId}_${guild.id}_${guild.ownerId}_${guild.id}`)
                .setPlaceholder({ en: 'Select the Language for the server', ru: 'Выберите Язык для сервера' }[setLang])
                .addOptions(options)
        )

        const buttonsLine_2 = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`help_${guild.ownerId}_${guild.id}_newtab`)
            .setLabel({ en: 'Help', ru: 'Помощь' }[setLang])
            .setStyle('Danger')
            .setEmoji('❔')
        )
        .addComponents(
            new ButtonBuilder()
            .setURL(DISCORD_INVITE)
            .setLabel({en: 'Join bot server', ru: 'Посетить сервер бота'}[setLang])
            .setStyle('Link')
        )
        .addComponents(
            new ButtonBuilder()
            .setURL(playlist)
            .setLabel({en: 'Watch videos on YouTube', ru: 'Смотреть видео на Ютуб'}[setLang])
            .setStyle('Link')
        )

        return {
            status: 1,
            // defer: true,
            needEmbed: true,
            messageData: {
                content: {
                    ru: ':white_check_mark::white_check_mark::white_check_mark:```yaml\nЯзык сервера успешно изменен.```', 
                    en: ':white_check_mark::white_check_mark::white_check_mark:```yaml\nThe server language has been successfully changed.```'
                }[setLang],
                components: [ buttonsLine_1, buttonsLine_2 ],
                embeds: [{
                    author: {
                        name: `${{ en: 'Server lang', ru: 'Язык сервера' }[setLang]}: ${setLang}`,
                        icon_url: EMPTY_ICON
                    },
                    // title: `${{ en: 'Server lang', ru: 'Язык сервера' }[setLang]}: ${setLang}`,
                    description: `**Server name:** ${guild.name}\ \n**Server id:** ${guild.id}`,
                    thumbnail: {
                        // url: guild.iconURL()
                        url: `https://cdn.discordapp.com/icons/${guild.id}/${guild.icon}.webp`
                    },
                    color: '#22BE2D',
                    footer: {
                        icon_url: BOT_ICON,
                        text: slogan[setLang]
                    },
                    fields: [ fieldData ],
                    image: {
                        url: 'https://media.discordapp.net/attachments/786140964360159283/1010564646933115040/help.gif'
                    }
                }]
            }
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'guildcreate.button'
        }
    }
}