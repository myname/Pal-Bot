/**
 * функция которая выполняет комнаду принимая данные
 */
const path = require('path')
const { news } = require(path.join(require.main.path, 'config', 'index.js'))
const { ActionRowBuilder, ButtonBuilder } = require('discord.js')
const { resolveName } = require(path.join(require.main.path, 'utils', 'discord.js'))


module.exports = async ({ settings, command, statsFor, filters, db }) => {
    try {
        const { id: discordUserId, lang, timezone } = settings.user
        const nameOrId = resolveName(statsFor || discordUserId)

        // получаем данные с БД
        const commandsStats = [ ...db.stats ].reverse()
        // console.log(commandsStats)
        const maxValue = Math.ceil(commandsStats.length / 20)

        // console.log(filters)
        const [ filterType, filterValue=1 ] = filters
        let leftValue = filterValue - 1 < 1 ? 1 : filterValue - 1
        let rightValue = +filterValue + 1 > maxValue ? maxValue : +filterValue + 1
        // console.log(leftValue, rightValue, maxValue, commandsStats.length)

        // фильтруем
        let comStatsFiltered
        if (!filterType) {
            // commandsStats.splice(0, commandsStats.length - 20) // оставляем последние 20
            comStatsFiltered = commandsStats.slice(0, 20)
        } else if (filterType == 'start') {
            comStatsFiltered = commandsStats.slice(0, 20)
            leftValue = 1
            rightValue = 2
        } else if (filterType == 'end') {
            comStatsFiltered = commandsStats.slice((maxValue - 1) * 20, commandsStats.length)
            leftValue = maxValue - 1
            rightValue = maxValue
        } else {
            const filteredTo = filterValue * 20 + 20 > commandsStats.length ? commandsStats.length : filterValue * 20 + 20
            // console.log(filterValue * 20 - 20, filteredTo)
            // commandsStats.splice(filterValue * 20 - 20, commandsStats.length - 20 * filterValue)
            comStatsFiltered = commandsStats.slice(filterValue * 20 - 20, filteredTo)
        }

        // рисуем
        const draw = await command.draw(comStatsFiltered)
        if (!draw.status) throw draw

        const canvas = draw.canvas
        const buffer = canvas.toBuffer('image/png') // buffer image

        const buttonsLine_1 = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`menu_${discordUserId}_${nameOrId}`)
            .setLabel({en: 'Menu', ru: 'Меню'}[lang])
            .setStyle('Danger')
            .setEmoji('<:menu:943824092635758632>')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`bot_${discordUserId}_${nameOrId}_start`)
            .setLabel({en: 'To start', ru: 'В начало'}[lang])
            .setStyle('Secondary')
            .setEmoji('⏪')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`bot_${discordUserId}_${nameOrId}_left_${leftValue}`) // 1 минимум
            .setLabel({en: 'Left', ru: 'Влево'}[lang])
            .setStyle('Secondary')
            .setEmoji('◀️')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`bot_${discordUserId}_${nameOrId}_right_${rightValue}`) // не больше чем max / 20 Math.floor
            .setLabel({en: 'Right', ru: 'Вправо'}[lang])
            .setStyle('Secondary')
            .setEmoji('▶️')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`bot_${discordUserId}_${nameOrId}_end`)
            .setLabel({en: 'To end', ru: 'В конец'}[lang])
            .setStyle('Secondary')
            .setEmoji('⏩')
        )

        return {
            content: news[lang] || null,
            components: [ buttonsLine_1 ],
            files: [{
                attachment: buffer,
                name: `${nameOrId}.png`
            }]
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'bot.execute'
        }
    }
}