/**
 * Скрипт который только рисует указанную инфу в canvas и возвращает ее или ошибку
 */


/**
 * 
 * @param {Array} commandsStats - 
 */
module.exports = async (commandsStats) => {
    try {
        const path = require('path')
        const { colors } = require(path.join(require.main.path, 'config', 'index.js'))
        const { red, white, blue, black, orange, green, yellow, grey, lightGrey } = colors
        const { createCanvas } = require('canvas')

        const width = 910
        const height = 530
        const canvas = createCanvas(width, height)
        const ctx = canvas.getContext('2d')
        ctx.font = 'bold 16px GothamSSm_Bold'

        // верхняя часть
        ctx.fillStyle = black
        ctx.fillRect(0, 0, width, 30)

        // остальная часть
        ctx.fillStyle = grey
        ctx.fillRect(0, 30, width, height - 30)

        // таблица
        const table = [
            { name: `Дата`, x: 10 },
            { name: `servers`, x: 100 },
            { name: `total`, x: 175 },
            { name: `menu`, x: 230 },
            { name: `help`, x: 290 },
            { name: `setting`, x: 340 },
            { name: `me`, x: 415 },
            { name: `se`, x: 460 },
            { name: `ss`, x: 500 },
            { name: `sh`, x: 540 },
            { name: `sm`, x: 580 },
            { name: `sp`, x: 620 },
            { name: `st`, x: 665 },
            { name: `sc`, x: 700 },
            { name: `sl`, x: 735 },
            { name: `sf`, x: 776 },
            { name: `bot`, x: 810 }
        ]

        // рисуем таблицу
        ctx.fillStyle = blue
        table.forEach(tr => ctx.fillText(tr.name, tr.x, 20))

        ctx.fillStyle = red
        // console.log(commandsStats[0], commandsStats[commandsStats.length - 1], commandsStats.length)
        commandsStats.forEach((statsData, index) => {
            const { servers, used, com, date } = statsData
            const { menu, help, set, me, se, ss, sh, sm, sp, st, sl, sc, sf, bot } = com
            const i = (index + 1) * 25 + 25

            if (index == 0 || !(index % 2)) {
                ctx.fillStyle = lightGrey
                ctx.fillRect(0, i + 6, width, 25)
            }

            ctx.fillStyle = yellow
            const d = new Date(date)
            ctx.fillText(`${d.getDate()}/${d.getMonth() + 1}/${d.getFullYear()}`, table[0].x - 5, i)
            ctx.fillStyle = green
            ctx.fillText(servers || '-', table[1].x + 20, i)
            ctx.fillStyle = orange
            ctx.fillText(used || '-', table[2].x + 5, i)
            ctx.fillStyle = white
            ctx.fillText(menu || '-', table[3].x + 10, i)
            ctx.fillText(help || '-', table[4].x + 10, i)
            ctx.fillText(set || '-', table[5].x + 20, i)
            ctx.fillText(me || '-', table[6].x, i)
            ctx.fillText(se || '-', table[7].x, i)
            ctx.fillText(ss || '-', table[8].x, i)
            ctx.fillText(sh || '-', table[9].x, i)
            ctx.fillText(sm || '-', table[10].x, i)
            ctx.fillText(sp || '-', table[11].x, i)
            ctx.fillText(st || '-', table[12].x, i)
            ctx.fillText(sc || '-', table[13].x, i)
            ctx.fillText(sl || '-', table[14].x, i)
            ctx.fillText(sf || '-', table[15].x, i)
            ctx.fillText(bot || '-', table[16].x + 5, i)
        })

        return {
            status: true,
            canvas
        }
    } catch(error) {
        if ('err_msg' in error) throw error // проброс ошибки если есть описание
        throw {
            status: false,
            error,
            err_msg: {
                ru: 'Ошибка при обработке данных. Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Error in data processing. Try again or report this error to the bot creator.'
            },
            log_msg: 'Ошибка функции "bot.draw"'
        }
    }
}