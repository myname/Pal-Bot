/**
 * функция которая выполняет комнаду и отправляет результат пользователю (кнопки)
 */
const path = require('path')
const AbstractChampion = require(path.join(require.main.path, 'classes', 'AbstractChampion.js'))


module.exports = async ({ command, selectMenuValues=[], statsFor, branches, settings, db }) => {
    try {
        const typeValue = new Set(branches)

        let editReply = false
        let newReply = false
        let showCards = null
        let playerName = null

        const cardOpt = typeValue.has('card')
        let championName = selectMenuValues[0] || (cardOpt ? branches[2] : undefined)
        if (championName && championName.startsWith('_')) {
            const [ , playerNameTemp, name, ...cards ] = championName.split('_')
            championName = name
            editReply = true
            newReply = true
            showCards = cards
            playerName = playerNameTemp
        }
        // const championId = cardOpt ? branches[2] : undefined
        const number = cardOpt ? branches[1] : undefined

        // const champion = cardOpt ? champions.getById(championId || '') : champions.getByName(championName || '')
        const champion = AbstractChampion.nameNormalize(championName)
        const messageData = await command.execute({ settings, command, statsFor, champion, number, showCards, playerName, db })

        return {
            status: 1,
            messageData,
            // editReply: !!selectMenuValues.length,
            // newReply: !!selectMenuValues.length
        }
    } catch(error) {
        if ('err_msg'in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'sl.button'
        }
    }
}