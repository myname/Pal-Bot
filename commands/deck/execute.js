/**
 * функция которая выполняет комнаду и отправляет результат пользователю
 */
const path = require('path')
const AbstractChampion = require(path.join(require.main.path, 'classes', 'AbstractChampion.js'))
const { championsByRoles } = require(path.join(require.main.path, 'config', 'index.js'))
const { news } = require(path.join(require.main.path, 'config', 'index.js'))
const { ActionRowBuilder, ButtonBuilder, StringSelectMenuBuilder } = require('discord.js')


module.exports = async ({ settings, command, statsFor, champion, number, showCards, playerName, db }) => {
    try {
        const { id: discordUserId, lang, timezone } = settings.user

        if (showCards) {
            // если нужно показать чисто тупо колоду и все (без кнопок)

            const loadout = {
                ChampionName: champion,
                DeckName: playerName,
                LoadoutItems: [
                    {
                        ItemId: +showCards[0],
                        Points: +showCards[5]
                    },
                    {
                        ItemId: +showCards[1],
                        Points: +showCards[6]
                    },
                    {
                        ItemId: +showCards[2],
                        Points: +showCards[7]
                    },
                    {
                        ItemId: +showCards[3],
                        Points: +showCards[8]
                    },
                    {
                        ItemId: +showCards[4],
                        Points: +showCards[9]
                    }
                ]
            }

            // рисуем
            const draw = await command.draw({ loadout, champion, lang, timezone })
            if (!draw.status) throw draw

            const canvas = draw.canvas
            const buffer = canvas.toBuffer('image/png') // buffer image

            return {
                files: [{
                    attachment: buffer,
                    name: `${playerName}.png`
                }],
                ephemeral: true
            }
        }

        const { resolveName } = require(path.join(require.main.path, 'utils', 'discord.js'))
        const nameOrId = resolveName(statsFor || discordUserId)

        const body = await command.getStats(discordUserId, nameOrId, db)
        // console.log(body)
        const { getplayerloadouts } = body

        const championList = {}
        getplayerloadouts.data.forEach(loadout => {
            const name = loadout.ChampionName
            if (!championList[name]) championList[name] = []
            championList[name].push(loadout)
        })

        const buttonsLine_1 = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`menu_${discordUserId}_${nameOrId}`)
            .setLabel({en: 'Menu', ru: 'Меню'}[lang])
            .setStyle('Danger')
            .setEmoji('<:menu:943824092635758632>')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`deck_${discordUserId}_${nameOrId}`)
            .setLabel({ en: 'To decks', ru: 'К колодам' }[lang])
            .setStyle('Secondary')
            .setEmoji('<:cards:943453491907661845>')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`champions_${discordUserId}_${nameOrId}`)
            .setLabel({en: 'Champions', ru: 'Чемпионы'}[lang])
            .setStyle('Secondary')
            .setEmoji('<:champions:943447650647310356>')
        )

        if (champion) {
            buttonsLine_1
            .addComponents(
                new ButtonBuilder()
                .setCustomId(`champion_${discordUserId}_${nameOrId}${champion ? `_champion__${champion}` : ''}`)
                .setLabel({en: 'Champion statistics', ru: 'Статистика чемпиона'}[lang])
                .setStyle('Secondary')
                .setEmoji('<:champion:943440471601061888>')
            )
        } else {
            buttonsLine_1
            .addComponents(
                new ButtonBuilder()
                .setCustomId(`random_${discordUserId}_${nameOrId}`)
                .setLabel({ en: 'Random champion', ru: 'Случайный чемпион' }[lang])
                .setStyle('Secondary')
                .setDisabled(true)
            )
        }

        buttonsLine_1
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`builddeck_${discordUserId}_${nameOrId}`)
            .setLabel({en: 'Build a deck', ru: 'Создать колоду'}[lang])
            .setStyle('Secondary')
            .setEmoji('<:cards:943453491907661845>')
            .setDisabled(true)
        )

        const roleIcons = {
            flanker: '<:flank:943440471823360010>',
            damage: '<:damage:943440471554924554>',
            support: '<:support:943440471924023328>',
            frontline: '<:frontline:943440471743672320>'
        }
        const championsFilterOpts = []
        let champCounter = 0
        for (let championName in championList) {
        // championList.forEach((champion, i) => {
            const count = Math.floor(champCounter / 25) // массив по счету
            if (!(champCounter % 25)) championsFilterOpts.push([]) // новый массив

            // фильтруем оставляя нужного чемпиона
            const loadouts = getplayerloadouts.data.filter(ld => ld.ChampionName == championName)

            championsFilterOpts[count].push({
                label: championName,//championList[championName].ChampionName,
                description: {en: `Has ${loadouts.length} decks`, ru: `Имеет ${loadouts.length} колод`}[lang],
                value: championName,
                emoji: roleIcons[championsByRoles[AbstractChampion.nameNormalize(championName)]] || null
            })
            champCounter++
        }
        const championsOpt = []
        championsFilterOpts.forEach((optList, i) => {
            championsOpt.push(
                new ActionRowBuilder()
                .addComponents(
                    new StringSelectMenuBuilder()
                        .setCustomId(`deck_${discordUserId}_${nameOrId}_champion_${i}`)
                        .setPlaceholder({en: 'Show champion decks', ru: 'Показать колоды чемпиона'}[lang])
                        .addOptions(optList)
                )
            )
        })

        if (!champion) {
            return {
                content: {en: 'Choose a champion.', ru: 'Выберите чемпиона.'}[lang],
                components: [ buttonsLine_1, ...championsOpt ]
            }
        }

        // фильтруем оставляя нужного чемпиона
        const loadouts = getplayerloadouts.data.filter(ld => 
            AbstractChampion.nameNormalize(ld.ChampionName) == AbstractChampion.nameNormalize(champion))

        if (!loadouts.length) return {
            content: {
                ru: `Колоды для ${champion} не найдены.`,
                en: `No decks were found for ${champion}.`
            }[lang],
            components: [ buttonsLine_1, ...championsOpt ]
        }

        const showOldStatsText = {
            ru: 'Аккаунт скрыт или API временно не работает.\n__**Вам будут показаны данные последнего удачного запроса.**__',
            en: 'The account is hidden or the API is temporarily not working.\n__**You will be shown the details of the last successful request.**__'
        }

        const replayOldText = body.getplayerloadouts.old ?
                `${showOldStatsText[lang]}\n` : ''

        const emojiNumbers = [
            '<:1_:943870598541615114>', '<:2_:943870598789079060>', '<:3_:943870598545801218>',
            '<:4_:943870598667436083>', '<:5_:943870598625497089>', '<:6_:943870598550007829>',
            '<:7_:943870598935883776>', '<:8_:943870598470332487>', '<:9_:943870599023960154>'
        ]
        // если пользователь не указал колоду, а выбрать нужно ( их > 1)
        const buttonList = []
        if (loadouts.length > 5) {
            const buttonsLine_2 = new ActionRowBuilder()
            const buttonsLine_3 = new ActionRowBuilder()
            for (let i = 0; i < loadouts.length; i++) {
                const loadout = loadouts[i]
                const bt = i >= 3 ? buttonsLine_3 : buttonsLine_2
                bt.addComponents(
                    new ButtonBuilder()
                    .setCustomId(`deck_${discordUserId}_${nameOrId}_card_${i+1}_${champion}`)
                    .setLabel(loadout.DeckName || 'null')
                    .setStyle('Primary')
                    .setEmoji(emojiNumbers[i])
                )
            }
            buttonList.push(buttonsLine_2)
            buttonList.push(buttonsLine_3)
        } else {
            const buttonsLine_2 = new ActionRowBuilder()
            for (let i = 0; i < loadouts.length; i++) {
                const loadout = loadouts[i]
                buttonsLine_2.addComponents(
                    new ButtonBuilder()
                    .setCustomId(`deck_${discordUserId}_${nameOrId}_card_${i+1}_${champion}`)
                    .setLabel(loadout.DeckName || 'null')
                    .setStyle('Primary')
                    .setEmoji(emojiNumbers[i])
                )
            }
            buttonList.push(buttonsLine_2)
        }

        if (!number && loadouts.length > 1) {
            const componentsEnd = [ buttonsLine_1 ]
            if (buttonList.length) componentsEnd.push(...buttonList)
            return {
                content: {
                    ru: `(${champion}) Выберите одну из колод:`,
                    en: `(${champion}) Choose one of the decks:`
                }[lang],
                components: componentsEnd
            }
        }

        const numberDecks = Math.floor(number) || 1 // выбираем первую колоду если она одна
        const loadout = loadouts[numberDecks - 1] // выбираем колоду
        if (!loadout) {
            const componentsEnd = [ buttonsLine_1 ]
            if (buttonList.length) componentsEnd.push(...buttonList)
            return {
                content: {
                    ru: `Указанной колоды нет, у пользователя ${loadouts.length} колод.\n(${champion}) Выберите одну из колод:`,
                    en: `There is no specified deck, the user has ${loadouts.length} decks.\n(${champion}) Choose one of the decks:`
                }[lang],
                components: componentsEnd
            }
        }

        // рисуем
        const draw = await command.draw({ loadout, champion, lang, timezone, db })
        if (!draw.status) throw draw

        const canvas = draw.canvas
        const buffer = canvas.toBuffer('image/png') // buffer image

        const matchesInfo = `\`\`\`\n[${body.playerName}](${body.playerId})<${champion || ''} ${draw.name}>\`\`\``

        const componentsEnd = [ buttonsLine_1 ]
        if (buttonList.length) componentsEnd.push(...buttonList)

        return {
            content: `${news[lang]}${replayOldText}${matchesInfo}`,
            components: componentsEnd,
            files: [{
                attachment: buffer,
                name: `${nameOrId}.png`
            }]
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'sl.execute'
        }
    }
}