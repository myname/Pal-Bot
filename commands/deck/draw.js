/**
 * Скрипт который только рисует указанную инфу в canvas и возвращает ее или ошибку
 */


/**
 * 
 * @param {*} body - 
 * @param {Object} prop - 
 */
module.exports = async ({ loadout, champion, lang, timezone, db }) => {
    try {
        const path = require('path')
        const { createCanvas } = require('canvas')
        const { imageNormalize } = require(path.join(require.main.path, 'utils', 'canvas.js'))
        const { getCards, getCardFrame, formProposals, 
            getIconBot, getChampionsWallpapers, getRandomBackground } = require(path.join(require.main.path, 'utils', 'index.js'))
        const { colors } = require(path.join(require.main.path, 'config', 'index.js'))
        const { red, white, black, blue, transparent } = colors

        const width = 1648
        const height = 927
        const canvas = createCanvas(width, height)
        const ctx = canvas.getContext('2d')

        const wallpaper = await getChampionsWallpapers(champion)
        if (wallpaper) {
            const newCanvas = imageNormalize({ img: wallpaper, width, height })
            ctx.drawImage(newCanvas, 0, 0, width, height) // рисуем
        } else {
            const background = await getRandomBackground()
            const newCanvas = imageNormalize({ img: background, width, height })
            if (background) ctx.drawImage(newCanvas, 0, 0, width, height) // рисуем
        }

        ctx.fillStyle = transparent
        // ctx.fillRect(width / 4, 157, width / 2, 70) // прозрачный прямоугольник для названия колоды
        ctx.fillRect(0, 0, width, 80) // прозрачный прямоугольник для имени чемпиона
        ctx.fillRect(0, height - 60, width, 60) // прозрачный прямоугольник снизу

        // рисуем название колоды
        ctx.font = 'bold 50px Ledger'
        ctx.fillStyle = white
        // ctx.textAlign = 'center'
        // ctx.fillText(loadout.DeckName, width / 2, 210) // название колоды
        ctx.textAlign = 'start'
        ctx.fillText(`${champion.wordToUp()}: ${loadout.DeckName}`, 20, 60) // имя чемпиона

        // ctx.fillStyle = black
        // ctx.fillRect(0, height - 60, width, 60) // прямоугольник снизу
        // const lastUpdate = lastUpdate.updateToDate(timezone).toText()
        // const nextUpdate = lastUpdate.getNextUpdate('getplayerloadouts', timezone)
        // const loadoutsUpdateText = `${translate.Loadouts[lang]}: ${lastUpdate} | ${translate.Update[lang]}: ${nextUpdate}`
        ctx.fillStyle = blue
        // ctx.textAlign = 'start'
        ctx.font = 'bold 34px Ledger'
        // ctx.fillText(loadoutsUpdateText, 20,  height - 20)
        const text = {
            ru: 'Pal-Bot - Лучшая статистика по паладинс в наилучшем виде',
            en: 'Pal-Bot - The best statistics on paladins in the best possible way'
        }
        ctx.fillText(text[lang], 75,  height - 17)
        ctx.textAlign = 'center'
        ctx.font = 'bold 16px Ledger'

        const icon = await getIconBot()
        if (icon) ctx.drawImage(icon, 20, height - 53, 45, 45)

        // console.log(`\n\nloadout (${loadout.length}):`, loadout, '`\n')
        // const cardIds = loadout.LoadoutItems.map(card => card.ItemId)
        // const cardNames = loadout.LoadoutItems.map(card => card.ItemName)
        const cards = await getCards({ champion, LoadoutItems: loadout.LoadoutItems, lang, db })
        // console.log(`cardIds: `, cardIds, loadout)

        // console.log(`cards length: ${cards.length}`, cards)
        for (let i = 0; i < cards.length; i++) {
            const card = cards[i]
            const loadoutData = loadout.LoadoutItems.find(c => c.ItemName == card.nameEn)
            // console.log(card)
            const points = loadoutData.Points

            // const card = await getCards({ champion, id: item.ItemId, lang })
            // console.log(card)
            if (!card) continue; // если карта не найдена то пропускаем ее
            if (card.img) ctx.drawImage(card.img, i * (10 + 314) + 48, 150 + 200, 256, 196)

            const cardFrames = await getCardFrame(points)
            if (cardFrames) ctx.drawImage(cardFrames, i * (10 + 314) + 20, 100 + 200, 314, 479)

            //
            ctx.fillStyle = white
            ctx.font = 'bold 16px Ledger'
            // ctx.textAlign = 'center'
            ctx.fillText(card.name || card.nameEn, i * (10 + 314) + 178, 366 + 200)

            // рисуем описание карты
            ctx.fillStyle = black
            // ctx.textAlign = 'start'
            const description = card.getDescriptionLvl(points)
            const textArr = formProposals(description, 23)
            for (let k = 0; k < textArr.length; k++) {
                const x = i * (10 + 314) + 178
                const y = 20 * k + 400
                const spacePoop = String.fromCharCode(160)
                const text = textArr[k].replaceAll(new RegExp(`${spacePoop}`, 'g'), ' ')
                ctx.fillText(text, x, y + 200)
            }

            // откат карты
            if (card.recharge > 0) {
                ctx.fillStyle = red
                ctx.font = 'bold 34px Ledger'
                ctx.fillText(card.recharge, i * (10 + 314) + 178, 560 + 200)
            }
        }

        return {
            status: true,
            canvas,
            name: loadout.DeckName
        }
    } catch(error) {
        if ('err_msg' in error) throw error // проброс ошибки если есть описание
        throw {
            status: false,
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'Ошибка функции "sl.draw"'
        }
    }
}