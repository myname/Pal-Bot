/**
 * функция обрабатывающая команду (устаревшие)
 */
const path = require('path')
const AbstractChampion = require(path.join(require.main.path, 'classes', 'AbstractChampion.js'))


module.exports = async ({ settings, command, contentParams, db }) => {
    try {
        const [ statsFor, championOfParam, number ] = contentParams.split(' ')
        const champion = AbstractChampion.nameNormalize(championOfParam)

        return await command.execute({ settings, command, statsFor, champion, number, db })
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'sl.command'
        }
    }
}