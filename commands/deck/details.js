/**
 * функция возвращающая текст полного описания команды с примерами их работы
 */


module.exports = function(command, lang) {
    const path = require('path')
    const { slogan, youtube: { deck: videoURL }, gif: { deck: gif } } = require(path.join(require.main.path, 'config', 'index.js'))
    const { EMPTY_ICON, BOT_ICON } = process.env

    const embed = {
        title: {
            ru: '`Смотреть пример на ютубе`',
            en: '`Watch an example on YouTube`'
        }[lang],
        url: videoURL,
        image: {
            url: gif
        },
        description: {
            ru: `**${command.info[lang]}.**\`\`\`\n[/lodouts 'nickname' 'number' 'support' 'frontline' 'flank' 'damage'].\`\`\`` + 
            `\`\`\`\n-Параметры:\`\`\``,
            en: `**${command.info[lang]}.**\`\`\`\n[/lodouts 'nickname' 'number' 'support' 'frontline' 'flank' 'damage'].\`\`\`` + 
            `\`\`\`\n-Params:\`\`\``
        }[lang],
        color: '3092790',
        footer: {
            icon_url: BOT_ICON,
            text: slogan[lang]
        },
        author: {
            name: `/${command.slashName || command.name}`,
            icon_url: EMPTY_ICON
        },
        fields: [
            {
                name: 'nickname:',
                value: {
                    ru: `\`\`\`css\n[Ник или id игрока, статистику которого вы хотите увидеть].\nВведите сюда никнейм ` + 
                        `игрока, чью статистику хотите посмотреть.\`\`\``,
                    en: `\`\`\`css\n[Nickname or id of the player whose stats you want to see].\nEnter here the nickname ` + 
                        `of the player whose statistics you want to see.\`\`\``
                }[lang]
            },
            {
                name: 'number:',
                value: {
                    ru: `\`\`\`css\n[Номер колоды для отображения].\n` + 
                    `Этот параметр лучше пропустить, если точно не знаешь каков будет результат.\`\`\``,
                    en: `\`\`\`css\n[Deck number to display].\n` + 
                    `It's better to skip this parameter if you don't know exactly what the result will be.\`\`\``
                }[lang]
            },
            {
                name: 'support:',
                value: {
                    ru: `\`\`\`css\n[Выберите чемпиона в одной из категорий (Саппорт)].\nМожно выбрать только одного чемпиона.\`\`\``,
                    en: `\`\`\`css\n[Choose a champion from one of the categories (Support)].\nOnly one champion can be selected.\`\`\``
                }[lang]
            },
            {
                name: 'frontline:',
                value: {
                    ru: `\`\`\`css\n[Выберите чемпиона в одной из категорий (Танк)].\nМожно выбрать только одного чемпиона.\`\`\``,
                    en: `\`\`\`css\n[Choose a champion from one of the categories (Frontline)].\nOnly one champion can be selected.\`\`\``
                }[lang]
            },
            {
                name: 'flank:',
                value: {
                    ru: `\`\`\`css\n[Выберите чемпиона в одной из категорий (Фланг)].\nМожно выбрать только одного чемпиона.\`\`\``,
                    en: `\`\`\`css\n[Choose a champion from one of the categories (Flank)].\nOnly one champion can be selected.\`\`\``
                }[lang]
            },
            {
                name: 'damage:',
                value: {
                    ru: `\`\`\`css\n[Выберите чемпиона в одной из категорий (Урон)].\nМожно выбрать только одного чемпиона.\`\`\``,
                    en: `\`\`\`css\n[Choose a champion from one of the categories (Damage)].\nOnly one champion can be selected.\`\`\``
                }[lang]
            }
        ]
    }

    return {
        embeds: [ embed ],
        ephemeral: true
    }
}