/**
 * функция возвращающая обьект слеш-команды
 */


module.exports = () => {
    return {
        name: 'last',
        description: 'Displays statistics of the last (or specified) match or player',
        options: [
            {
                name: 'nickname',
                description: 'Nickname or id of the player whose stats you want to see',
                type: 3
            },
            {
                name: 'match',
                description: 'Show match statistics by match ID',
                type: 3
            },
            {
                name: 'count',
                description: 'Specify which match according to the score from the match history you want to see',
                type: 3
            }
        ]
    }
}