/**
 * функция возвращающая текст полного описания команды с примерами их работы
 */


module.exports = function(command, lang) {
    const path = require('path')
    const { slogan, youtube: { last: videoURL }, gif: { last: gif } } = require(path.join(require.main.path, 'config', 'index.js'))
    const { EMPTY_ICON, BOT_ICON } = process.env

    const embed = {
        title: {
            ru: '`Смотреть пример на ютубе`',
            en: '`Watch an example on YouTube`'
        }[lang],
        url: videoURL,
        image: {
            url: gif
        },
        description: {
            ru: `**${command.info[lang]}.**\`\`\`\n[/stats 'nickname' 'match' 'count']\`\`\` \`\`\`\n-Параметры:\`\`\``,
            en: `**${command.info[lang]}.**\`\`\`\n[/stats 'nickname' 'match' 'count']\`\`\` \`\`\`\n-Params:\`\`\``
        }[lang],
        color: '3092790',
        footer: {
            icon_url: BOT_ICON,
            text: slogan[lang]
        },
        author: {
            name: `/${command.name}`,
            icon_url: EMPTY_ICON
        },
        fields: [
            {
                name: 'nickname:',
                value: {
                    ru: `\`\`\`\n[Ник или id игрока, статистику которого вы хотите увидеть].\nВведите сюда никнейм ` + 
                        `игрока, чью статистику хотите посмотреть.\`\`\``,
                    en: `\`\`\`\n[Nickname or id of the player whose stats you want to see].\nEnter here the nickname ` + 
                        `of the player whose statistics you want to see.\`\`\``
                }[lang]
            },
            {
                name: 'match:',
                value: {
                    ru: `\`\`\`\n[Показать статистику матча по идентификатору матча].\nЕсли вы укажите этот параметр, то ` + 
                    `вам покажет статистику указанного ID матча, проигнорировав остальные параметры.\`\`\``,
                    en: `\`\`\`\n[Show match statistics by match ID].\nIf you specify this parameter, ` + 
                    `the statistics of the specified match ID will be shown to you, ignoring the other parameters.\`\`\``
                }[lang]
            },
            {
                name: 'count:',
                value: {
                    ru: `\`\`\`\n[Укажите какой матч по счету из истории матчей вы хотите увидеть].\nПозволяет выбрать какой матч ` + 
                    `показать из истории матчей. ['Должен быть числом!'].\nНапример у вас в истории матчей всего сыграно 35 игр, ` + 
                    `это значит что максимальное число которое вы можете вписать - это 35, а минимальное - 1.\nЕсли вы укажите 1, ` + 
                    `то вам покажет последний матч.\nЕсли же вы укажите 2, то вам покажет предпоследний матч.\n[Эта опция не обязательна! ` + 
                    `если вы ничего не укажите, то бот автоматически укажет '1' - последний матч].\`\`\``,
                    en: `\`\`\`\n[Specify which match according to the score from the match history you want to see].\nAllows you ` + 
                    `to choose which match to show from the match history. ['Must be a number!'].\nFor example, you have a total of 35 games ` + 
                    `played in the history of matches, which means that the maximum number that you can enter is 35, and the minimum is 1.\n` + 
                    `If you specify 1, then the last match will show you.\nIf you specify 2, then the penultimate match will show you.` + 
                    `[This option is optional! if you do not specify anything, the bot will automatically indicate '1' - the last match].\`\`\``
                }[lang]
            }
        ]
    }

    return {
        embeds: [ embed ],
        ephemeral: true
    }
}