/**
 * функция которая выполняет комнаду и отправляет результат пользователю
 */
const path = require('path')
const { news, platforms } = require(path.join(require.main.path, 'config', 'index.js'))
const { resolveName } = require(path.join(require.main.path, 'utils', 'discord.js'))
const { ActionRowBuilder, ButtonBuilder, StringSelectMenuBuilder } = require('discord.js')


module.exports = async ({ settings, command, statsFor, matchId, matchNumber, pageShow=1, db }) => {
    try {
        const { id: discordUserId, lang, timezone } = settings.user
        const nameOrId = resolveName(statsFor || discordUserId)

        const buttonsLine_1 = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`menu_${discordUserId}_${nameOrId}`)
            .setLabel({en: 'Menu', ru: 'Меню'}[lang])
            .setStyle('Danger')
            .setEmoji('<:menu:943824092635758632>')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`history_${discordUserId}_${nameOrId}_pageShow_${pageShow}`)
            .setLabel({en: 'Back to history', ru: 'Назад к истории'}[lang])
            .setStyle('Secondary')
            .setEmoji('<:history:943818397009985597>')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`last_${discordUserId}_${nameOrId}_prev_${matchNumber}_pageShow_${pageShow}`)
            .setLabel({en: 'Previous match', ru: 'Предыдущий матч'}[lang])
            .setStyle('Primary')
            .setEmoji('<:left:943831632312401991>')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`last_${discordUserId}_${nameOrId}_next_${matchNumber}_pageShow_${pageShow}`)
            .setLabel({en: 'Next match', ru: 'Следующий матч'}[lang])
            .setStyle('Primary')
            .setEmoji('<:right:943831760372895754>')
        )

        if (
            (matchId && !isFinite(matchId)) || 
            (matchId && (matchId+'').length < 9) || 
            (matchNumber && !isFinite(matchNumber)) || 
            (matchNumber && matchNumber > 50)
        ) {
            return {
                content: `:warning::warning::warning: \`\`\`\n
                    ${{en: 'Incorrect data was entered.', ru: 'Введены не корректные данные.'}[lang]}\`\`\``,
                components: [ buttonsLine_1 ]
            }
        }

        // console.log(`statsFor: ${statsFor}; matchNumber: ${matchNumber}; modifier: ${modifier}`)
        // console.log(discordUserId, nameOrId, matchId, matchNumber)
        const body = await command.getStats(discordUserId, nameOrId, matchId, matchNumber, db)
        // console.log(body)
        const { getmatchdetails } = body
        const match = getmatchdetails.data

        if (!match.length) {
            // если нет матчей
            return {
                content: `${news[lang]}${{
                    ru: '**У игрока нет матчей.**',
                    en: '**The player has no matches.**'
                }[lang]}`,
                components: [ buttonsLine_1 ]
            }
        }

        // const matchId = match[0].Match || match[1].Match || match[2].Match || match[3].Match || match[4].Match || match[5].Match

        const emojiNumbers = [
            '<:1_:943870598541615114>', '<:2_:943870598789079060>', '<:3_:943870598545801218>',
            '<:4_:943870598667436083>', '<:5_:943870598625497089>', '<:6_:943870598550007829>',
            '<:7_:943870598935883776>', '<:8_:943870598470332487>', '<:9_:943870599023960154>',
            '<:10:943870599040729098>'
        ]

        const optionsForStats = []
        const optionsForDecks = []
        match.forEach((player, i) => {
            optionsForDecks.push({
                label: `${player.playerName || '-'} (${player.Account_Level} lvl)`,
                description: `LVL: ${player.Account_Level} (${player.Reference_Name})`,
                value: `_${player.playerName || '-'}_${player.Reference_Name}_${player.ItemId1}_${player.ItemId2}_${player.ItemId3}_${player.ItemId4}_` + 
                    `${player.ItemId5}_${player.ItemLevel1}_${player.ItemLevel2}_${player.ItemLevel3}_${player.ItemLevel4}_` + 
                    `${player.ItemLevel5}`,
                emoji: emojiNumbers[i]
            })

            const id = +player.playerId
            if (!id) return

            optionsForStats.push({
                label: `${player.playerName} (${player.Account_Level} lvl)`,
                description: `${player.Reference_Name} (${player.Mastery_Level} lvl)`,
                value: `_${id}`,
                emoji: emojiNumbers[i]
            })
        })

        const buttonsLine_2 = new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
            .setCustomId(`stats_${discordUserId}_${nameOrId}`)
            .setPlaceholder({en: 'Show player statistics', ru: 'Показать статистику игрока'}[lang])
            .addOptions(optionsForStats)
        )

        const buttonsLine_3 = new ActionRowBuilder() // нужно что бы эту команду мог использовать любой (скрытый ответ)
        .addComponents(
            new StringSelectMenuBuilder()
            .setCustomId(`deck_${discordUserId}_${nameOrId}`)
            .setPlaceholder({en: 'Show player decks', ru: 'Показать колоду игрока'}[lang])
            .addOptions(optionsForDecks)
        )

        const draw = await command.draw({ body, lang, timezone })
        if (!draw.status) throw draw

        const canvas = draw.canvas
        const buffer = canvas.toBuffer('image/png') // buffer image

        const showOldStatsText = {
            ru: 'Аккаунт скрыт или API временно не работает.\n__**Вам будут показаны данные последнего удачного запроса.**__',
            en: 'The account is hidden or the API is temporarily not working.\n__**You will be shown the details of the last successful request.**__'
        }

        const replayOldText = getmatchdetails.old ?
                `${showOldStatsText[lang]}\n` : ''

        const matchInfoText = match.map((pl, i) => `${i+1}. [${pl.Reference_Name}](${pl.playerName})<id ${pl.playerId}> ` +
                `[${platforms[pl.playerPortalId]}](${pl.playerPortalUserId})`).join('\n')


        const matchInfo = `\`\`\`\n[]()<id ${draw.matchId}>\n\n${matchInfoText}\`\`\``

        return {
            content: `${news[lang]}${replayOldText}${matchInfo}`,
            components: [ buttonsLine_1, buttonsLine_2, buttonsLine_3 ],
            files: [{
                attachment: buffer,
                name: `${nameOrId}.png`
            }]
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'sm.execute'
        }
    }
}