/**
 * функция которая получает данные статистик и обрабатывает возможные ошибки связанные с этим
 */
const path = require('path')
const { getUserSavedData } = require(path.join(require.main.path, 'utils', 'index.js'))


module.exports = async function( discordUserId, nameOrId, matchId, matchNumber=1, db ) {
    try {
        const { api: hirez } = db

        matchNumber = matchNumber || 1
        if (matchId) {
            const fetchGetmatchdetails = await hirez.getmatchdetails(matchId)
            if (!fetchGetmatchdetails.status) throw fetchGetmatchdetails
            const getmatchdetails = fetchGetmatchdetails.data // если данных нет то ничего страшного

            return {
                getmatchdetails: {
                    lastUpdate: fetchGetmatchdetails.lastUpdate,
                    data: getmatchdetails,
                    old: fetchGetmatchdetails.old
                },
                status: true
            }
        }

        let { data: userSavedData, type: typeData, playerId, playerName } = await getUserSavedData(nameOrId, discordUserId, true, db)

        if (typeData == 'match') { // если есть id матча то сразу поулчаем его
            const fetchGetmatchdetails = await hirez.getmatchdetails(userSavedData)
            if (!fetchGetmatchdetails.status) throw fetchGetmatchdetails

            const getmatchdetails = fetchGetmatchdetails.data // если данных нет то ничего страшного

            return {
                getmatchdetails: {
                    lastUpdate: fetchGetmatchdetails.lastUpdate,
                    data: getmatchdetails
                },
                playerId: 'me',
                playerName: 'me',
                status: true
            }
        }

        // let playerId = userSavedData?.playerId || (typeData == 'player' ? nameOrId : null)
        // const playerName = userSavedData?.name || (typeData == 'name' ? nameOrId : null)

        if (!playerId && !playerName) throw {
            status: false,
            err_msg: {
                ru: `У пользователя нет сохраненного никнейма.\n=\nИспользуйте команду "/me ТвойНик" (без кавычек) что бы сохранить свой никнейм.`,
                en: `The user does not have a saved nickname.\n=\nUse the command "/me YourNickname" (without quotes) to save your nickname.`
            }
        }

        if (!playerId) { // если нет id то делаем запрос для получения id
            const fetchGetplayer = await hirez.getplayer(playerName)
            if (!fetchGetplayer.status) throw fetchGetplayer

            const getplayer = fetchGetplayer.data[0]
            playerId = getplayer?.Id
            if (!getplayer || !playerId) throw {
                status: false,
                err_msg: {
                    ru: 'Аккаунт скрыт.',
                    en: 'The account is hidden.'
                }
            }
        }

        const fetchGetmatchhistory = await hirez.getmatchhistory(playerId)
        if (!fetchGetmatchhistory.status) throw fetchGetmatchhistory

        const getmatchhistory = fetchGetmatchhistory.data // если данных нет то ничего страшного
        const match = getmatchhistory[matchNumber-1]

        if (!match) throw {
            status: false,
            err_msg: {
                ru: `Матч ${matchNumber} не найден, всего ${getmatchhistory.length} матчей`,
                en: `Match ${matchNumber} not found, total ${getmatchhistory.length} matches`
            }
        }

        matchId = match.Match
        const fetchGetmatchdetails = await hirez.getmatchdetails(matchId)
        if (!fetchGetmatchdetails.status) throw fetchGetmatchdetails
        const getmatchdetails = fetchGetmatchdetails.data // если данных нет то ничего страшного

        return {
            getmatchdetails: {
                lastUpdate: fetchGetmatchdetails.lastUpdate,
                data: getmatchdetails,
                old: fetchGetmatchdetails.old
            },
            playerId,
            playerName,
            status: true
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'sm.getStats'
        }
    }
}