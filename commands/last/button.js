/**
 * функция которая выполняет комнаду и отправляет результат пользователю (кнопки)
 */


module.exports = async ({ command, selectMenuValues=[], statsFor, branches, settings, db }) => {
    try {
        const matchDate = selectMenuValues[0] ? selectMenuValues[0] : ''
        const [ matchId, num ] = matchDate.split('_')

        const [ type, val, pageShowType, pageShowData ] = branches
        const pageShow = pageShowData || pageShowType || (!isNaN(+type) ? type : 1) || 1
        const nextNum = val || num
        // console.log(branches)
        // console.log(nextNum)
        const matchNumber = (type == 'next' ? +nextNum + 1 : type == 'prev' ? nextNum - 1 : nextNum) || 1
        // const matchNumber = (type == 'next' ? +num + 1 : num - 1) || 1
        // console.log(branches, values)

        const messageData =  await command.execute({ settings, command, statsFor, matchId, matchNumber, pageShow, db })

        return {
            status: 1,
            messageData
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'sm.button'
        }
    }
}