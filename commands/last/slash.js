/**
 * функция обрабатывающая слеш-команду
 */


module.exports = async ({ command, settings, options, db }) => {
    try {
        const statsFor = options.getString('nickname') || 'me'
        const matchId = options.getString('match')
        const matchNumber = options.getString('count') || 1

        const messageData = await command.execute({ settings, command, statsFor, matchId, matchNumber, db })

        return {
            status: 1,
            messageData
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'sm.slash'
        }
    }
}