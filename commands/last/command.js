/**
 * функция обрабатывающая команду (устаревшие)
 */


module.exports = async ({ settings, command, contentParams, db }) => {
    try {
        const [ firstParam, secondParam, thirdParam ] = contentParams.split(' ')

        const userNameOrIdParam = (isFinite(firstParam) && firstParam.length > 6) || firstParam === 'me' || (/^[^0-9\-]/i.test(firstParam) && firstParam !== undefined) ? firstParam :
        (isFinite(secondParam) && secondParam.length > 6) || secondParam === 'me' || (/^[^0-9\-]/i.test(secondParam) && secondParam !== undefined) ? secondParam :
        (isFinite(thirdParam) && thirdParam.length > 6) || thirdParam === 'me' || (/^[^0-9\-]/i.test(thirdParam) && thirdParam !== undefined) ? thirdParam : 'me'

        const matchId = isFinite(userNameOrIdParam) && (userNameOrIdParam + '').length > 9 && (userNameOrIdParam + '').length < 15 ? userNameOrIdParam : null
        const userNameOrId = matchId ? 'me' : userNameOrIdParam

        // матч по счету в итории матчей (не всегда нужен будет)
        const matchNumber = isFinite(firstParam) && firstParam > 1 && firstParam < 51 ? Math.floor(firstParam) :
            isFinite(secondParam) && secondParam > 1 && secondParam < 51 ? Math.floor(secondParam) :
            isFinite(thirdParam) && thirdParam > 1 && thirdParam < 51 ? Math.floor(thirdParam) : 1

        return await command.execute({ settings, command, statsFor: userNameOrId, matchId, matchNumber, db })
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'sm.command'
        }
    }
}