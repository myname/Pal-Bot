/**
 * Скрипт который только рисует указанную инфу в canvas и возвращает ее или ошибку
 */
const path = require('path')
const { colors } = require(path.join(require.main.path, 'config', 'index.js'))
const { createCanvas } = require('canvas')
const { imageNormalize } = require(path.join(require.main.path, 'utils', 'canvas.js'))
const AbstractChampion = require(path.join(require.main.path, 'classes', 'AbstractChampion.js'))
const { getChampionIcon, getDivision, getIconBot, getCard, getItem } = require(path.join(require.main.path, 'utils', 'index.js'))
const { getMap } = require(path.join(require.main.path, 'utils', 'index.js'))


/**
 * 
 * @param {*} body - 
 * @param {Object} prop - 
 */
module.exports = async function({ body, lang, timezone }) {
    try {
        const { getmatchdetails } = body
        const match = getmatchdetails.data
        const width = 1225
        const height = 795
        const canvas = createCanvas(width, height)
        const ctx = canvas.getContext('2d')

        // рисуем дефолтные
        const resDefault = await drawDefault({ ctx, match, lang, timezone, width, height, colors })
        if (!resDefault.status) throw resDefault

        // рисуем таблицу
        const resTable = await drawTable({ ctx, match, lang, timezone, width, height, colors })
        if (!resTable.status) throw resTable

        return {
            status: true,
            canvas,
            matchId: match[0].Match
        }
    } catch(error) {
        if ('err_msg' in error) throw error // проброс ошибки если есть описание
        throw {
            status: false,
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'Ошибка функции "sm.draw"'
        }
    }
}


async function drawDefault({ ctx, match, lang, timezone, width, height, colors }) {
    try {
        const { red, white, black, green, yellow, transparent} = colors

        ctx.fillStyle = black
        ctx.fillRect(0, 0, width, 30) // прямоугольник сверху

        const matchOne = match[0]
        if (!matchOne.Map_Game) throw {
            status: false,
            err_msg: {
                ru: 'Нет данных карты, вероятно API не работает (хайрезы скоро подчинят мб).',
                en: 'There is no map data, probably the API does not work (hi-rez will soon subdue).'
            }
        }
        // инфа по центру
        const getMapMatch = await getMap(matchOne.Map_Game)
        const mapImg = getMapMatch.img
        const mapName = mapImg ? getMapMatch.name : '-'

        if (mapImg) {
            const newCanvas = imageNormalize({ img: mapImg, width, height: height - 60 })
            if (newCanvas) ctx.drawImage(newCanvas, 0, 30, width, height - 30) // рисуем карту на весь экран
        }

        // затемняющий фон
        ctx.fillStyle = transparent
        ctx.fillRect(0, 30, width, height - 30)

        // иконка бота
        const botIcon = await getIconBot()
        if (botIcon) ctx.drawImage(botIcon, 37, 330, 130, 130)
        ctx.fillStyle = white
        ctx.font = 'bold 40px Ledger'
        ctx.fillText('Pal-Bot', 25, 500)

        ctx.font = 'bold 20px Ledger'
        
        const typeMatch = matchOne.name
        ctx.fillStyle = yellow
        ctx.fillText(mapName, 240, 345)
        ctx.fillStyle = white
        ctx.fillText(`${matchOne.Minutes} ${{ ru: 'Минут', en: 'Minutes' }[lang]}`, 240, 375)
        ctx.fillText(`${{ ru: 'Регион', en: 'Region' }[lang]}: ${matchOne.Region}`, 240, 405)
        ctx.fillText(typeMatch, 240, 435)
        const hasReplay = matchOne.hasReplay != 'n'
        const replayText = hasReplay ? {ru: 'есть', en: 'has'} : {ru: 'нету', en: 'no'}
        ctx.fillStyle = hasReplay ? green : red
        ctx.fillText(`Replay: ${replayText[lang]}`, 240, 465)
        ctx.fillStyle = yellow
        ctx.fillText(new Date(matchOne.Entry_Datetime).addHours(timezone).toText(), 240, 495)

        const winTeam1 = match.reduce((prev, current) => {
            return current.TaskForce == 1 ? (prev + +current.League_Wins) : prev
        }, 0)

        const lossTeam1 = match.reduce((prev, current) => {
            return current.TaskForce == 1 ? (prev + +current.League_Losses) : prev
        }, 0)

        const winTeam2 = match.reduce((prev, current) => {
            return current.TaskForce == 2 ? (prev + +current.League_Wins) : prev
        }, 0)

        const lossTeam2 = match.reduce((prev, current) => {
            return current.TaskForce == 2 ? (prev + +current.League_Losses) : prev
        }, 0)

        const winrateTeam1 = AbstractChampion.getWinrate(winTeam1, lossTeam1)
        const winrateTeam2 = AbstractChampion.getWinrate(winTeam2, lossTeam2)

        const tierTeam1 = match.reduce((prev, current) => {
            return current.TaskForce == 1 ? (prev + +current.League_Tier) : prev
        }, 0)

        const tierHaveTeam1 = match.reduce((prev, current) => {
            return current.TaskForce == 1 && +current.League_Tier ? (prev + 1) : prev
        }, 0)

        const tierTeam2 = match.reduce((prev, current) => {
            return current.TaskForce == 2 ? (prev + +current.League_Tier) : prev
        }, 0)

        const tierHaveTeam2 = match.reduce((prev, current) => {
            return current.TaskForce == 2 && +current.League_Tier ? (prev + 1) : prev
        }, 0)

        const avgTierTeam1 = Math.round(tierTeam1 / tierHaveTeam1)
        const avgTierTeam2 = Math.round(tierTeam2 / tierHaveTeam2)
        // console.log(tierTeam1, tierHaveTeam1, avgTierTeam1)

        ctx.fillStyle = white
        const textCommandWin = { ru: 'Команда', en: 'Team' }[lang]
        const textCommandLose = { ru: 'Команда', en: 'Team' }[lang]
        const textScore = { ru: 'Счет', en: 'Score' }[lang]

        const winStatus = matchOne.Win_Status == 'Winner'
        if (winStatus && matchOne.TaskForce == 2) {
            ctx.fillStyle = green
            ctx.fillText(`${textCommandWin} 2 ${{ ru: 'ПОБЕДИЛА', en: 'WINNING'}[lang]}`, 600, 341)
            ctx.fillStyle = red
            ctx.fillText(`${textCommandLose} 1 ${{ ru: 'ПРОИГРАЛА', en: 'LOSING'}[lang]}`, 600, 497)
            ctx.fillStyle = yellow
            if (matchOne.name == 'Ranked') ctx.fillText(`Winrate: ${winrateTeam2}`, 600, 373)
            if (matchOne.name == 'Ranked') ctx.fillText(`Winrate: ${winrateTeam1}`, 600, 466)
            ctx.fillStyle = white
            ctx.font = 'bold 35px Ledger'
            if (typeMatch.search('Team Deathmatch') !== -1) {
                const sumDeathWin = match.filter(m => m.Win_Status == 'Winner').reduce((prev, current) => {
                    return prev + (current.Deaths || 0)
                }, 0)
                const sumDeathLose = match.filter(m => m.Win_Status != 'Winner').reduce((prev, current) => {
                    return prev + (current.Deaths || 0)
                }, 0)
                ctx.fillText(`${textScore}: ${sumDeathWin}:${sumDeathLose}`, 630, 423)
            } else if (typeMatch.search('Onslaught') !== -1) {
                // ничего
            } else {
                ctx.fillText(`${textScore}: ${matchOne.Team2Score}:${matchOne.Team1Score}`, 630, 423)
            }
            ctx.font = 'bold 20px Ledger'
            if (matchOne.name == 'Ranked') {
                const avgDivesionTeam1 = await getDivision(avgTierTeam1)
                const avgDivesionTeam2 = await getDivision(avgTierTeam2)
                if (avgDivesionTeam1) ctx.drawImage(avgDivesionTeam1, 500, 325, 80, 80) // рисуем ранг только в рейте
                if (avgDivesionTeam2) ctx.drawImage(avgDivesionTeam2, 500, 425, 80, 80) // рисуем ранг только в рейте
            }
        } else {
            ctx.fillStyle = green
            ctx.fillText(`${textCommandWin} 1 ${{ ru: 'ПОБЕДИЛА', en: 'WINNING'}[lang]}`, 600, 341)
            ctx.fillStyle = red
            ctx.fillText(`${textCommandLose} 2 ${{ ru: 'ПРОИГРАЛА', en: 'LOSING'}[lang]}`, 600, 497)
            ctx.fillStyle = yellow
            if (matchOne.name == 'Ranked') ctx.fillText(`Winrate: ${winrateTeam1}`, 600, 373)
            if (matchOne.name == 'Ranked') ctx.fillText(`Winrate: ${winrateTeam2}`, 600, 466)
            ctx.fillStyle = white
            ctx.font = 'bold 35px Ledger'
            if (typeMatch.search('Team Deathmatch') !== -1) {
                const sumDeathWin = match.filter(m => m.Win_Status == 'Winner').reduce((prev, current) => {
                    return prev + (current.Deaths || 0)
                }, 0)
                const sumDeathLose = match.filter(m => m.Win_Status != 'Winner').reduce((prev, current) => {
                    return prev + (current.Deaths || 0)
                }, 0)
                ctx.fillText(`${textScore}: ${sumDeathWin}:${sumDeathLose}`, 630, 423)
            } else if (typeMatch.search('Onslaught') !== -1) {
                // ничего
            } else {
                ctx.fillText(`${textScore}: ${matchOne.Team2Score}:${matchOne.Team1Score}`, 630, 423)
            }
            ctx.font = 'bold 20px Ledger'
            if (matchOne.name == 'Ranked') {
                const avgDivesionTeam1 = await getDivision(avgTierTeam1)
                const avgDivesionTeam2 = await getDivision(avgTierTeam2)
                if (avgDivesionTeam1) ctx.drawImage(avgDivesionTeam1, 500, 325, 80, 80) // рисуем ранг только в рейте
                if (avgDivesionTeam2) ctx.drawImage(avgDivesionTeam2, 500, 425, 80, 80) // рисуем ранг только в рейте
            }
        }

        ctx.textAlign = "start"
        ctx.fillStyle = yellow
        if (typeMatch == 'Ranked') ctx.fillText(`${{ ru: 'Баны', en: 'Bans' }[lang]}:`, 865, 420)
        ctx.fillStyle = white
        ctx.font = 'bold 16px Ledger'
        if (winStatus && matchOne.TaskForce == 2) {
            const Ban2Name = AbstractChampion.nameNormalize(matchOne.Ban_2)
            const Ban_2 = await getChampionIcon(Ban2Name)
            if (Ban_2) ctx.drawImage(Ban_2, 960, 360, 50, 50)

            const Ban4Name = AbstractChampion.nameNormalize(matchOne.Ban_4)
            const Ban_4 = await getChampionIcon(Ban4Name)
            if (Ban_4) ctx.drawImage(Ban_4, 1020, 360, 50, 50)

            const Ban5Name = AbstractChampion.nameNormalize(matchOne.Ban_5)
            const Ban_5 = await getChampionIcon(Ban5Name)
            if (Ban_5) ctx.drawImage(Ban_5, 1080, 360, 50, 50)

            const Ban7Name = AbstractChampion.nameNormalize(matchOne.Ban_7)
            const Ban_7 = await getChampionIcon(Ban7Name)
            if (Ban_7) ctx.drawImage(Ban_7, 1140, 360, 50, 50)

            const Ban1Name = AbstractChampion.nameNormalize(matchOne.Ban_1)
            const Ban_1 = await getChampionIcon(Ban1Name)
            if (Ban_1) ctx.drawImage(Ban_1, 960, 420, 50, 50)

            const Ban3Name = AbstractChampion.nameNormalize(matchOne.Ban_3)
            const Ban_3 = await getChampionIcon(Ban3Name)
            if (Ban_3) ctx.drawImage(Ban_3, 1020, 420, 50, 50)

            const Ban6Name = AbstractChampion.nameNormalize(matchOne.Ban_6)
            const Ban_6 = await getChampionIcon(Ban6Name)
            if (Ban_6) ctx.drawImage(Ban_6, 1080, 420, 50, 50)

            const Ban8Name = AbstractChampion.nameNormalize(matchOne.Ban_8)
            const Ban_8 = await getChampionIcon(Ban8Name)
            if (Ban_8) ctx.drawImage(Ban_8, 1140, 420, 50, 50)
        } else {
            const Ban1Name = AbstractChampion.nameNormalize(matchOne.Ban_1)
            const Ban_1 = await getChampionIcon(Ban1Name)
            if (Ban_1) ctx.drawImage(Ban_1, 960, 360, 50, 50)

            const Ban3Name = AbstractChampion.nameNormalize(matchOne.Ban_3)
            const Ban_3 = await getChampionIcon(Ban3Name)
            if (Ban_3) ctx.drawImage(Ban_3, 1020, 360, 50, 50)

            const Ban6Name = AbstractChampion.nameNormalize(matchOne.Ban_6)
            const Ban_6 = await getChampionIcon(Ban6Name)
            if (Ban_6) ctx.drawImage(Ban_6, 1080, 360, 50, 50)

            const Ban8Name = AbstractChampion.nameNormalize(matchOne.Ban_8)
            const Ban_8 = await getChampionIcon(Ban8Name)
            if (Ban_8) ctx.drawImage(Ban_8, 1140, 360, 50, 50)

            const Ban2Name = AbstractChampion.nameNormalize(matchOne.Ban_2)
            const Ban_2 = await getChampionIcon(Ban2Name)
            if (Ban_2) ctx.drawImage(Ban_2, 960, 420, 50, 50)

            const Ban4Name = AbstractChampion.nameNormalize(matchOne.Ban_4)
            const Ban_4 = await getChampionIcon(Ban4Name)
            if (Ban_4) ctx.drawImage(Ban_4, 1020, 420, 50, 50)

            const Ban5Name = AbstractChampion.nameNormalize(matchOne.Ban_5)
            const Ban_5 = await getChampionIcon(Ban5Name)
            if (Ban_5) ctx.drawImage(Ban_5, 1080, 420, 50, 50)

            const Ban7Name = AbstractChampion.nameNormalize(matchOne.Ban_7)
            const Ban_7 = await getChampionIcon(Ban7Name)
            if (Ban_7) ctx.drawImage(Ban_7, 1140, 420, 50, 50)
        }

        return { status: true }
    } catch(error) {
        if ('err_msg' in error) throw error // проброс ошибки если есть описание
        throw {
            status: false,
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'Ошибка функции "sm.drawDefault"'
        }
    }
}


async function drawTable({ ctx, match, lang, timezone, width, height, colors }) {
    try {
        const { white, blue, black, orange } = colors

        ctx.fillStyle = blue
        ctx.fillText({ ru: 'Чемпион', en: 'Champion' }[lang], 10, 20)
        ctx.fillText(`${{ ru: 'Ранг', en: 'Rank' }[lang]} / ${{ ru: 'От', en: 'Tp' }[lang]} / ${{ ru: 'Игрок', en: 'Player' }[lang]}`, 140, 20)
        ctx.fillText({ ru: 'Пати', en: 'Party' }[lang], 365, 20)
        ctx.fillText({ ru: 'Кредиты', en: 'Credits' }[lang], 420, 20)
        ctx.fillText({ ru: 'К/Д/А', en: 'K/D/A' }[lang], 505, 20)
        ctx.fillText({ ru: 'Урон', en: 'Damage' }[lang], 585, 20)
        ctx.fillText({ ru: 'Защита', en: 'Defense' }[lang], 670, 20)
        ctx.fillText({ ru: 'Лечение', en: 'Healing' }[lang], 765, 20)
        ctx.fillText({ ru: 'Получено', en: 'Dmg taken' }[lang], 860, 20)
        ctx.fillText({ ru: 'Цель', en: 'Object' }[lang], 960, 20)
        ctx.fillText({ ru: 'Предметы', en: 'Items' }[lang], 1025, 20)
        ctx.fillStyle = white

        const party = {}
        let partyNumber = 1
        const partyColors = ['#00FFFF', '#006400', '#F08080', '#FFFF00', '#FF0000', '#4682B4', '#C71585', '#FF4500', '#7FFF00'].sort(function() {
            return Math.random() - 0.5 // рандомизируем цвета каждый раз
        })

        for (let i = 0; i < match.length; i++) {
            const players = match[i]
            const champName = AbstractChampion.nameNormalize(players.Reference_Name)

            const cnampionIcon = await getChampionIcon(champName)
            let nextTeam = i >= 5 ? 245 : 40
            if (cnampionIcon) ctx.drawImage(cnampionIcon, 10, 55 * i + nextTeam, 50, 50) // рисуем иконки чемпионов


            const legendary = await getCard({
                id: players.ItemId6, 
                name: players.Item_Purch_6, 
                legendary: true, 
                champion: champName
            })
            if (legendary) ctx.drawImage(legendary, 65, 55 * i + nextTeam, 50, 50) // рисуем легендарки

            const imgDivision = await getDivision(players.League_Tier)
            if (players.name == 'Ranked' && imgDivision) ctx.drawImage(imgDivision, 115, 55 * i + nextTeam, 50, 50) // рисуем ранг только в рейте

            // рисуем закуп
            const item1 = await getItem(players.Item_Active_1)
            if (item1) {
                ctx.drawImage(item1, 1025, 55 * i + nextTeam, 40, 40)
                drawLevelItem(ctx, players.ActiveLevel1, 1025, 55 * i + nextTeam + 43, 10, 3)
            }
            const item2 = await getItem(players.Item_Active_2)
            if (item2) {
                ctx.drawImage(item2, 1075, 55 * i + nextTeam, 40, 40)
                drawLevelItem(ctx, players.ActiveLevel2, 1075, 55 * i + nextTeam + 43, 10, 3)
            }
            const item3 = await getItem(players.Item_Active_3)
            if (item3) {
                ctx.drawImage(item3, 1125, 55 * i + nextTeam, 40, 40)
                drawLevelItem(ctx, players.ActiveLevel3, 1125, 55 * i + nextTeam + 43, 10, 3)
            }
            const item4 = await getItem(players.Item_Active_4)
            if (item4) {
                ctx.drawImage(item4, 1175, 55 * i + nextTeam, 40, 40)
                drawLevelItem(ctx, players.ActiveLevel4, 1175, 55 * i + nextTeam + 43, 10, 3)
            }

            const partyId = players.PartyId
            let partyNum = party[partyId]
            if (!partyNum) {
                party[partyId] = partyNum = partyNumber
                partyNumber++
            }

            // ОТ ЛВЛ и ник
            ctx.fillText(players.League_Points || '', 165, 55 * i + nextTeam + 27)
            ctx.fillText(players.playerName, 205, 55 * i + nextTeam + 15)
            ctx.fillStyle = orange
            ctx.fillText(`Lvl: ${players.Account_Level}`, 205, 55 * i + nextTeam + 40)

            nextTeam += 25

            ctx.fillStyle = partyColors[partyNum - 1]
            ctx.beginPath()
            ctx.arc(385, 55 * i + nextTeam - 2, 15, 0, 2*Math.PI, false) // круг пати
            ctx.fill()
            ctx.fillStyle = black
            ctx.fillText(partyNum, 381, 55 * i + nextTeam) // цифра пати
            ctx.fillStyle = white
            ctx.fillText(players.Gold_Earned, 420, 55 * i + nextTeam)
            ctx.fillStyle = orange
            ctx.fillText(`${players.Kills_Player}/${players.Deaths}/${players.Assists}`, 505, 55 * i + nextTeam)
            ctx.fillStyle = white
            ctx.fillText(players.Damage_Player.goDot(), 585, 55 * i + nextTeam)
            ctx.fillText(players.Damage_Mitigated.goDot(), 670, 55 * i + nextTeam)
            ctx.fillText(players.Healing.goDot(), 765, 55 * i + nextTeam)
            ctx.fillText(players.Damage_Taken.goDot(), 860, 55 * i + nextTeam)
            ctx.fillText(players.Objective_Assists, 960, 55 * i + nextTeam)
        }

        return { status: true }
    } catch(error) {
        if ('err_msg' in error) throw error // проброс ошибки если есть описание
        throw {
            status: false,
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'Ошибка функции "sm.drawTable"'
        }
    }
}


function drawLevelItem(ctx, lvl, x, y) { // рисует полоски под закупом (их лвл)
	for (let i = 0; i <= lvl; i++) {
		ctx.fillRect(x + 14 * i, y, 10, 3)
	}
}