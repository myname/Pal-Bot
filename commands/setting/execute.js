/**
 * функция которая выполняет комнаду и отправляет результат пользователю
 */
const path = require('path')
const { ActionRowBuilder, ButtonBuilder, StringSelectMenuBuilder } = require('discord.js')
const { news, defaultSettings, langFlag, fullnameLang } = require(path.join(require.main.path, 'config', 'index.js'))
const { resolveName } = require(path.join(require.main.path, 'utils', 'discord.js'))


/**
 * 
 * @param {String} userId - id написавшего / нажавшего
 * @param {Object} settings 
 * @param {String} setFor - server / user
 * @param {String} setId - serverId / userId
 * @returns {Object}
 */
module.exports = async ({ statsFor, isAdmin, settings, setFor, optLang, optTimezone, typeValue, isPM=false, db }) => {
    try {
        let { lang, id: discordUserId } = settings.user

        const nameOrId = resolveName(statsFor || discordUserId)
        const forUser = !setFor || setFor == 'user'

        // проверка прав и ЛС для серверных настроек
        if (setFor == 'server') {
            if (!isAdmin) {
                const { buttonsLine_1, buttonsLine_3, buttonsLine_4 } = getButtons({ lang, discordUserId, nameOrId, isAdmin, isPM, forUser })

                return {
                    content: `:warning::warning::warning: \`\`\`\n${{
                        ru: 'У вас нет прав администратора для изменения этих настроек.',
                        en: 'You do not have administrator rights to change these settings.'
                    }[lang]}\`\`\``,
                    // components: [ buttonsLine_1, buttonsLine_2, buttonsLine_3, buttonsLine_4 ]
                    components: [ buttonsLine_1, buttonsLine_3, buttonsLine_4 ]
                }
            }

            if (isPM) {
                const { buttonsLine_1, buttonsLine_3, buttonsLine_4 } = getButtons({ lang, discordUserId, nameOrId, isAdmin, isPM, forUser })

                return {
                    content: `:warning::warning::warning: \`\`\`\n${{
                        ru: 'Эта опция предназначения для сервера, она не будет работать в личных сообщениях.',
                        en: 'This option is intended for the server, it will not work in private messages.'
                    }[lang]}\`\`\``,
                    // components: [ buttonsLine_1, buttonsLine_2, buttonsLine_3, buttonsLine_4 ]
                    components: [ buttonsLine_1, buttonsLine_3, buttonsLine_4 ]
                }
            }
        }

        if (typeValue == 'delete') {
            const { buttonsLine_1, buttonsLine_3, buttonsLine_4 } = getButtons({ lang, discordUserId, nameOrId, isAdmin, isPM, forUser })

            if (forUser) {
                delete settings.db.users[ settings.user.id ]
                await db.save()
            } else {
                delete settings.db.guilds[ settings.guild.id ]
                await db.save()
            }
            lang = settings.user.lang

            return {
                content: ':white_check_mark::white_check_mark::white_check_mark:```yaml\n' + {
                    ru: 'Настройки успешно удалены.',
                    en: 'Settings have been successfully deleted.'
                }[lang] +  '```',
                // components: [ buttonsLine_1, buttonsLine_2, buttonsLine_3, buttonsLine_4 ]
                components: [ buttonsLine_1, buttonsLine_3, buttonsLine_4 ]
            }
        }

        const optLangToLang = {
            'Русский [RU]': 'ru',
            ru: 'ru',
            'English [EN]': 'en',
            en: 'en'
        }
        if (optLang) {
            if (setFor == 'user') {
                const userId = settings.user.id
                if ( !settings.db.users[ userId ] ) settings.db.users[ userId ] = { id: userId }
                settings.db.users[ settings.user.id ].lang = optLang == 'auto' ? null : optLangToLang[optLang]
                await db.save()
            } else {
                const guildId = settings.guild.id
                if ( !settings.db.guilds[ guildId ] ) settings.db.guilds[ guildId ] = { id: guildId }
                settings.db.guilds[ guildId ].lang = optLang == 'auto' ? null : optLangToLang[optLang]
                await db.save()
            }
        }
        lang = settings.user.lang // типо обновляет язык сразу же в ответе

        if (optTimezone) {
            if (setFor == 'user') {
                const userId = settings.user.id
                if ( !settings.db.users[ userId ] ) settings.db.users[ userId ] = { id: userId }
                settings.db.users[ settings.user.id ].timezone = optTimezone
                await db.save()
            } else {
                const guildId = settings.guild.id
                if ( !settings.db.guilds[ guildId ] ) settings.db.guilds[ guildId ] = { id: guildId }
                settings.db.guilds[ guildId ].timezone = optTimezone
                await db.save()
            }
        }

        const statusMess = (optLang || optTimezone) ? `:white_check_mark::white_check_mark::white_check_mark: \`\`\`\n${{
            en: 'Settings have been successfully applied.',
            ru: 'Настройки успешно применены.'
        }[lang]}\`\`\`` : ''

        const { buttonsLine_1, buttonsLine_3, buttonsLine_4 } = getButtons({ lang, discordUserId, nameOrId, isAdmin, isPM, forUser })

        return {
            content: `${news[lang]}${statusMess || {
                en: '```md\n#Gray button - turned off; Green button - turned on.```', 
                ru: '```md\n#Серая кнопка - выключено; Зеленая кнопка - включено.```'
            }[lang]}`,
            // components: [ buttonsLine_1, buttonsLine_2, buttonsLine_3, buttonsLine_4 ]
            components: [ buttonsLine_1, buttonsLine_3, buttonsLine_4 ]
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'set.execute'
        }
    }
}


function getButtons({ lang, discordUserId, nameOrId, isAdmin, isPM, forUser }) {
    const buttonsLine_1 = new ActionRowBuilder()
    .addComponents(
        new ButtonBuilder()
        .setCustomId(`menu_${discordUserId}_${nameOrId}`)
        .setLabel({en: 'Menu', ru: 'Меню'}[lang])
        .setStyle('Danger')
        .setEmoji('<:menu:943824092635758632>')
    )

    if (!forUser) {
        buttonsLine_1
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`setting_${discordUserId}_${nameOrId}_change_user`)
            .setLabel({en: 'Go to User Settings', ru: 'Перейти к настройкам пользователя'}[lang])
            .setStyle('Success')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`setting_${discordUserId}_${nameOrId}_delete_server`)
            .setLabel({en: 'Delete Server Settings', ru: 'Удалить настройки сервера'}[lang])
            .setStyle('Danger')
            .setDisabled(!isAdmin || isPM)
        )
    } else {
        buttonsLine_1
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`setting_${discordUserId}_${nameOrId}_change_server`)
            .setLabel({en: 'Go to Server Settings', ru: 'Перейти к настройкам сервера'}[lang])
            .setStyle('Success')
            .setDisabled(!isAdmin || isPM)
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`setting_${discordUserId}_${nameOrId}_delete_user`)
            .setLabel({en: 'Delete my Settings', ru: 'Удалить мои настройки'}[lang])
            .setStyle('Danger')
        )
    }

    buttonsLine_1
    .addComponents(
        new ButtonBuilder()
        .setCustomId(`me_${discordUserId}_${nameOrId}`)
        .setLabel({en: 'Saved nicknames', ru: 'Сохраненные никнеймы'}[lang])
        .setStyle('Secondary')
    )
    // .addComponents(
    //     new ButtonBuilder()
    //     .setCustomId(`an_${discordUserId}_${nameOrId}`)
    //     .setLabel({en: 'Word Analysis', ru: 'Анализ слов'}[lang])
    //     .setStyle('Secondary')
    // )

    const optionsTimezone = [{
        label: 'auto',
        // description: 'auto',
        value: 'auto',
        emoji: '<:time:943836999771623475>'
    }]

    for (let i = 0; i < 24; i++) {
        const num = i + ''
        optionsTimezone.push({
            label: num,
            // description: num,
            value: num,
            emoji: '<:time:943836999771623475>'
        })
    }

    const buttonsLine_3 = forUser ? 
        new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
                .setCustomId(`setting_${discordUserId}_${nameOrId}_timezone_user`)
                .setPlaceholder({en: 'Choose a time zone for yourself', ru: 'Выбрать временную зону для себя'}[lang])
                .addOptions(optionsTimezone)
        ) : new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
                .setCustomId(`setting_${discordUserId}_${nameOrId}_timezone_server`)
                .setPlaceholder({en: 'Select a time zone for the server', ru: 'Выбрать временную зону для сервера'}[lang])
                .addOptions(optionsTimezone)
                .setDisabled(!isAdmin || isPM)
        )

    const optionsLangs = [{
        label: 'auto',
        // description: 'auto',
        value: 'auto',
        // emoji: '<:time:943836999771623475>'
    }]

    for (const langName in langFlag) {
        optionsLangs.push({
            label: `${fullnameLang[langName]} [${langName.toUpperCase()}]`,
            value: langName,
            emoji: langFlag[langName]
        })
    }

    const buttonsLine_4 = forUser ? 
        new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
                .setCustomId(`setting_${discordUserId}_${nameOrId}_lang_user`)
                .setPlaceholder({en: 'Choose a language for yourself', ru: 'Выбрать язык для себя'}[lang])
                .addOptions(optionsLangs)
        ) : new ActionRowBuilder()
        .addComponents(
            new StringSelectMenuBuilder()
                .setCustomId(`setting_${discordUserId}_${nameOrId}_lang_server`)
                .setPlaceholder({en: 'Select the language for the server', ru: 'Выбрать язык для сервера'}[lang])
                .addOptions(optionsLangs)
                .setDisabled(!isAdmin || isPM)
        )

    return { buttonsLine_1, buttonsLine_3, buttonsLine_4 }
}