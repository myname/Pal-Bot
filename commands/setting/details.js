/**
 * функция возвращающая текст полного описания команды с примерами их работы
 */


module.exports = function(command, lang) {
    const path = require('path')
    const { slogan, youtube: { setting: videoURL }, gif: { setting: gif } } = require(path.join(require.main.path, 'config', 'index.js'))
    const { EMPTY_ICON, BOT_ICON } = process.env

    const embed = {
        title: {
            ru: '`Смотреть пример на ютубе`',
            en: '`Watch an example on YouTube`'
        }[lang],
        url: videoURL,
        image: {
            url: gif
        },
        description: {
            ru: `**${command.info[lang]}.**\`\`\`\n[/setting 'for' 'lang' 'timezone']\`\`\` \`\`\`\n-Параметры:\`\`\``,
            en: `**${command.info[lang]}.**\`\`\`\n[/setting 'for' 'lang' 'timezone']\`\`\` \`\`\`\n-Params:\`\`\``
        }[lang],
        color: '3092790',
        footer: {
            icon_url: BOT_ICON,
            text: slogan[lang]
        },
        author: {
            name: `/${command.name}`,
            icon_url: EMPTY_ICON
        },
        fields: [
            {
                name: 'for',
                value: {
                    ru: `\`\`\`\n[Выберите для кого будут применены настройки].\nНастройки можно применять как для себя, ` + 
                    `так и для сервера.\nДля изменения настроек сервера вам нужно обязательно иметь админ права.\n` + 
                    `['Важно знать что настройки пользователя будут перекрывать настройки сервера']. Эта опция ` + 
                    `позволяет тонко настроить бота.\nНапример, если пользователь выбрал временную зону 3, а на сервере установлена ` + 
                    `временная зона 6, то бот покажет пользователю статистику с временной зоной 3, а всем другим - с временной зоной 6.` + 
                    `\nТаким образом каждый пользователь получает максимально комфортные для себя условия.\`\`\``,
                    en: `\`\`\`\n[Select for whom the settings will be applied].\nThe settings can be applied both for ` + 
                    `yourself and for the server.\nTo change the server settings, you must have admin rights.\n` + 
                    `['It is important to know that the user settings will override the server settings']. This option ` + 
                    `allows you to fine-tune the bot.\nFor example, if the user has selected time zone 3, and time zone 6 is installed ` + 
                    `on the server, the bot will show statistics to the user with time zone 3, and to all others with time zone 6.` + 
                    `\nThus, each user receives the most comfortable conditions for himself.\`\`\``
                }[lang]
            },
            {
                name: 'lang',
                value: {
                    ru: `\`\`\`\n[Изменить язык].\nПозволяет изменить язык бота как лично для себя, так и для всего сервера.\`\`\``,
                    en: `\`\`\`\n[Change the language].\nAllows you to change the language of the bot both for yourself and for the entire server.\`\`\``
                }[lang]
            },
            {
                name: 'timezone',
                value: {
                    ru: `\`\`\`\n[Изменить временную зону].\nНекоторая статистика показывает даты, выбор временной зоны может ` + 
                    `улучшить понимание информации.\`\`\``,
                    en: `\`\`\`\n[Change the timezone].\nSome statistics show dates, the choice of a time zone can improve ` +
                    `the understanding of information.\`\`\``
                }[lang]
            }
        ]
    }

    return {
        embeds: [ embed ],
        ephemeral: true
    }
}