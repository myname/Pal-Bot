/**
 * функция обрабатывающая команду (устаревшие)
 */


module.exports = async ({ settings, command, contentParams, isAdmin, isPM, db }) => {
    try {
        const [ typeValue, setFor, setValue ] = contentParams.split(' ')
        const optLang = typeValue == 'lang' ? setValue : null
        const optTimezone = typeValue == 'timezone' ? setValue : null

        return await command.execute({ statsFor: 'me', isAdmin, settings, setFor, optLang, optTimezone, typeValue, isPM, db })
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'set.command'
        }
    }
}