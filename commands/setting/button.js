/**
 * функция которая выполняет комнаду и отправляет результат пользователю (кнопки)
 */


module.exports = async ({ command, selectMenuValues=[], statsFor, branches, settings, isAdmin, isPM, db }) => {
    try {
        // console.log(selectMenuValues, branches)
        // typeValue - тип данных | setFor - для кого данные ставятся | setValue - значение которое ставится (lang ures en)
        let [ typeValue, setFor, setValue ] = branches
        if (selectMenuValues[0]) setValue = selectMenuValues[0] // если данные из выпадающего списка

        const optLang = typeValue == 'lang' ? setValue : null
        const optTimezone = typeValue == 'timezone' ? setValue : null
        const messageData = await command.execute({ statsFor, isAdmin, settings, setFor, optLang, optTimezone, typeValue, isPM, db })

        return {
            status: 1,
            messageData
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'set.buttonExecute'
        }
    }
}