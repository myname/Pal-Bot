/**
 * функция обрабатывающая слеш-команду
 */


module.exports = async ({ command, settings, options, isAdmin, isPM, db }) => {
    try {
        const optFor = options.getString('for')
        const optLang = options.getString('lang')
        const optTimezone = options.getString('timezone')
        const setFor = optFor == 'user' ? 'user' : 'server'

        const messageData = await command.execute({ statsFor: 'me', isAdmin, settings, setFor, optLang, optTimezone, isPM, db })

        return {
            status: 1,
            messageData
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'pal.slash'
        }
    }
}