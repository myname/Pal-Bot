/**
 * функция обрабатывающая команду (устаревшие)
 */


module.exports = async ({ settings, command, contentParams, db }) => {
    try {
        // const userId = message.author.id
        // const params = contentParams.split(' ')
        // const [ nameOrId, modifierParam ] = params
        // const modifierList = [ '-pc', '-steam', '-console' ] // список доступных модификаторов
        // const modifier = modifierList.find(mod => mod == modifierParam) || '-pc'

        const statsFor = contentParams || 'me'

        return await command.execute({ settings, command, statsFor, db })
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'ss.command'
        }
    }
}