/**
 * функция которая выполняет комнаду и отправляет результат пользователю (кнопки)
 */


module.exports = async ({ command, selectMenuValues=[], statsFor, branches, settings, db }) => {
    try {
        // console.log(selectMenuValues, branches)
        const statsForNew = selectMenuValues[0]
        if (statsForNew) statsFor = statsForNew.slice(1)
        // const prefConsole = params?.ss?.console || false
        const consoleStats = new Set(branches).has('console')
        // const changeConsole = prefConsole !== consoleStats // есть ли изменения в консоле

        // if (changeConsole) { // если есть изменения
        //     // тут нужно изменить кнопки и обновить данные в БД
        //     // а потом все равно запросить новую стату!

        //     // await settings.setParams('ss', 'console', consoleStats, userId) // обновляем данные локально и в БД
        //     // if (!settings.params.ss) settings.params.ss = {}
        //     // settings.params.ss.console = consoleStats // меняем данные в текущей переменной
        // }

        const messageData = await command.execute({ command, statsFor, settings, consoleStats, db })

        return {
            status: 1,
            messageData,
            editReply: !!selectMenuValues.length,
            newReply: !!selectMenuValues.length
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'ss.buttonExecute'
        }
    }
}