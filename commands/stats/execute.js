/**
 * функция которая выполняет комнаду и отправляет результат пользователю
 */
const path = require('path')
const { news } = require(path.join(require.main.path, 'config', 'index.js'))
const { ActionRowBuilder, ButtonBuilder, StringSelectMenuBuilder } = require('discord.js')
const { resolveName } = require(path.join(require.main.path, 'utils', 'discord.js'))


module.exports = async ({ settings, command, statsFor, consoleStats=false, platform, db }) => {
    try {
        const { id: discordUserId, lang, timezone } = settings.user
        const nameOrId = platform == 'steam' ? statsFor : resolveName(statsFor || discordUserId)

        if (platform == 'steam') {
            if (!/^\d+$/.test(nameOrId)) throw {
                err_msg: {
                    ru: 'Стим аккаунт должен быть ID',
                    en: 'Steam need by ID'
                }
            }
        }

        const stats = await command.getStats(discordUserId, nameOrId, platform, db)
        if (!stats.status) throw stats

        const draw = await command.draw({ stats, lang, timezone, consoleStats }) // рисуем
        if (!draw.status) throw draw

        const canvas = draw.canvas
        const buffer = canvas.toBuffer('image/png') // buffer image

        const showOldStatsText = {
            ru: '```fix\nАккаунт скрыт или API временно не работает.\n=\nВам будут показаны данные последнего удачного запроса.```',
            en: '```fix\nThe account is hidden or the API is temporarily not working.\n=\nYou will be shown the details of the last Successful request.```'
        }

        const replayOldText = stats.getplayer.old ?
                `${showOldStatsText[lang]}\n` :
            stats.getchampionranks.old ?
                `${showOldStatsText[lang]}\n` : ''

        const playerInfo = `\`\`\`\n[${draw.name}](${draw.id})\`\`\``

        // const consoleStats = params?.ss?.console || false
        const buttonsLine_1 = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`menu_${discordUserId}_${draw.id}`)
            .setLabel({en: 'Menu', ru: 'Меню'}[lang])
            .setStyle('Danger')
            .setEmoji('<:menu:943824092635758632>')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`stats_${discordUserId}_${draw.id}${consoleStats ? '_console' : ''}`)
            .setLabel({en: 'Refresh', ru: 'Обновить'}[lang])
            .setStyle('Success')
            .setEmoji('<:refresh_mix:943814451226873886>')
        )
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`stats_${discordUserId}_${draw.id}${consoleStats ? '' : '_console'}`)
            .setLabel((consoleStats ? {en: 'Show PC stats', ru: 'Показать статистику PC'} : 
                {en: 'Show console stats', ru: 'Показать статистику консоли'})[lang])
            // .setStyle(consoleStats ? 'Success' : 'Primary')
            .setStyle('Secondary')
            .setEmoji(consoleStats ? '<:mouse:943825037536952350>' : '<:console:943825037113303071>')
        )

        return {
            content: `${news[lang]}${replayOldText}${playerInfo}`,
            components: [ buttonsLine_1 ],
            files: [{
                attachment: buffer,
                name: `${nameOrId}.png`
            }]
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'omething went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'ss.execute'
        }
    }
}