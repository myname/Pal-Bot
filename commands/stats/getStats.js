/**
 * функция которая получает данные статистик и обрабатывает возможные ошибки связанные с этим
 */
const path = require('path')
const { getUserSavedData } = require(path.join(require.main.path, 'utils', 'index.js'))


module.exports = async (userId, nameOrId, platform, db) => {
    try {
        const { api: hirez } = db

        let savePlayerId
        if (platform == 'steam') {
            // сделать проверку, id это или нет, если не id, то получить id

            // получаем данные со стима
            const playerData = await hirez.getplayeridbyportaluserid(nameOrId, '5')
            if (!playerData.status) throw playerData
            const data = playerData?.data || []
            savePlayerId = data[0]?.player_id
        }

        const playerIdOrName = savePlayerId ? null : getUserSavedData(nameOrId, userId, false, db)
        console.log('\n\n\n', playerIdOrName, '\n')

        const fetchGetplayer = await hirez.getplayer(playerIdOrName || savePlayerId)
        if (!fetchGetplayer.status) throw fetchGetplayer

        const getplayer = fetchGetplayer.data[0]
        const playerId = getplayer?.Id
        if (!getplayer || !playerId) throw {
            status: false,
            err_msg: {
                ru: 'Аккаунт скрыт.',
                en: 'The account is hidden.'
            }
        }

        const fetchGetchampionranks = await hirez.getchampionranks(playerId)
        if (!fetchGetchampionranks.status) throw fetchGetchampionranks

        const getchampionranks = fetchGetchampionranks.data // если данных нет то ничего страшного

        return {
            getplayer: {
                lastUpdate: fetchGetplayer.lastUpdate,
                data: getplayer,
                old: fetchGetplayer.old
            },
            getchampionranks: {
                lastUpdate: fetchGetchampionranks.lastUpdate,
                data: getchampionranks,
                old: getchampionranks.old
            },
            status: true
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            error,
            err_msg: {
                ru: 'Что-то пошло не так... Попробуйте снова или сообщите об этой ошибке создателю бота.',
                en: 'Something went wrong... Try again or report this error to the bot creator.'
            },
            log_msg: 'ss.getStats'
        }
    }
}