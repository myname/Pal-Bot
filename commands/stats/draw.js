/**
 * Скрипт который только рисует указанную инфу в canvas и возвращает ее или ошибку
 */
const path = require('path')
const reqMainPath = require.main.path
const { createCanvas } = require('canvas')
const { imageNormalize, drawRoundedImage } = require(path.join(reqMainPath, 'utils', 'canvas.js'))
const { getChampionsWallpapers, getRandomBackground, getIconBot, 
    getAvatar, getChampionIcon, getDivision } = require(path.join(reqMainPath, 'utils', 'index.js'))
const ChampionsStats = require(path.join(reqMainPath, 'classes', 'ChampionsStats.js'))
const { colors, ranks } = require(path.join(reqMainPath, 'config', 'index.js'))


module.exports = async function({ stats, lang, timezone, consoleStats }) {
    try {
        const { red, white, black, transparent, green, orange, blue, darkBlue, lightGreen, greyYellow } = colors

        const { getplayer, getchampionranks } = stats
        const { data: player } = getplayer
        const { data: championsData } = getchampionranks
        const champions = new ChampionsStats(championsData)
        const { timePlayRole } = champions

        const width = 800
        const height = 450
        const canvas = createCanvas(width, height)
        const ctx = canvas.getContext('2d')

        const champSort = !champions.error ? champions.sort().slice(0, 5) : null
        const randomChampion = !champions.error ? champSort.random() : {}
        // console.log(randomChampion.name)
        const background = await getChampionsWallpapers(randomChampion.name)
        if (background) {
            const newCanvas = imageNormalize({ img: background, width, height })
            if (newCanvas) ctx.drawImage(newCanvas, 0, 0, width, height) // рисуем
        } else {
            const background = await getRandomBackground()
            const newCanvas = imageNormalize({ img: background, width, height })
            if (newCanvas) ctx.drawImage(newCanvas, 0, 0, width, height) // рисуем
        }

        // рисуем иконку бота
        const icon = await getIconBot()
        if (icon) ctx.drawImage(icon, 1, 5, 60, 60)

        // фон для текста сверху (бота)
        ctx.fillStyle = transparent
        ctx.beginPath()
        ctx.rect(75, 0, 640, 34)
        ctx.fill()

        // текст сверху (бота)
        ctx.fillStyle = white
        ctx.font = 'bold 18px Ledger'
        ctx.textAlign = 'start'
        ctx.fillText({
            ru: 'Pal-Bot - Лучшая статистика по Paladins в наилучшем виде',
            en: 'Pal-Bot - The best statistics on Paladins in the best possible way'
        }[lang], 85, 22)

        // фон для часового пояса
        ctx.fillStyle = transparent
        ctx.beginPath()
        ctx.rect(632, 421, 168, 29)
        ctx.fill()

        // треугольник для часового пояса 
        ctx.beginPath()
        ctx.moveTo(622, 450)
        ctx.lineTo(632, 450)
        ctx.lineTo(632, 421)
        ctx.closePath()
        ctx.fill()

        // чассовой пояс
        ctx.fillStyle = white
        ctx.font = 'bold 16px Ledger'
        ctx.fillText(`${{en: 'Time zone', ru: 'Часовой пояс'}[lang]}: 3`, 640, 442)

        // треугольник для инфы сверху
        ctx.fillStyle = transparent
        ctx.beginPath()
        ctx.moveTo(65, 0)
        ctx.lineTo(75, 0)
        ctx.lineTo(75, 34)
        ctx.closePath()
        ctx.fill()

        // треугольник для инфы сверху
        ctx.beginPath()
        ctx.moveTo(715, 0)
        ctx.lineTo(725, 0)
        ctx.lineTo(715, 34)
        ctx.closePath()
        ctx.fill()

        // фон для круговой статы ака (лвл и опыт)
        ctx.beginPath()
        ctx.rect(46, 150, 71, 48)
        ctx.fill()

        // круг опыта
        ctx.fillStyle = darkBlue
        ctx.beginPath()
        ctx.moveTo(81, 115)
        ctx.arc(81, 115, 60, getRadians(126), 7.208209810736581)
        ctx.closePath()
        ctx.fill()

        const playerExp = !champions.error ? champions.parseExp(player.Total_XP) : null
        if (!champions.error) {
            const radiusExp = 126 + 287 * playerExp.progress / 100 // отсчет от 126 идет
            // console.log(`${playerExp.progress}%`, radiusExp)
            // круг опыта - заполнение
            ctx.fillStyle = lightGreen
            ctx.beginPath()
            ctx.moveTo(81, 115)
            ctx.arc(81, 115, 60, getRadians(126), getRadians(radiusExp))
            ctx.closePath()
            ctx.fill()
        }

        // аватарка
        const avatar = await getAvatar(player.AvatarId)
        drawRoundedImage.bind(ctx)(avatar, 50, 82-50, 116-50, 96, 96)

        if (!champions.error) {
            // процент опыта
            ctx.fillStyle = white
            ctx.font = 'bold 12px Ledger'
            ctx.fillText(`${(+playerExp.progress).toFixed(0)}%`, 87, 173)

            // лвл ака
            ctx.textAlign = 'center'
            ctx.font = 'bold 20px Ledger'
            ctx.fillText(playerExp.lvl, 81, 193)
        } else {
            // процент опыта
            ctx.fillStyle = white
            ctx.font = 'bold 12px Ledger'
            ctx.fillText(`0%`, 87, 173)

            // лвл ака
            ctx.textAlign = 'center'
            ctx.font = 'bold 20px Ledger'
            ctx.fillText(player.Level, 81, 193)
        }

        // лвл
        // ctx.font = 'bold 10px Ledger'
        // ctx.fillText('LVL', 87, 192)

        // градиент для фона инфы об аке
        const gradient6 = ctx.createLinearGradient(154, 55, 154, 188)
        gradient6.addColorStop(0, black)
        gradient6.addColorStop(1, red)
        ctx.fillStyle = gradient6
        ctx.beginPath()
        ctx.rect(154, 55, 5, 133)
        ctx.fill()

        // фон для основной инфы об аке
        ctx.textAlign = 'start'
        ctx.fillStyle = transparent
        ctx.beginPath()
        ctx.rect(159, 55, 310, 133)
        ctx.fill()

        // текст основной инфы ака
        ctx.fillStyle = green
        ctx.font = 'bold 17px Ledger'
        ctx.fillText(`${player.hz_player_name || player.hz_gamer_tag} (${player.Region})`, 166, 78)

        ctx.fillStyle = white
        const dateCreate = new Date(player.Created_Datetime).addHours(timezone).toText()
        ctx.fillText(`${{ ru: 'Создан', en: 'Created' }[lang]}: ${dateCreate}`, 166, 98)
        ctx.fillText(`${{ ru: 'Сыграно', en: 'Played' }[lang]}: ${player.HoursPlayed.goDot()} ${{ ru: 'Часов', en: 'Hours' }[lang]}`, 166, 118)
        const dateLastLogin = new Date(player.Last_Login_Datetime).addHours(timezone).toText()
        ctx.fillText(`${{ ru: 'Онлайн', en: 'Last login' }[lang]}: ${dateLastLogin}`, 166, 138)

        if (!champions.error) {
            ctx.fillStyle = orange
            ctx.fillText(`K/D/A: ${champions.kda}`, 166, 158)
        }

        ctx.fillStyle = white
        ctx.fillText(`${{ ru: 'Клиент', en: 'Client' }[lang]}: ${player.Platform} - ${player.Name}`, 166, 178)

        if (!champions.error) {
            // любимые чемпионы - градиент
            const gradient4 = ctx.createLinearGradient(30, 223, 30, 263)
            gradient4.addColorStop(0, black)
            gradient4.addColorStop(1, red)
            ctx.fillStyle = gradient4
            ctx.beginPath()
            ctx.rect(10, 223, 5, 40)
            ctx.fill()

            // любимые чемпионы - фон
            ctx.fillStyle = transparent
            ctx.beginPath()
            ctx.rect(15, 223, 270, 40)
            ctx.fill()

            // любимые чемпионы
            ctx.fillStyle = red
            ctx.font = 'bold 19px Ledger'
            ctx.fillText(`${{ ru: 'ЛЮБИМЫЕ ЧЕМПИОНЫ', en: 'FAVORITE CHAMPIONS' }[lang]}`, 20, 250)

            // фон статы любимых чемпионов
            ctx.fillStyle = transparent
            ctx.beginPath()
            ctx.rect(15, 335, 270, 72)
            ctx.fill()

            // градиент для статы любимых персов
            const gradient3 = ctx.createLinearGradient(15, 335, 15, 407)
            gradient3.addColorStop(0, black)
            gradient3.addColorStop(1, red)
            ctx.fillStyle = gradient3
            ctx.beginPath()
            ctx.rect(10, 335, 5, 72)
            ctx.fill()

            // рисуем загруженых чемпионов
            ctx.font = 'bold 17px Ledger'
            ctx.textAlign = 'center'
            const positionImgX = 15
            const positionTextX = 40

            for (let i = 0; i < champSort.length; i++) {
                const champ = champSort[i]
                if (!champ) continue;
                // console.log(champ)

                const img = await getChampionIcon(champ.name)
                const xImg = positionImgX + 55 * i
                if (img) ctx.drawImage(img, xImg, 270, 50, 50)

                // опыт чемпиона
                ctx.fillStyle = darkBlue
                ctx.beginPath()
                ctx.rect(xImg, 320, 50, 7)
                ctx.fill()

                const widthExp = 50 * champ.exp.progress / 100
                // console.log(widthExp)
                ctx.fillStyle = lightGreen
                ctx.beginPath()
                ctx.rect(xImg, 320, widthExp, 7)
                ctx.fill()

                // стата чемпионов
                const xText = positionTextX + 55 * i
                ctx.fillStyle = green
                ctx.fillText(champ.exp.lvl, xText, 357)
        
                ctx.fillStyle = orange
                ctx.fillText(champ.kda, xText, 379)
        
                ctx.fillStyle = blue
                ctx.fillText(`${(+champ.winrate).toFixed(0)}`, xText, 400)            
            }
        }

        // градиент для общей статы
        const gradient2 = ctx.createLinearGradient(300, 250, 300, 408)
        gradient2.addColorStop(0, black)
        gradient2.addColorStop(1, red)
        ctx.fillStyle = gradient2
        ctx.beginPath()
        ctx.rect(300, 250, 5, 158)
        ctx.fill()

        // прозрачный фон для общей статы
        ctx.fillStyle = transparent
        ctx.beginPath()
        ctx.rect(305, 250, 385, 158)
        ctx.fill()

        const ranked = (consoleStats ? player.RankedController : player.RankedKBM) || {}
        const tier = consoleStats ? player.Tier_RankedController : player.Tier_RankedKBM
        const rankNum = tier == 26 && ranked.Rank <= 100 && ranked.Rank > 0 ? 27 : tier
        const division = await getDivision(rankNum)
        if (division) ctx.drawImage(division, 688, 284, 120, 120)

        ctx.fillStyle = blue
        ctx.textAlign = 'start'
        ctx.fillText(`${{ ru: 'Винрейт', en: 'Winrate' }[lang]}: ${champions.winrate || 0}%`, 310, 397)

        ctx.fillStyle = white
        ctx.fillText(`${{ ru: 'Убийств', en: 'Kills' }[lang]}: ${(champions.kills || 0).goDot()}`, 310, 297)
        ctx.fillText(`${{ ru: 'Смертей', en: 'Deaths' }[lang]}: ${(champions.deaths || 0).goDot()}`, 310, 317)
        ctx.fillText(`${{ ru: 'Помощи', en: 'Assists' }[lang]}: ${(champions.assists || 0).goDot()}`, 310, 337)
        ctx.fillText(`${{ ru: 'Побед', en: 'Wins' }[lang]}: ${(player.Wins || champions.wins || 0).goDot()}`, 310, 357)
        ctx.fillText(`${{ ru: 'Поражений', en: 'Losses' }[lang]}: ${(player.Losses || champions.losses || 0).goDot()}`, 310, 377)

        ctx.fillStyle = red
        ctx.fillText(`${{ ru: 'ВСЕГО', en: 'TOTAL' }[lang]}`, 340, 272)
        ctx.fillText(`${{ ru: 'РАНКЕД', en: 'RANKED' }[lang]}`, 525, 272)

        ctx.fillStyle = white
        ctx.fillText(`${{ ru: 'Побед', en: 'Wins' }[lang]}: ${(ranked.Wins || 0).goDot()}`, 502, 297)
        ctx.fillText(`${{ ru: 'Поражений', en: 'Losses' }[lang]}: ${(ranked.Losses || 0).goDot()}`, 502, 317)

        ctx.fillStyle = blue
        const winrateNumRanked = ChampionsStats.getWinrate(ranked.Wins, ranked.Losses)
        ctx.fillText(`${{ ru: 'Винрейт', en: 'Winrate' }[lang]}: ${winrateNumRanked}%`, 502, 337)

        ctx.fillStyle = white
        ctx.fillText(`${{ ru: 'Ранг', en: 'Rank' }[lang]}: ${ranks[lang][rankNum]}`, 502, 357)
        ctx.fillText(`${{ ru: 'ОТ', en: 'TP' }[lang]}: ${ranked.Points.goDot()}`, 502, 377)
        if (ranked.Rank) ctx.fillText(`${{ ru: 'Позиция', en: 'Position' }[lang]}: ${ ranked.Rank }`, 502, 397)

        if (timePlayRole) {
            // обводка для диаграмы ролей
            ctx.fillStyle = black
            ctx.beginPath()
            ctx.moveTo(530, 115)
            ctx.arc(530, 115, 52, 0, 6.283185307179586)
            ctx.closePath()
            ctx.fill()

            // данные для диаграммы
            const totalTime = timePlayRole.damage + timePlayRole.flanker + timePlayRole.frontline + timePlayRole.support
            const damageDeg = 360 * (timePlayRole.damage / totalTime)
            const flankerDeg = 360 * (timePlayRole.flanker / totalTime)
            const frontlineDeg = 360 * (timePlayRole.frontline / totalTime)

            // рисуем диаграмму ->
            const second = frontlineDeg + damageDeg
            const third = flankerDeg + damageDeg + frontlineDeg
            if (0 < damageDeg) drawPieSlice(ctx, 530, 115, 50, 0, damageDeg, greyYellow)
            if (damageDeg < second) drawPieSlice(ctx, 530, 115, 50, damageDeg, frontlineDeg + damageDeg, blue)
            if (second < third) drawPieSlice(ctx, 530, 115, 50, frontlineDeg + damageDeg, flankerDeg + damageDeg + frontlineDeg, orange)
            if (third < 360) drawPieSlice(ctx, 530, 115, 50, flankerDeg + damageDeg + frontlineDeg, 360, green)

            // градиент для ролей
            const gradient1 = ctx.createLinearGradient(787, 55, 787, 175)
            gradient1.addColorStop(0, black)
            gradient1.addColorStop(1, red)
            ctx.fillStyle = gradient1
            ctx.beginPath()
            ctx.rect(787, 55, 5, 120)
            ctx.fill()

            // прозрачный фон для ролей
            ctx.fillStyle = transparent
            ctx.beginPath()
            ctx.rect(587, 55, 200, 120)
            ctx.fill()

            ctx.fillStyle = red
            ctx.font = 'bold 19px Ledger'
            ctx.fillText(`${{ ru: 'Роли', en: 'Roles' }[lang]}`, 635, 77)

            ctx.fillStyle = white
            ctx.font = 'bold 16px Ledger'
            ctx.fillText(`${{ ru: 'Урон', en: 'Damage' }[lang]} - ${(timePlayRole.damage / totalTime * 100).toFixed(2)}%`, 615, 98)
            ctx.fillText(`${{ ru: 'Танк', en: 'Front Line' }[lang]} - ${(timePlayRole.frontline / totalTime * 100).toFixed(2)}%`, 615, 119)
            ctx.fillText(`${{ ru: 'Фланг', en: 'Flank' }[lang]} - ${(timePlayRole.flanker / totalTime * 100).toFixed(2)}%`, 615, 140)
            ctx.fillText(`${{ ru: 'Саппорт', en: 'Support' }[lang]} - ${(timePlayRole.support / totalTime * 100).toFixed(2)}%`, 615, 161)

            ctx.fillStyle = greyYellow
            ctx.beginPath()
            ctx.rect(593, 84, 17, 17)
            ctx.fill()

            ctx.fillStyle = blue
            ctx.beginPath()
            ctx.rect(593, 105, 17, 17)
            ctx.fill()

            ctx.fillStyle = orange
            ctx.beginPath()
            ctx.rect(593, 126, 17, 17)
            ctx.fill()

            ctx.fillStyle = green
            ctx.beginPath()
            ctx.rect(593, 147, 17, 17)
            ctx.fill()
        }

        return {
            status: true,
            canvas,
            id: player.Id, // ActivePlayerId
            name: player.hz_player_name || player.hz_gamer_tag
        }
    } catch(error) {
        if ('err_msg' in error) throw error
        throw {
            status: false,
            error,
            err_msg: {
                ru: 'Ошибка во время рисования.',
                en: 'Error during drawing.'
            },
            log_msg: 'Ошибка функции "ss.draw"'
        }
    }
}


function drawPieSlice(ctx, centerX, centerY, radius, startAngle, endAngle, color) {
	ctx.fillStyle = color
	ctx.beginPath()
	ctx.moveTo(centerX, centerY)
	ctx.arc(centerX, centerY, radius, getRadians(startAngle), getRadians(endAngle))
	ctx.closePath()
	ctx.fill()
}


function getRadians(degrees) {return (Math.PI / 180) * degrees}