/**
 * функция возвращающая текст полного описания команды с примерами их работы
 */
const path = require('path')
const { slogan, youtube: { stats: videoURL }, gif: { stats: gif } } = require(path.join(require.main.path, 'config', 'index.js'))
const { EMPTY_ICON, BOT_ICON } = process.env


module.exports = function(command, lang) {
    const embed = {
        title: {
            ru: '`Смотреть пример на ютубе`',
            en: '`Watch an example on YouTube`'
        }[lang],
        url: videoURL,
        image: {
            url: gif
        },
        description: {
            ru: `**${command.info[lang]}.**\`\`\`\n[/stats 'nickname' 'platform']\`\`\` \`\`\`\n-Параметры:\`\`\``,
            en: `**${command.info[lang]}.**\`\`\`\n[/stats 'nickname' 'platform']\`\`\` \`\`\`\n-Params:\`\`\``
        }[lang],
        color: '3092790',
        footer: {
            icon_url: BOT_ICON,
            text: slogan[lang]
        },
        author: {
            name: `/${command.name}`,
            icon_url: EMPTY_ICON
        },
        fields: [
            {
                name: 'nickname:',
                value: {
                    ru: `\`\`\`\n[Ник или id игрока, статистику которого вы хотите увидеть].\nВведите сюда никнейм ` + 
                        `игрока, чью статистику хотите посмотреть.\nЕсли же вы выбрали ['platform':'Steam'], то введите ID стима.\`\`\``,
                    en: `\`\`\`\n[Nickname or id of the player whose stats you want to see].\nEnter here the nickname ` + 
                        `of the player whose statistics you want to see.\nIf you chose ['platform':'Steam'], then enter the steam ID.\`\`\``
                }[lang]
            },
            {
                name: 'platform:',
                value: {
                    ru: `\`\`\`\n[Платформа аккаунта для поиска (не обязательно)].\n` + 
                        `Платформа позволяет выбрать тип запроса статистики. Всего доступно 3 способа запроса статистики.\n` + 
                        `1. #Steam - этот метод делает поиск аккаунта через ID стима.\n` + 
                        `2. #Console - этот метод позволит вам использовать разные спецсимволы (не будет на них ругаться), а так ` + 
                        `же в приоритете поиска будут консольные аккаунты. Стоит отметить что для показа статистики консольного ранкеда вам ` + 
                        `потребуется активировать кнопку ['Показать статистику консоли'].\n3. #PC - это стандартный метод запроса, ` + 
                        `если вы хотите воспользоваться им, то можно просто не указывать никакую платформу.\`\`\``,
                    en: `\`\`\`\n[Search account platform (optional)].\n` + 
                        `The platform allows you to select the type of statistics request. There are 3 ways to request statistics in total.\n` + 
                        `1. #Steam - this method makes the account search through the steam ID.\n` + 
                        `2. #Console - this method will allow you to use different special characters (will not swear at them), and console ` + 
                        `accounts will also be in the priority of the search. It is worth noting that to show the statistics of the console ` + 
                        `ranking you will need to activate the button ['Show console statistics'].\n3. #PC - this is the standard request ` + 
                        `method, if you want to use it, then you can simply not specify any platform.\`\`\``
                }[lang]
            }
        ]
    }

    return {
        embeds: [ embed ],
        ephemeral: true
    }
}