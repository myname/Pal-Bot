/**
 * функция возвращающая обьект слеш-команды
 */


module.exports = () => {
    return {
        name: 'stats',
        description: 'Displays general account statistics',
        options: [
            {
                name: 'nickname',
                description: 'Nickname or id of the player whose stats you want to see',
                type: 3
            },
            {
                name: 'platform',
                description: 'Search account platform (optional)',
                type: 3,
                choices: [
                    {
                        name: 'Steam (only steam id)',
                        value: 'steam'
                    },
                    {
                        name: 'Console (show console stats)',
                        value: 'console'
                    },
                    {
                        name: 'PC (default)',
                        value: 'pc'
                    }
                ]
            }
        ]
    }
}