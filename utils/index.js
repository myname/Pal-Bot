/**
 * 
 */
const path = require('path')
const fs = require('fs')
const reqMainPath = require.main.path
// const { usersNicknames } = require(path.join(reqMainPath, 'mongo', 'schemes.js'))
// const { getitems } = require(path.join(reqMainPath, 'mongo', 'schemes.js'))
const { youtube: { me: meVideo }, gif: { me: gif }, maps, langToNum } = require(path.join(reqMainPath, 'config', 'index.js'))
const { loadImage } = require('canvas')
const Card = require(path.join(reqMainPath, 'classes', 'Card.js'))


module.exports.getUserSavedData = function(nameOrId, userId, needType=false, db) { // используется в getStats у champions команды
    if (!nameOrId) return false

    let typeData = null

    nameOrId = nameOrId += ''
    if (/^me((\d){1})?$/.test(nameOrId)) {
        typeData =  'me'
    } else if (!isFinite(nameOrId)) {
        typeData = 'name'
    } else if (nameOrId.length == 18) {
        typeData = 'discord'
    } else if (nameOrId.length < 10) {
        typeData = 'player'
    } else if (nameOrId.length > 9) {
        typeData = 'match'
    }

    if (!typeData) return false
    let userSavedData = null // сохраненыне данные из БД

    if (!typeData || typeData == 'discord') { // получаем сохраненные данные с БД
        const dataUser = db?.users?.[ nameOrId || userId ]
        userSavedData = dataUser
    }

    const err_msg = {
        ru: `У пользователя нет сохраненного никнейма.\n=\nИспользуйте команду "/me ТвойНик" что бы сохранить свой никнейм.\n` + 
        `Используйте команду "/help" чтобы получить более детальную помощь по командам.`,
        en: `The user does not have a saved nickname.\n=\nUse the command "/me YourNickname" to save your nickname.\n` + 
        `Use the "/help" command to get more detailed help on commands.`
    }

    const embed = {
        description: `[YOUTUBE VIDEO](${meVideo})`,
        color: '3092790',
        image: {
            url: gif
        }
    }

    if (typeData == 'me') {
        const userData = db?.users?.[ userId ]

        if (!userData) throw {
            status: false,
            err_msg,
            embed
        }

        userSavedData = nameOrId == 'me' ? userData: (userData.slot?.[nameOrId] || userData.slot?.[nameOrId])

        if (!userSavedData) throw {
            status: false,
            err_msg,
            embed
        }
    }

    const playerId = (userSavedData?.saveId || userSavedData?.id) || (typeData == 'player' ? nameOrId : null)
    const playerName = (userSavedData?.saveName || userSavedData?.name) || (typeData == 'name' ? nameOrId : null)
    const playerIdOrName = playerId || playerName
    if (!playerIdOrName) throw {
        status: false,
        err_msg,
        embed
    }

    if (needType) return { data: playerIdOrName, type: typeData, playerId, playerName }
    return playerIdOrName
}


module.exports.getMap = async function(mapGame) { // возможно ее лучше вынести в utils
    // [
    //     'Ranked Jaguar Falls', 'Ranked Timber Mill', 'Ranked Frog Isle', 'Ranked Ascension Peak', 'Ranked Frog Isle', 'Ranked Ice Mines', 
    //     'LIVE Frog Isle', 'LIVE Jaguar Falls', 'LIVE Timber Mill', 'LIVE Serpent Beach', 'LIVE Stone Keep (Day)', 'LIVE Splitstone Quarry', 
    //     'LIVE Primal Court (Onslaught)', `LIVE Foreman's Rise (Onslaught)`, `LIVE Magistrate's Archives (KOTH)`, `LIVE Marauder's Port (KOTH) `, 
    //     'LIVE Snowfall Junction (KOTH) ', 'WIP Jaguar Falls Beyond', 
    // ]
    try {
        // console.log(mapGame)

        const mapName = maps.find(map => {
            const reg = new RegExp(`${map}`, 'i')
            return mapGame.replace(/[\(\)\']/ig, '').replace(/v\w/ig, '').replace(/[ ]+/ig, ' ').match(reg)
        }) || 'test maps'

        const pathToImg = path.join(reqMainPath, 'img', 'maps', `${mapName}.jpg`)
        const image = await loadImage(pathToImg).catch(() => console.log(`Ошибка получения КАРТЫ ${ pathToImg }`))

        return { img: image, name: mapName.wordToUp() }
    } catch(error) {
        if ('err_msg' in error) throw error // проброс ошибки если есть описание
        throw {
            status: false,
            error,
            err_msg: {
                ru: 'Ошибка загрузки превью карты.',
                en: 'Error loading map preview.'
            },
            log_msg: 'Ошибка функции "getMap"'
        }
    }
}


module.exports.getCard = async function({ champion, id, name, legendary=false }) {
    try {
        const cardType = legendary ? 'legendary' : 'common'
        const cardFormat = legendary ? 'png' : 'jpg'

        name = name.toLowerCase().replace(/[ ]+/g, '-').replace(/[']+/g, '')

        const pathOfId = path.join(reqMainPath, 'champions', champion, 'cards', cardType, `${id}.${cardFormat}`)
        const loadOfId = await loadImage(pathOfId).catch(() => console.log(`Ошибка получения карты ${ pathOfId }`))

        if (loadOfId) return loadOfId

        const pathOfName = path.join(reqMainPath, 'champions', champion, 'cards', cardType, `${name}.${cardFormat}`)
        const loadOfName = await loadImage(pathOfName).catch(() => console.log(`Ошибка получения карты ${ pathOfId }`))

        return loadOfName
    } catch(error) {
        // console.log(error)
        return false
    }
}


module.exports.getCards = async function({ champion, LoadoutItems, lang='en', db }) {
    try {
        // ids = new Set(ids)
        // names = new Set(names)
        // console.log(`names: ${names.length}`, names)

        const findNeedLang = db.items[ lang ]//await getitems.find()//({ lang: langToNum[lang] })
        // const findNeedLang = find.filter(el => el.lang == langToNum[lang])
        // const findEn = (find.filter(el => el.lang == 1))[0].data.map(card => new Card(card))
        // const filter = findNeedLang[0].data.filter(card => ids.has(card.ItemId))
        // console.log(filter.length, filter)
        // const cards = filter.map(card => new Card(card))

        const newCards = []
        // перебираем имена карт которые нам нужны, получаем все необходимые данные с items и с fs (картинки)
        for (let i = 0; i < LoadoutItems.length; i++) {
            const cardName = LoadoutItems[i].ItemName
            const cardId = LoadoutItems[i].ItemId
            // const findCardEN = findEn.find(c => c.DeviceName == cardName) // данные картинки (англ)
            // const findCard = cards.find(c => c.name == cardName) // данные картинки (установленный язык)
            // console.log(`\ncardName:`, findCard)
            // const cardNameEn = cardName.toLowerCase().replace(/[ ]+/g, '-').replace(/[']+/g, '')

            const findData = findNeedLang.find(data => data.ItemId == cardId)
            const card = new Card({ ...findData })
            card.id = cardId
            card.nameEn = cardName


            const pathOfId = path.join(reqMainPath, 'champions', champion, 'cards', 'common', `${card.id}.jpg`)
            const loadOfId = await loadImage(pathOfId).catch(console.log)
            if (loadOfId) {
                card.img = loadOfId
            } else {
                const pathOfName = path.join(reqMainPath, 'champions', champion, 'cards', 'common', `${cardName}.jpg`)
                const loadOfName = await loadImage(pathOfName).catch(console.log)
                card.img = loadOfName
            }

            newCards.push(card)
        }
        // console.log(newCards.length, newCards)

        // for (let i = 0; i < cards.length; i++) {
        //     const card = cards[i]

        //     const pathOfId = path.join(reqMainPath, 'champions', champion, 'cards', 'common', `${card.id}.jpg`)
        //     const loadOfId = await loadImage(pathOfId).catch(console.log)
        //     if (loadOfId) {
        //         card.img = loadOfId
        //         continue
        //     }

        //     const findCard = findEn.find(c => c.id == card.id)
        //     const cardNameEn = findCard.name.toLowerCase().replace(/[ ]+/g, '-').replace(/[']+/g, '')
        //     const pathOfName = path.join(reqMainPath, 'champions', champion, 'cards', 'common', `${cardNameEn}.jpg`)
        //     const loadOfName = await loadImage(pathOfName).catch(console.log)
        //     card.img = loadOfName
        // }

        return newCards
    } catch(error) {
        console.log(error)
        return false
    }
}


module.exports.getCardFrame = async function(points) {
    try {
        const pathPoints = path.join(reqMainPath, 'img', 'card frames', `${points}.png`)
        return await loadImage(pathPoints)
    } catch(error) {
        // console.log(error)
        return false
    }
}


module.exports.getChampionIcon = async function(name) {
    try {
        if (!name) return false
        const cnampionIconPath = path.join(reqMainPath, 'champions', name, 'icon.jpg')
        return await loadImage(cnampionIconPath)
    } catch(error) {
        // console.log(error)
        return false
    }
}


module.exports.getDivision = async function(num) {
    try {
        const divisionPath = path.join(reqMainPath, 'img', 'divisions', `${num}.png`)
        return await loadImage(divisionPath)
    } catch(error) {
        // console.log(error)
        return false
    }
}


module.exports.getItem = async function(name) {
    try {
        if (!name) return false
        const divisionPath = path.join(reqMainPath, 'img', 'items', `${name.toLowerCase()}.jpg`)
        return await loadImage(divisionPath)
    } catch(error) {
        // console.log(error)
        return false
    }
}


module.exports.getIconBot = async function() {
    try {
        const icon = path.join(reqMainPath, 'img', `icon.png`)
        return await loadImage(icon)
    } catch(error) {
        // console.log(error)
        return false
    }
}


module.exports.getRandomBackground = async function() { // загружает рандомную дефолтную картинку для фона
    try {
        const pathToBackground = path.join(reqMainPath, 'img', 'backgrounds')
        const backgroundFiles = await fs.readdirSync(pathToBackground)
        if (!backgroundFiles || !backgroundFiles.length) return false

        const imageName = backgroundFiles.random()
        return await loadImage(path.join(pathToBackground, imageName))
    } catch(error) {
        // console.log(error)
        return false
    }
}


module.exports.formProposals = function(text, maxLen) { // возвращает массив, разделяет строку на части
	if (!text) return ''
	if (text.length <= 25) return [text]
	let newText = []
	let tempLen = maxLen
	let lastIndex = 0

	while (true) {
		const letter = text?.slice(tempLen - 1, tempLen)
		if (!letter) {
			// если пусто, то вставляем оставшееся
			const g = text.slice(lastIndex, tempLen)
			if (g) newText.push( g )
			return newText
		} else if (letter !== ' ') { // если не пустая строка то пропускаем
			tempLen-- // сдвигаем влево поиск
			// если слово 25 символов?
		} else { // если пустая строка то нужно будет разбивать
			newText.push( text.slice(lastIndex, tempLen).trim() )
			lastIndex = tempLen
			tempLen += maxLen // продолжим поиски с того места где закончили + максимальная длина
		}
	}
}


module.exports.getChampionsWallpapers = async function(champion) { // random
    try {
        const pathToWallpapers = path.join(reqMainPath, 'champions', champion, 'wallpapers')
        const wallpaperFiles = await fs.readdirSync(pathToWallpapers)
        if (!wallpaperFiles || !wallpaperFiles.length) return false

        const imageName = wallpaperFiles.random()
        return await loadImage(path.join(pathToWallpapers, imageName))
    } catch(error) {
        // console.log(error)
        return false
    }
}


module.exports.getAvatar = async function(id) {
    try {
        const avatarPath = path.join(reqMainPath, 'img', 'avatars', `${id}.jpg`)
        const avatar = await loadImage(avatarPath).catch(console.log)
        if (avatar) return avatar
        return await loadImage(path.join(reqMainPath, 'img', 'avatars', `0.jpg`))
    } catch(error) {
        // console.log(error)
        return false
    }
}