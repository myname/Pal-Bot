/**
 * 
 */


/**
 * вернет canvas (картинку) - подгон картинки под нужное разрешение холста
 * @param {*} width - длина картинки которая нужна
 * @param {*} height - ширина картинки которая нужна
 */
module.exports.imageNormalize = function({ img, width, height }) {
    const { createCanvas } = require('canvas')
    const imgWidth = img.width
    const imgHeight = img.height
    // const { width: imgWidth, height: imgHeight } = img
    const canvas = createCanvas(imgWidth, imgHeight)
    const ctx = canvas.getContext('2d')
    ctx.drawImage(img, 0, 0, imgWidth, imgHeight)

    // определяем какая из сторон будет длинее и какую сторону бы будем обрезать
    const scaleWidth = width / imgWidth
    const scaleHeight = height / imgHeight

    if (scaleWidth == scaleHeight) return canvas // если равны то ничего менять не нужно

    if (scaleWidth > scaleHeight) {
        // длина больше
        const newHeight = height / (width / imgWidth)
        const paddingHeight = (imgHeight - newHeight) / 2

        // обрезаем картинку слева и справа
        const imgData = ctx.getImageData(0, paddingHeight, imgWidth, newHeight)

        // рисуем новую картинку на холсте
        ctx.canvas.height = newHeight
        ctx.putImageData(imgData, 0, 0)
    } else {
        // высота больше
        const newWidth = width / (height / imgHeight)
        const paddingWidth = (imgWidth - newWidth) / 2

        // обрезаем картинку сверху и снизу
        const imgData = ctx.getImageData(paddingWidth, 0, newWidth, imgHeight)

        // рисуем новую картинку на холсте
        ctx.canvas.width = newWidth
        ctx.putImageData(imgData, 0, 0)
    }

    return canvas
}


module.exports.drawRoundedImage = function(image, radius, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight) {
	let x = dx || sx
	let y = dy || sy
	let width =  dWidth || sWidth || image.naturalWidth
	let height = dHeight || sHeight || image.naturalHeight
	let r = { topLeft: 0, topRight: 0, bottomLeft: 0, bottomRight: 0 }

	if(!Array.isArray(radius)) {
		radius = [radius]
	}

	r.topLeft = radius[0]
	r.topRight = radius[1] || (radius[1]===undefined) * radius[0]
	r.bottomRight = radius[2] || (radius[2]===undefined) * radius[0]
	r.bottomLeft = radius[3] || (radius[3]===undefined) * (radius[1] || (radius[1]===undefined) * radius[0])

	this.beginPath()
	this.arc(x + r.topLeft, y + r.topLeft, r.topLeft, Math.PI, Math.PI + Math.PI / 2)
	this.lineTo(x + width - r.topRight, y)
	this.arc(x + width - r.topRight, y + r.topRight, r.topRight, Math.PI + Math.PI/2, Math.PI*2)
	this.lineTo(x + width, y + height - r.bottomRight)
	this.arc(x + width - r.bottomRight, y + height - r.bottomRight, r.bottomRight, Math.PI*2, Math.PI/2)
	this.lineTo(x + r.bottomLeft, y + height)
	this.arc(x + r.bottomLeft, y + height - r.bottomLeft, r.bottomLeft, Math.PI/2, Math.PI)
	this.closePath()
	this.save()
	this.clip()

	switch(true) {
		case arguments.length > 6:
			this.drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight)		
		break

		case arguments.length > 4:
			this.drawImage(image, sx, sy, sWidth, sHeight);		
		break

		default:
			this.drawImage(image, sx, sy)
		break
	}

	this.restore()
}