/**
 * функции для работы с обьектами и массивами
 */


Object.prototype.find = function(callback) {
    for (let key in this) {
        const value = this[key]
        const res = callback(key, value)
        if (res) return res
    }
}
Object.defineProperty(Object.prototype, 'find', { enumerable: false })


Array.prototype.filterRemove = function(callback) {
    const removedItems = [] // удаленные эллементы
	for (let i = 0; i < this.length; i++) {
		const item = this[i]
		if (!callback(item, i)) {
			removedItems.push( this.splice(i, 1)[0] )
			i--
		}
	}
    return removedItems
}
Object.defineProperty(Array.prototype, 'filterRemove', { enumerable: false })


/**
 * проверяет есть ли каждый переданный параметр в текущем массиве
 * @param {String} text 
 * @return {Boolean}
 */
Array.prototype.has = function(...list) {
	return list.every(text => {
		const lowerText = text.toLowerCase()
		return this.some(el => el.toLowerCase() == lowerText)
	})
}
Object.defineProperty(Array.prototype, 'has', { enumerable: false })


/**
 * получает рандомный эллемент из массива
 */
Array.prototype.random = function() {
	const random = Math.floor(Math.random() * this.length)
	return this[random]
}


// изменение выдачи логов для удобства
console.logOrig = console.log // сохраняем оригинал
console.log = async function(...args) {
	try {
		const path = require('path')
		const { appendFileSync } = require('fs')
		const date = new Date().toText()
		// тут нужно записать это в файл логов
		const day = date.split(' ')[0]
		const p = path.join(require.main.path, 'logs', day)
		console.logOrig(`|${date}|\t`, ...args)

		// let formText = ''
		// for (let i = 0; i < args.length; i ++) {
		// 	const text = `|${date}| ${args[i]}`
		// 	formText += text
		// }
		const textArgs = args.map(el => {
			if (!el) return 'undefined'

			if (el instanceof Error) {
				return el.stack.toString()
			} else {
				// console.logOrig('\n\n\n\n\n')
				// console.logOrig(el)
				// console.logOrig('\n\n\n\n\n')
				if (el.error instanceof Error) {
					el.error = el.error.stack.toString()
					if (el.toString && el.toString() != '[object Object]') el = el.toString()
					return JSON.stringify(el)?.slice(1)?.slice(0, -1)
				} else {
					if (el.toString && el.toString() != '[object Object]') el = el.toString()
					return JSON.stringify(el)?.slice(1)?.slice(0, -1)
				}
			}
		})
		const text = `\n|${date}| ${textArgs.join(' | ')}\n\n`

		await appendFileSync(p, text)
	} catch(err) {
		console.logOrig(err)
	}
}