/**
 * дополняют работу функций дискорда
 */


/**
 * проверяет, является ли пользователь владельцем бота
 * @returns {Boolean}
 */
module.exports.isOwner = function(userId) {
	const { OWNERS } = process.env
	return OWNERS.split(' ').some(id => id == userId)
}


module.exports.messageError = function({ text, lang='en', newTab=true, authorId, statsFor='me', embed }) {
	const path = require('path')
	const { youtube: { playlist } } = require(path.join(require.main.path, 'config', 'index.js'))
	const { DISCORD_INVITE } = process.env
	const { ActionRowBuilder, ButtonBuilder } = require('discord.js')
	const errText = (text || {
		ru: `Неизвестная ошибка... Сообщите подробности ошибки создателю бота.`, 
		en: `Unknown error... Report the details of the error to the creator of the bot.`
	})[lang]

	const bugText = ({
		ru: `<@${authorId}> Нашли баг? - сообщите о нем!`,
		en: `<@${authorId}> Did you find a bug? - report him!`
	})[lang]

	const buttonsLine_1 = new ActionRowBuilder()
	if (!newTab && authorId) {
		buttonsLine_1
		.addComponents(
			new ButtonBuilder()
			.setCustomId(`pal_${authorId}_${statsFor}`)
			.setLabel({ en: 'Menu', ru: 'Меню' }[lang])
			.setStyle('Danger')
			.setEmoji('<:menu:943824092635758632>')
		)
	}

	buttonsLine_1
	// .addComponents(
	// 	new ButtonBuilder()
	// 	.setCustomId(`help_${authorId}_${statsFor}_newtab`)
	// 	.setLabel({ en: 'Help', ru: 'Помощь' }[lang])
	// 	.setStyle('Danger')
	// 	.setEmoji('❔')
	// )
	.addComponents(
		new ButtonBuilder()
		.setURL(DISCORD_INVITE)
		.setLabel({ en: 'Help Server', ru: 'Сервер помощи' }[lang])
		.setStyle('Link')
	)
	// .addComponents(
	// 	new ButtonBuilder()
	// 	.setURL(playlist)
	// 	.setLabel({en: 'Watch videos on YouTube', ru: 'Смотреть видео на Ютуб'}[lang])
	// 	.setStyle('Link')
	// )

	return {
		content: `:warning::warning::warning: **${bugText}**\`\`\`\n${errText}\`\`\``,
		embeds: (embed ? [ embed ] : null),
		components: [ buttonsLine_1 ],
		ephemeral: newTab
	}
}


module.exports.resolveName = function(nameOrId, console=false) {
	const path = require('path')
	const { youtube: { search: searchVideo }, gif: { search: gif } } = require(path.join(require.main.path, 'config', 'index.js'))

	if (!nameOrId) return nameOrId
	if (nameOrId && nameOrId.mentionToId) nameOrId = nameOrId.mentionToId()
	// !/^[^\d\<\>\!\@\#\$\%\^\&\*\(\)\+\=\\\|\/\?\.\,\'\"\`\;\:][^\<\>\!\@\#\$\%\^\&\*\(\)\+\=\\\|\/\?\.\,\'\"\`\;\:]+$/.test

	if ( /[\`\~\!\@\#\$\%\^\&\*\(\)\=\+\[\]\{\}\;\:\'\"\\\|\?\/\.\>\,\< ]/.test(nameOrId) ) throw {
		status: false,
		err_msg: {
			ru: `Введите корректный Ник или id аккаунта Paladins.`,
			en: `Enter the correct Nickname or Paladins account id.`
		}
	}

	if (console) return nameOrId

	if ( /[\-\_]/.test(nameOrId) ) throw {
		status: false,
		err_msg: {
			ru: `Консольные аккаунты доступны только по ID.\n=\nИспользуйте команду /search для поиска нужного аккаунта.`,
			en: `Console accounts are available only by ID.\n=\nUse the command /search to find the desired account.`
		},
		embed: {
			description: `[YOUTUBE VIDEO](${searchVideo})`,
			color: '3092790',
			image: {
				url: gif
			}
		}
	}

	return nameOrId
}


