/**
 * вспомогательные функции для запросов на сайт
 */
const axios = require('axios')
const randomUseragent = require('random-useragent')
const { STEAMKEY } = process.env


module.exports.getSteamData = async function() {
    try {
        const steamAPIUrl = `https://api.steampowered.com/ISteamUserStats/GetNumberOfCurrentPlayers/v1/?format=json&appid=444090&key=${STEAMKEY}`
        const steamDataReq = await axios.get(steamAPIUrl, {
            headers: {
                'User-Agent': randomUseragent.getRandom()
            }
        })

        const steamData = steamDataReq.data
        return steamData?.response?.player_count || '-no data-'
    } catch(err) {
        console.log(err)
        return '-err-'
    }
}
