/**
 * обработчик сообщений
 */
const path = require('path')
const { isOwner } = require(path.join(require.main.path, 'utils', 'discord.js'))
const SettingsManager = require(path.join(require.main.path, 'classes', 'SettingsManager.js'))
const CommandsManager = require(path.join(require.main.path, 'classes', 'CommandsManager.js'))
const { messageError } = require(path.join(require.main.path, 'utils', 'discord.js'))


module.exports = db => async message => {
    let saveLang = 'en'
    let needSendReplyError = false

    try {
        const { launched, TESTING, PREFIX } = process.env
        const testing = TESTING === '1' ? true : false

        if (!launched) return; // если бот не запущен до конца
        const { author: { bot: isBot, id: authorId }, guild, content: messageContent, client } = message
        const { user: { id: botId } } = client
        if (isBot) return; // если сообщение от бота то игнорим

        const hasMention = message.mentions.users.has(botId)
        if (!messageContent && !hasMention) return;

        const checkOwner = isOwner(authorId)
        // если включен режим тестирования
        if (testing && !checkOwner) return;
        const content = (messageContent || '').replace(/^[\\]+/, '').replace(/[\n\r]+/g, ' ').trim()

        const settings = new SettingsManager({
            db,
            userId: authorId, 
            guildId: guild?.id, 
            preferredLocale: guild?.preferredLocale
        })

        const { lang } = settings.user
        saveLang = lang

        if (
            !content.startsWith(PREFIX) && 
            !hasMention
            // !content.startsWith(`<@!${botId}>`) && 
            // !content.startsWith(`<@${botId}>`) 
        ) return; // если нет префикса или упоминания (вместо префикса) то выход
        needSendReplyError = true
        const cont = content.startsWith(PREFIX) ? content.cut(PREFIX) : content.cut(`<@!${botId}>`).cut(`<@${botId}>`)

        const commandsManager = new CommandsManager()
        const textForGetCommand = cont || (hasMention ? 'menu' : '')

        // если нет текста, то просто выходим, скорее всего тут просто префикс без ничего
        if (!textForGetCommand) return;

        const command = commandsManager.get(textForGetCommand)
        if (!command) return; // команда не найдена
        const contentParams = cont?.parseContent(command.owner)

        // console.log(cont, hasMention, command.name, textForGetCommand)
        // console.log(command)

        // если команда только для админов и это не админ то выходим
        if (command.owner && !checkOwner) return;

        // если нет прав то сообщаем об этом на канале (если можем)
        const checkPerm = !guild ? true : message.channel.permissionsFor(client.user).has([ 'AttachFiles' ]) // EMBED_LINKS
        if (guild && !checkPerm) {
            const checkPerm = message.channel.permissionsFor(client.user).has([ 'SEND_MESSAGES' ])
            if (!checkPerm) return; // если даже не может написать сообщение
            throw {
                err_msg: {
                    ru: 'Не хватает прав для выполнения команды (AttachFiles).',
                    en: 'Insufficient rights to execute command (AttachFiles).'
                }
            }
        }

        // выводим в консоль отладочные данные
        let guildName = 'ls'
        if (guild) guildName = guild.name
        const logMessageInfo = `> ${guildName} <#${message.channel.id}> <@${authorId}> ${command.name}/${contentParams}`
        if (!command.owner) console.log(logMessageInfo)

        // запускаем печатание
        message.channel.sendTyping().catch(console.log)

        let isAdmin = null
        if (command.admin) {
            if (guild) {
                // const member = await guild.members.fetch(userId)
                isAdmin = message.member.permissions.has('ADMINISTRATOR')
            } else {
                isAdmin = true
            }
        }

        // выполняем команду
        const messageData = await command.command({
            db,
            settings, 
            command, 
            contentParams, 
            isAdmin, 
            client: command.owner ? message.client : null,
            isPM: guildName == 'ls'
        })
        await message.reply(messageData)

        if (command.owner) return;
        console.log(`Команда (text) успешно выполнена ${logMessageInfo}`)

        try {
            // увеличиваем число использований команды, только удачные и не админские
            await command.used({ db, client })
        } catch(error) {
            console.log(error)

            // отправка логов админу
            try {
                const { LOG_CHANNEL_ID } = process.env
                const logError = `Ошибка после успешного выполнения текст-команды.`

                const chLogs = await message.client.channels.fetch(LOG_CHANNEL_ID)
                await chLogs.send(logError)
            } catch(error) {
                console.log(error)
            }
        }
    } catch(error) {
        console.log(error)

        if (needSendReplyError) {
            // отправка ошибки пользователю
            try {
                const { author: { id: authorId } } = message
                const messageData = messageError({
                    text: error?.err_msg, 
                    lang: saveLang,
                    authorId,
                    embed: error?.embed
                })

                await message.reply(messageData)
            } catch(error) {
                console.log(error)
            }
        }

        // отправка логов админу
        try {
            const { LOG_CHANNEL_ID } = process.env
            const logError = error?.log_msg

            if (logError) {
                const chLogs = await message.client.channels.fetch(LOG_CHANNEL_ID)
                await chLogs.send(logError)
            }
        } catch(error) {
            console.log(error)
        }
    }
}


function an(content) {
    if (!content || !content.split) return false
    const { OBSCENE='' } = process.env
    const obscene = OBSCENE.split(' ')
    const words = content.split(' ')
    const setWords = new Set(words)

    let obsceneLen = 0
    obscene.forEach(obs => {
        if (setWords.has(obs)) obsceneLen++
    })

    return {
        obscene: obsceneLen, // маты
        wordLen: words.length, // кол-во слов
        avgWordLen: words.reduce((a, b) => {return a + b.length}, 0) / words.length // средняя длина слова
    }
}