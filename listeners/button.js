/**
 * обработчик сообщений
 */
const path = require('path')
const { DISCORD_INVITE } = process.env
const { ActionRowBuilder, ButtonBuilder } = require('discord.js')
const SettingsManager = require(path.join(require.main.path, 'classes', 'SettingsManager.js'))
const CommandsManager = require(path.join(require.main.path, 'classes', 'CommandsManager.js'))
const { messageError } = require(path.join(require.main.path, 'utils', 'discord.js'))


module.exports = async (interaction, db) => {
    let saveLang = 'en'
    let saveNewTab = true

    try {
        const { guild, values: selectMenuValues } = interaction
        const [ commandName, userId, statsFor='me', ...branches ] = interaction.customId.split('_')
        // commandName - название команды | userId - id юзера | statsFor - для кого вызвано меню (me)

        const authorId = interaction.user.id
        const settings = new SettingsManager({
            db,
            userId: authorId, 
            guildId: guild?.id, 
            preferredLocale: guild?.preferredLocale
        })

        const { lang } = settings.user
        saveLang = lang

        const commandsManager = new CommandsManager()
        const command = commandsManager.get(commandName)
        if (!command) return; // команда не найдена (поидее такого быть не должно)

        // не его кнопка, то выдадим новое сообщение, подставив его id
        const newUser = userId != authorId
        let { newTab=false, newMessInCommand=false } = command
        if (newUser || (selectMenuValues && selectMenuValues[0] && selectMenuValues[0].startsWith('_'))) newTab = true
        if (newMessInCommand) {
            // определяем команда внутри или нет
            if (selectMenuValues) newTab = true // если был выбор в списке, значит внутри
            // branches
        }

        if (command.name == 'help' && branches?.[0] == 'newtab') {
            newTab = true // фикс для кнопки хелпы которая в новой вкладке будет и скрытой
            console.log('СКРЫТЫЙ НОВЫЙ ТАБ ДЛЯ ХЕЛПЫ')
        }

        saveNewTab = newTab
        console.log(`newTab: ${newTab}; newUser: ${newUser}, ${userId} ${authorId}, ${interaction.customId}`)

        // можно проверить права (а можно и не проверять), но лучше проверить

        // выводим в консоль отладочные данные
        let guildName = 'ls'
        if (guild) guildName = guild.name
        const logMessageInfo = `> ${guildName} <#${interaction.channelId}> <@${authorId}> ${command.name}/${statsFor}/${selectMenuValues}/${branches}`
        if (!command.owner) console.log(logMessageInfo)

        if (!newTab) {
            // тут нужно добавить в массив ( new Set ) то что это сообщение уже редачится и что бы его нажатия игнорились !!!
            await interaction.deferUpdate().catch(console.log) // откладываем ответ

            const buttonsLine_1 = new ActionRowBuilder()
            .addComponents(
                new ButtonBuilder()
                .setURL(DISCORD_INVITE)
                .setLabel({ en: 'Help', ru: 'Помощь' }[lang])
                .setStyle('Link')
            )

            await interaction.editReply({
                content: `<@${authorId}> ` + {
                    ru: 'Идет обработка команды, пожалуйста подождите...',
                    en: 'The command is being processed, please wait...'
                }[lang],
                components: [ buttonsLine_1 ]
            }).catch(console.log)
        }

        // тут из 'массива' уже можно удалить эти данные

        let isAdmin = null
        if (command.admin) {
            if (guild) {
                isAdmin = interaction.member.permissions.has('ADMINISTRATOR')
            } else {
                isAdmin = true
            }
        }

        let guildForCommand = null
        if (command.guild) {
            const serverId = branches[1]
            guildForCommand = await interaction.client.guilds.fetch(serverId)
            // console.log(guildForCommand, branches, serverId)
        }

        const { status, messageData, editReply, newReply, defer, needEmbed } = await command.button({
            db,
            command,
            selectMenuValues,
            statsFor,
            branches,
            settings,
            isAdmin,
            isPM: guildName == 'ls',
            guild: guildForCommand
        })

        if (!needEmbed) messageData.embeds = []

        const { ephemeral } = messageData
        messageData.content = `<@${authorId}>` + (messageData.content || '')

        // отправляем ответ пользователю
        if ((newTab && !editReply) || newReply) {
            // await interaction.deferUpdate().catch(e => {}) // откладываем ответ
            if (ephemeral) {
                if (editReply) await interaction.deferUpdate().catch(console.log) // откладываем ответ
                await interaction.reply(messageData)
            } else {
                if (editReply) await interaction.deferUpdate().catch(console.log) // откладываем ответ
                await interaction.channel.send(messageData) // мб в этом случае можно послать какуют хуйню что бы забить? (Ошибка взаимодействия)
            }
        } else {
            if (editReply) await interaction.deferUpdate().catch(console.log) // откладываем ответ
            if (!ephemeral) await interaction.message.removeAttachments() // удаляем файлы
            await interaction.editReply(messageData)
        }

        if (status === 1) {
            console.log(`Команда (button) успешно выполнена ${logMessageInfo}`)
            await command.used({ db, client: interaction.client })
        } else if (status === 2) {
            console.log(`Был осуществлен переход внутри команды ${logMessageInfo}`)
        } else {
            console.log(`Какая-то ошибка поидее... ${logMessageInfo}`)
        }
    } catch(error) {
        console.log(error)

        // отправка ошибки пользователю
        try {
            const [ , , statsFor='me' ] = interaction.customId.split('_')
            const authorId = interaction.user.id
            const messageData = messageError({
                text: error?.err_msg, 
                lang: saveLang, 
                newTab: saveNewTab,
                authorId,
                statsFor,
                embed: error?.embed
            })

            if (saveNewTab) {
                // await interaction.channel.send(messageData)
                await interaction.reply(messageData)
            } else {
                await interaction.editReply(messageData)
            }
        } catch(error) {
            console.log(error)
        }

        // отправка логов админу
        try {
            const { LOG_CHANNEL_ID } = process.env
            const logError = error?.log_msg

            if (logError) {
                const chLogs = await interaction.client.channels.fetch(LOG_CHANNEL_ID)
                await chLogs.send(logError)
            }
        } catch(error) {
            console.log(error)
        }
    }
}