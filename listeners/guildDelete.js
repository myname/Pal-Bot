/**
 * событие при удалении бота с сервера
 */
const { EMPTY_ICON, TESTING, NOTIFICATION_CHANNEL_ID } = process.env


module.exports = async guild => {
    try {
        const testing = TESTING === '1' ? true : false
        
        console.log(`\tGUILD DELETE ${guild.name} (${guild.id}) | <@${guild.ownerId}>`)
        if (!guild.name || !guild.id || !guild.ownerId || guild.ownerId == 'undefined' || guild.id == '1004919883387121664') return; // хз че за бредовый баг при перезапуске ивент на пустой серв

        // если включен режим тестирования
        if (testing) return;

        const channel = await guild.client.channels.fetch(NOTIFICATION_CHANNEL_ID)
        await channel.send({
            embeds: [{
                title: `${guild.name} (${guild.id})`,
                description: '__удален__',
                color: '13049629',
                timestamp: guild.joinedAt,
                footer: {
                    icon_url: EMPTY_ICON,
                    text: 'Был добавлен'
                },
                thumbnail: {
                    url: guild.iconURL()
                },
                fields: [
                    {
                        name: 'Owner',
                        value: `<@${guild.ownerId}>`,
                        inline: true
                    },
                    {
                        name: 'member count',
                        value: (guild.memberCount + ''),
                        inline: true
                    }
                ]
            }]
        })
    } catch(err) {
        console.log('Ошибка отправки сообщения при удаленни бота с сервера:')
        console.log(err)
    }
}