/**
 * событие при добавлении бота на сервер
 */
const path = require('path')
const { ActionRowBuilder, ButtonBuilder, StringSelectMenuBuilder } = require('discord.js')
const localPath = require.main.path
const { DISCORD_INVITE, BOT_ICON, EMPTY_ICON, TESTING } = process.env
const { langFlag, fullnameLang, slogan, youtube: { playlist } } = require(path.join(require.main.path, 'config', 'index.js'))
const SettingsManager = require(path.join(localPath, 'classes', 'SettingsManager'))


module.exports = async guild => {
    try {
        const testing = TESTING === '1' ? true : false
        // если включен режим тестирования
        if (testing) return;
        console.log(`\tADD GUILD: ${guild.name} (${guild.id}) | <@${guild.ownerId}>`)

        const settings = new SettingsManager({
            // db,
            // userId: authorId, 
            guildId: guild?.id, 
            preferredLocale: guild?.preferredLocale
        })

        const { lang } = settings.guild

        const owner = await guild.members.fetch(guild.ownerId)
        const fieldData = lang == 'ru' ? {
            name: 'Спасибо что используете бота ❤️',
            value: `Есть вопрос?, предложение? или хочешь быть всегда в курсе событий? - заходи на [сервер бота](${DISCORD_INVITE}).\n` +
                `Для получения помощи введите команду **\`/help\`**.\nМы подготовили видео примеры работы команд для вас [СМОТРЕТЬ НА ЮТУБЕ](${playlist}).\n` +
                '**Изменить язык для сервера** можно воспользовавшись командой **`/settings`**.\n' +
                '```yaml\nПриятного пользования!```'
        } : {
            name: 'Thanks for using the bot ❤️',
            value: `Have a question ?, a suggestion? or do you want to be always up to date? - go to the [bot server](${DISCORD_INVITE}).\n` +
                `To get help, enter the command **\`/help\`**.\nWe have prepared video examples of how teams work for you [WATCH VIDEOS ON YOUTUBE](${playlist}).\n` +
                '**You can change the language for the server** by using the **`/settings`** command.\n' +
                'Enjoy your use.'
        }

        // const options = []
        // for (let langName in langFlag) {
        //     options.push({
        //         label: `${fullnameLang[langName]} [${langName.toUpperCase()}]`,
        //         value: langName,
        //         emoji: langFlag[langName]
        //     })
        // }

        // const buttonsLine_1 = new ActionRowBuilder()
        // .addComponents(
        //     new StringSelectMenuBuilder()
        //         .setCustomId(`guildcreate_${guild.ownerId}_${guild.id}_${guild.ownerId}_${guild.id}`)
        //         .setPlaceholder({ en: 'Select the Language for the server', ru: 'Выберите Язык для сервера' }[lang])
        //         .addOptions(options)
        // )

        const buttonsLine_2 = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
            .setCustomId(`help_${guild.ownerId}_${guild.id}_newtab`)
            .setLabel({ en: 'Help', ru: 'Помощь' }[lang])
            .setStyle('Danger')
            .setEmoji('❔')
        )
        .addComponents(
            new ButtonBuilder()
            .setURL(DISCORD_INVITE)
            .setLabel({en: 'Join bot server', ru: 'Посетить сервер бота'}[lang])
            .setStyle('Link')
        )
        .addComponents(
            new ButtonBuilder()
            .setURL(playlist)
            .setLabel({en: 'Watch videos on YouTube', ru: 'Смотреть видео на Ютуб'}[lang])
            .setStyle('Link')
        )

        // отправляет приветственное сообщение с информацией создателю сервера (нужно предупредить об этом)
        await owner.send({
            components: [ buttonsLine_2 ],
            embeds: [{
                // title: `${{ en: 'Server lang', ru: 'Язык сервера' }[lang]}: ${lang}`,
                author: {
                    name: `${{ en: 'Server lang', ru: 'Язык сервера' }[lang]}: ${lang}`,
                    icon_url: EMPTY_ICON
                },
                description: `**Server name:** ${guild.name}\ \n**Server id:** ${guild.id}`,
                thumbnail: {
                    url: guild.iconURL()
                },
                color: '2276909',
                footer: {
                    icon_url: BOT_ICON,
                    text: slogan[lang]
                },
                fields: [ fieldData ],
                image: {
                    url: 'https://media.discordapp.net/attachments/786140964360159283/1010564646933115040/help.gif'
                }
            }]
        })
    } catch(error) {
        console.log(`Ошибка отправки сообщения ОВНЕРУ: ${guild.ownerId}:`)
        console.log(error)
    }

    try {
        const { NOTIFICATION_CHANNEL_ID } = process.env
        const channel = await guild.client.channels.fetch(NOTIFICATION_CHANNEL_ID)
        await channel.send({
            embeds: [{
                title: `${guild.name} (${guild.id})`,
                description: '__добавлен__',
                color: '2276909',
                thumbnail: {
                    url: guild.iconURL()
                },
                fields: [
                    {
                        name: 'Owner',
                        value: `<@${guild.ownerId}>`,
                        inline: true
                    },
                    {
                        name: 'member count',
                        value: (guild.memberCount + ''),
                        inline: true
                    }
                ]
            }]
        })
    } catch(error) {
        console.log(`Ошибка отправки сообщения на канал сервера:`)
        console.log(error)
    }
}