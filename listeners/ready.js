/**
 * 
 */
const path = require('path')
const { youtube: { setting: videoURL } } = require(path.join(require.main.path, 'config', 'index.js'))
const { getSteamData } = require(path.join(require.main.path, 'utils', 'requestFunctions.js'))
// const { commandsStats } = require(path.join(require.main.path, 'mongo', 'schemes.js'))
// const { commandsStats: commandsStatsModel  } = require(path.join(require.main.path, 'mongo', 'schemes.js'))
const { CHANNEL_SERVERS_ID, CHANNEL_USED_ID, timeStart, NOTIFICATION_CHANNEL_ID, TESTING, GUILD_ID } = process.env


 module.exports = db => async client => {
    try {
        const testing = TESTING === '1' ? true : false
        process.env.launched = true // разрешаем работу слушателей

        // если включен режим тестирования
        if (testing) return console.log(`Запущен в режиме тестирования.`)
        // const guild = await client.guilds.fetch(GUILD_ID)
        const chServers = await client.channels.fetch(CHANNEL_SERVERS_ID)
        const chCommands = await client.channels.fetch(CHANNEL_USED_ID)

        const randActFunc = randomAction.bind(client)
        setInterval(randActFunc, 1000 * 60 * 3)
        randActFunc()

        setInterval(async () => {
            try {
                const statsNowDay = db.stats[ db.stats.length - 1 ]
                if ( !statsNowDay ) return;

                await chServers.setName(`Servers: ${ statsNowDay?.servers }`)
                await chCommands.setName(`Used: ${ statsNowDay?.used }`)
            } catch(e) {
                console.log(e)
            }
        }, 1000 * 60 * 5)

        const channelNot = await client.channels.fetch(NOTIFICATION_CHANNEL_ID)
        channelNot.send('Я запустился!')
        .catch(err => {
            console.log('Ошибка отправки сообщения о запуске бота.')
        })

        console.log(` - Бот полностью запущен. (${new Date() - timeStart}ms)`)
    } catch(err) {
        console.log('ОШИБКА В READY:')
        console.log(err)
    }
}


async function randomAction() {
    try {
        const actions = [ 'streaming', 'steam', 'help' ]
        const randNum = Math.ceil(Math.random() * actions.length) - 1
        const trandType = actions[ randNum ]
        console.log(`set activity: ${trandType}`)

        // if (trandType == 'streaming') {
        //     this.user.setPresence({
        //         activities: [{
        //             name: 'Bot commands',
        //             type: 'STREAMING',
        //             url: videoURL
        //         }],
        //         status: 'online'
        //     })
        // } else if (trandType == 'steam') {
        //     const steamCountPlayers = await getSteamData()

        //     this.user.setActivity(`Steam: ${steamCountPlayers} players.`, { type: 'Playing', status: 'online' })
        //     // this.user.setPresence({
        //     //     activities: [{
        //     //         name: `Steam: ${steamCountPlayers} players.`,
        //     //         type: 'PLAYING'
        //     //     }],
        //     //     status: 'online'
        //     // })
        // } else if (trandType == 'help') {
        //     this.user.setPresence({
        //         activities: [{
        //             name: '/help - get help',
        //             type: 'WATCHING'
        //         }],
        //         status: 'online'
        //     })
        // }
        // else if (trandType == 'used') {
        //     const find = await commandsStats.find()
        //     const data = find[ find.length - 1 ]
        //     const servers = this.guilds.cache.size

        //     this.user.setPresence({
        //         activities: [{
        //             name: `Servers: ${servers} | Command used: ${data.usedCommands}`,
        //             type: 'WATCHING'
        //         }],
        //         status: 'online'
        //     })
        // }
    } catch(err) {
        console.log(err)
    }
}