/**
 * обработчик slash команд
 */
const path = require('path')
const SettingsManager = require(path.join(require.main.path, 'classes', 'SettingsManager.js'))
const CommandsManager = require(path.join(require.main.path, 'classes', 'CommandsManager.js'))
const { messageError } = require(path.join(require.main.path, 'utils', 'discord.js'))


module.exports = async (interaction, db) => {
    let saveLang = 'en'

    try {
        // const { commandName, options: { _group: optionsGroup, _subcommand: optionsSubcommand }, guild } = interaction
        const { commandName, options, guild } = interaction
        const authorId = interaction.user.id

        const settings = new SettingsManager({
            db,
            userId: authorId, 
            guildId: guild?.id, 
            preferredLocale: guild?.preferredLocale
        })

        const { lang } = settings.user
        saveLang = lang

        const commandsManager = new CommandsManager()
        // const command = commandsManager.getBySlashName(commandName, optionsGroup, optionsSubcommand)
        const command = commandsManager.getBySlashName(commandName)

        if (!command) return; // команда не найдена (поидее такого быть не должно)
        // выводим в консоль отладочные данные
        let guildName = 'ls'
        if (guild) guildName = guild.name
        const logMessageInfo = `> ${guildName} <#${interaction.channelId}> <@${authorId}> ${command.name}/${options}`
        if (!command.owner) console.log(logMessageInfo)

        let isAdmin = null
        if (command.admin) {
            if (guild) {
                // const member = await guild.members.fetch(userId)
                isAdmin = interaction.member.permissions.has('ADMINISTRATOR')
            } else {
                isAdmin = true
            }
        }

        await interaction.deferReply()
        const { status, messageData } = await command.slash({
            db,
            command,
            settings,
            options,
            isAdmin,
            isPM: guildName == 'ls'
        })

        // отправляем ответ пользователю
        await interaction.editReply(messageData)

        try {
            if (status === 1) {
                console.log(`Команда (slash) успешно выполнена ${logMessageInfo}`)
                await command.used({ db, client: interaction.client })
            } else {
                console.log(`Какая-то ошибка поидее (slash)... ${logMessageInfo}`)
            }
        } catch(error) {
            console.log(error)

            // отправка логов админу
            try {
                const { LOG_CHANNEL_ID } = process.env
                const logError = `Ошибка после успешного выполнения слеш-команды.`

                const chLogs = await interaction.client.channels.fetch(LOG_CHANNEL_ID)
                await chLogs.send(logError)
            } catch(error) {
                console.log(error)
            }
        }
    } catch(error) {
        console.log(error)

        // отправка ошибки пользователю
        try {
            const authorId = interaction.user.id
            const messageData = messageError({
                text: error?.err_msg, 
                lang: saveLang,
                authorId,
                embed: error?.embed
            })

            await interaction.editReply(messageData)
        } catch(error) {
            console.log(error)
        }

        // отправка логов админу
        try {
            const { LOG_CHANNEL_ID } = process.env
            const logError = error?.log_msg

            if (logError) {
                const chLogs = await interaction.client.channels.fetch(LOG_CHANNEL_ID)
                await chLogs.send(logError)
            }
        } catch(error) {
            console.log(error)
        }
    }
}