# Pal-bot

Дискорд бот выдачи статистики по игре Paladins.

[Добавить](https://discordapp.com/oauth2/authorize?client_id=626327927050600448&permissions=2147797056&scope=bot%20applications.commands) бота на сервер.

Заходите на наш [Discord-сервер](https://discord.gg/C2phgzTxH9) - там всегда можно быстро узнать о предстоящих обновлениях, сообщить об ошибках или получить другую помощь/информацию.

Вы так же можете сами запустить бота следуя [Инструкции](#installation)

Подробную информацию о командах вы можете получить использовав команду `hh`, например: `!hh ss`
Рекомендую начать использовать слеш-команды, например: `/help`

## Commands

* [help](#help)
* [me](#me)
* [menu](#menu)
* [setting](#setting)
* [search](#search)
* [stats](#stats)
* [history](#history)
* [last](#last)
* [current](#current)
* [champions](#champions)
* [champion](#champion)
* [deck](#deck)
* [friends](#friends)

### help

Меню с видео помощью по командам и примерами их работы

### me

Позволяет сохранить свой ник для подстановки его в команды автоматически

### menu

Вызывает главное меню с кнопками

### setting

Настройки

### search

Поиск игроков среди всех платформ

### stats

Статистика аккаунта

### history

История матчей игрока

### last

Информация о последнем или конкретном матче

### current

Статус игркоа в игре

### champions

Топ чемпионов аккаунта

### champion

Статистика чемпиона

### deck

Колоды аккаунта

### friends

Друзья аккаунта

## installation

Установку стоит производить только если вы хотите получить своего собственного бота.
Вы можете пользоваться уже существующим ботом не производя установки.

* Клонируйте репозиторий `git clone https://codeberg.org/myname/Pal-Bot.git`
* прейдите в папку `cd Pal-Bot`
* установите зависимости `npm i`
* Создайте пустую папку `logs` в корневой папке бота `mkdir logs`
* Создайте файл `.env` и запишите туда следующие настройки подставив вместо нулей свои значения
* ВАЖНО!!! Измените id создателя в конфиге .env (OWNERS). Если хотите что бы было несколько админов, то перечислите их id через пробел
```
DISCORDTOKEN=00000000000000000000000000000000000000000000000000000000000
STEAMKEY=0000000000000000000000000000000
DEVID=0000
AUTHKEY=0000000000000000000000000000000
OWNERS=000000000000000000
NOTIFICATION_CHANNEL_ID=000000000000000000
LOG_CHANNEL_ID=000000000000000000
CHANNEL_SERVERS_ID=000000000000000000
CHANNEL_USED_ID=000000000000000000
SITE_URL=
DISCORD_INVITE=https://discord.gg/C2phgzTxH9
EMPTY_ICON=https://codeberg.org/myname/Pal-Bot/raw/branch/master/img/empty.png
BOT_ICON=https://codeberg.org/myname/Pal-Bot/raw/branch/master/img/icon.png
COPYRIGHT=© 2019 Pal-Bot
PREFIX=!
TESTING=0
```
* Когда бот будет уже запущен введите в чат команду `!console slash` для регистрации слеш команд глобально. Учтите что нужно будет сменить ID создателя в конфинге
* Очистка всех данных (кроме информации о чемпионах) из базы данных: `!console cleardbItem`
* Очистка всех данных (кроме items и информации о чемпионах) из базы данных: `!console cleardb`
* Очистка данных чемпионов (для обновления, когда вышел новый чемпион или были изменения в игре) из базы данных: `!console champions`

## Original Authors

* [myname](https://codeberg.org/myname)

## Contacts

* [Discord](https://discord.gg/C2phgzTxH9)