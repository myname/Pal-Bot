# Pal-bot

[RU README](https://codeberg.org/myname/Pal-Bot/src/branch/master/README_RU.md)

Discord bot for issuing statistics on the Paladins game.

[Add](https://discordapp.com/oauth2/authorize?client_id=626327927050600448&permissions=2147797056&scope=bot%20applications.commands) to Discord server.

Visit our [Discord server](https://discord.gg/C2phgzTxH9) - there you can always quickly find out about upcoming updates, report bugs or get other help/information.

You can also run the bot yourself by following the [Instructions](#installation)

You can get detailed information about the commands by using the command `hh`, example: `!hh ss`
I recommend starting using slash commands, for example: `/help`

## Commands

* [help](#help)
* [me](#me)
* [menu](#menu)
* [setting](#setting)
* [search](#search)
* [stats](#stats)
* [history](#history)
* [last](#last)
* [current](#current)
* [champions](#champions)
* [champion](#champion)
* [deck](#deck)
* [friends](#friends)

### help

Menu with video help on commands and examples of their work

### me

Allows you to save your nickname for substituting it into teams automatically

### menu

Opens the main menu with the buttons

### setting

Settings

### search

Search for players among all platforms

### stats

Account statistics

### history

Player's Match History

### last

Information about the last or specific match

### current

Player status in the game

### champions

Top Account Champions

### champion

Championship statistics

### deck

Account Decks

### friends

Friends of the account

## installation

Installation should be done only if you want to get your own bot.
You can use an existing bot without installing it.

* Clone the repository `git clone https://codeberg.org/myname/Pal-Bot.git`
* install all the dependencies `cd Pal-Bot`
* Create an empty `logs` in the root folder of the bot, if it is not there `mkdir logs`
* Create a file `.env` and write the following settings there, substituting their values together with zeros
* IMPORTANT!!! Change the id of the creator in the config .env (OWNERS). If you want there to be several admins, then list their id separated by a space
```
DISCORDTOKEN=00000000000000000000000000000000000000000000000000000000000
STEAMKEY=0000000000000000000000000000000
DEVID=0000
AUTHKEY=0000000000000000000000000000000
OWNERS=000000000000000000
NOTIFICATION_CHANNEL_ID=000000000000000000
LOG_CHANNEL_ID=000000000000000000
CHANNEL_SERVERS_ID=000000000000000000
CHANNEL_USED_ID=000000000000000000
SITE_URL=
DISCORD_INVITE=https://discord.gg/C2phgzTxH9
EMPTY_ICON=https://codeberg.org/myname/Pal-Bot/raw/branch/master/img/empty.png
BOT_ICON=https://codeberg.org/myname/Pal-Bot/raw/branch/master/img/icon.png
COPYRIGHT=© 2019 Pal-Bot
PREFIX=!
TESTING=0
```
* When the bot is already running, enter the command in the chat `!console slash` to register slash commands globally. Please note that you will need to change the creator's ID in the config
* Clearing all data (except information about champions) from the database: `!console cleardbItem`
* Clearing all data (except items and information about champions) from the database: `!console cleardb`
* Clearing champions data (to update when a new champion came out or there were changes in the game) from the database: `!console champions`

## Original Authors

* [myname](https://codeberg.org/myname)

## Contacts

* [Discord](https://discord.gg/C2phgzTxH9)