const path = require('path')

const { GatewayIntentBits, Client, REST, Routes, Partials } = require('discord.js')
const client = new Client({ intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildIntegrations,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.GuildMessageTyping,
    GatewayIntentBits.DirectMessages,
    GatewayIntentBits.DirectMessageTyping,
    GatewayIntentBits.MessageContent
], partials: [ Partials.Message, Partials.Channel, Partials.User ] })


// подключаем статические улиты
require(path.join(require.main.path, 'utils', 'static', 'time.js'))
require(path.join(require.main.path, 'utils', 'static', 'string.js'))
require(path.join(require.main.path, 'utils', 'static', 'object.js'))


require('dotenv').config()
const { DISCORDTOKEN, DEVID, AUTHKEY } = process.env
process.env.timeStart = +new Date() // время старта

const { isOwner } = require(path.join(require.main.path, 'utils', 'discord.js'))

const DB = require('./classes/DB.js')
const API = require('./classes/API.js')


;(async () => {
    try {
        const db = new DB()
        const startDb = await db.start()
        if ( !startDb ) throw 'Ошибка запуска БД'
        console.log('Загрузка БД завершена успешно')

        const api = new API({ DEVID, AUTHKEY, db })
        db.api = api
        await api.start()

        console.log('Загружаем champions и items')
        db.champions = {}
        db.champions.ru = await api.getchampions(11)
        db.champions.en = await api.getchampions(1)
        db.items = {}
        db.items.ru = await api.getitems(11)
        db.items.en = await api.getitems(1)
        await db.loadStats()

        // classes/Command.js
        // setInterval(async () => {
        //     try {
        //         await db.saveStats()
        //     } catch( error ) {
        //         console.log(`\n\tОшибка сохранения статистики в БД.\n`)
        //     }
        // }, 1000 * 60 * 5)

        // загружаем обработчики
        const ready = require(path.join(require.main.path, 'listeners', 'ready.js'))
        client.on('ready', ready(db))

        const guildCreate = require(path.join(require.main.path, 'listeners', 'guildCreate.js'))
        client.on('guildCreate', guildCreate)

        const guildDelete = require(path.join(require.main.path, 'listeners', 'guildDelete.js'))
        client.on('guildDelete', guildDelete)

        const button = require(path.join(require.main.path, 'listeners', 'button.js'))
        const slash = require(path.join(require.main.path, 'listeners', 'slash.js'))
        client.on('interactionCreate', async interaction => {
            const { launched, TESTING } = process.env
            const testing = TESTING === '1' ? true : false

            if (!launched) return; // если бот не запущен до конца
            if (interaction.user.bot) return; // пропускаем ботов

            // если включен режим тестирования
            if (testing && !isOwner(interaction.user.id)) return;

            if (interaction.isButton() || interaction.isStringSelectMenu()) { //interaction.isSelectMenu
                await button(interaction, db)
            } else if (interaction.isCommand()) {
                await slash(interaction, db)
            }
        })

        const message = require(path.join(require.main.path, 'listeners', 'message.js'))
        client.on('messageCreate', message(db))

        console.log(' # Начинаю запуск клиента бота...')
        client.login(DISCORDTOKEN)
    } catch(err) {
        console.log('try/catch')
        console.log(err)
    }
})();


